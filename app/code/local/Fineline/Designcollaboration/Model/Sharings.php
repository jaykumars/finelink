<?php

class Fineline_Designcollaboration_Model_Sharings extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/sharings');
    }

    public function sendMailSharing($customerEmail, $senderData, $templateParams = array())
    {
        $senderName = $senderData->getFirstname() . " " . $senderData->getLastname();
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('sharing_info');
        $processedTemplate = $emailTemplate->getProcessedTemplate($templateParams);
        $subject = $senderName . ' shared a Design with You';

        Mage::helper('designcollaboration')->sendMail($customerEmail, $subject, $senderName, $processedTemplate);
    }

    public function getLockedBySharingCustomer($designId, $customerId)
    {
        $designData = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFieldToFilter('design_id', $designId)
            ->addFieldToFilter('locked_by', $customerId)
            ->setPageSize(1);

        return $designData->getSize() > 0;
    }

}