<?php
class Onelogin_SAML_Block_MetadataURL extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) { // store level
            $storeId = Mage::getModel('core/store')->load($code)->getId();
        } elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) { // website level
            $websiteId = Mage::getModel('core/website')->load($code)->getId();
            $storeId = Mage::app()->getWebsite($websiteId)->getDefaultStore()->getId();
        } else { // default level
            $storeId = 0;
        }

        $store = Mage::app()->getStore($storeId);
        $metadata_url = $store->getUrl("sso/saml/metadata",array('_secure'=>true));
        $pos = strpos($metadata_url, '?');
        $metadata_url = ($pos>0) ? substr($metadata_url, 0, $pos) : $metadata_url;

        $license = Mage::getStoreConfig('onelogin_saml/status/license');
        $license_html = "";
        if (!empty($license)) {
            $license_html = '&license='.urlencode($license);
        }
        $html = '<a target="_blank" href="'.$metadata_url.'">'.$metadata_url.'</a>';
        $html .= '<img src="https://logs-01.loggly.com/inputs/30c06b29-45ba-411f-a89c-60a9eb5f8e22.gif?source=pixel'.$license_html.'&extension=saml" />';

        return $html;
    }
}
