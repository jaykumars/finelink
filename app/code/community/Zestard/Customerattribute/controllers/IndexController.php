<?php
class Zestard_Customerattribute_IndexController extends Mage_Core_Controller_Front_Action
{
	public function uniqueAction()
	{		
		$attributecode = $this->getRequest()->getParam('attributecode');
		$attributeValue = $this->getRequest()->getParam('attributeValue');		
		if($attributeValue == NULL)
			return 0;
		$customerCollection = Mage::getModel('customer/customer')
			->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter($attributecode,array('eq'=>$attributeValue));
		echo count($customerCollection);
	}
	
	public function edituniqueAction()
	{		
		$attributecode = $this->getRequest()->getParam('attributecode');
		$attributeValue = $this->getRequest()->getParam('attributeValue');
		if($attributeValue == NULL)
			return 0;
		$customerData = Mage::getSingleton('customer/session')->getCustomer();		
		$customerCollection = Mage::getModel('customer/customer')
			->getCollection()
			->addAttributeToSelect('*')
			->addFieldToFilter('entity_id',array('neq'=>$customerData->getEntityId()))
			->addAttributeToFilter($attributecode,array('eq'=>$attributeValue));
		echo count($customerCollection);
	}
}
?>
