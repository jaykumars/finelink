<?php
class Rocketbuild_Myapprovals_Block_Index extends Mage_Core_Block_Template{

	private $requisitions = array();

	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('myapprovals/index.phtml');

		// $requisitions = Mage::helper('myapprovals')->getRequisitionsForCustomer(Mage::getSingleton('customer/session')->getCustomer()->getId());

		$requisitions = Mage::helper('myapprovals')->getRequisitionsForApprovalByCustomer(Mage::getSingleton('customer/session')->getCustomer()->getId());	

		if ($requisitions !== NULL) {
		$this->requisitions = $requisitions->setOrder('created_at','DESC');
      }

		Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
	}

	protected function _prepareLayout()
	{
		parent::_prepareLayout();

		// $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
		            //   ->setCollection($this->getOrders());
		// $this->setChild('pager', $pager);
		// $this->getOrders()->load();
		return $this;
	}

	public function getRequisitions() {
		return $this->requisitions;
	}

	public function getPagerHtml()
	{
		return '';//$this->getChildHtml('pager');
	}

	public function getViewUrl($order)
	{
		return $this->getUrl('*/*/view', array('order_id' => $order->getId()));
	}

	public function getBackUrl()
	{
		return $this->getUrl('customer/account/');
	}
}
