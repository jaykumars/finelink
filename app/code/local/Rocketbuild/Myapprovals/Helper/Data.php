<?php
class Rocketbuild_Myapprovals_Helper_Data extends Mage_Core_Helper_Data
{

    //////////////////////////////////////////////
    //////////// BEGIN EMAILS
    //////////////////////////////////////////////

    private function sendEmail($message, $subject, $to=array(), $toname=array(), $from, $fromname) {


        $mail = Mage::getModel('core/email');

        $mail->setFromEmail($from);
        $mail->setFromName($fromname);
        $mail->setType('text');
        $mail->setBody($message);
        $mail->setSubject($subject);

        foreach ($to as $key => $email) {
            $mail->setToName($toname[$key]);
            $mail->setToEmail($email);

            try {
                $mail->send();
            }
            catch (Exception $e) {
                var_dump($e->getMessage());
                die();
            }
        }
    }

    public function sendApprovalEmail($order) {

        $storeId=Mage::app()->getStore()->getId();
        $customer = $order->getCustomer();
        $approvingCustomer = Mage::getSingleton('customer/session')->getCustomer();
        $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);

        $quote=Mage::getModel('sales/quote')->load($order->getQuoteId());

        $quote->setApprovalApprovedById($approvingCustomer->getId());
        $quote->save();

        $paymentBlock->getMethod()->setStore($storeId);
        $paymentBlockHtml = $paymentBlock->toHtml();
        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $copyTo = Mage::getStoreConfig('sales_email/order/copy_to'); 
        //$bccsArray = Mage::getModel('core/email_info')->getBccEmails();
        //$bccs = implode(" ", $bccsArray);
        if (!empty($copyTo) && isset($copyTo)) { 
              // Add bcc to customer email 
              $emailInfo->addBcc($copyTo);
         }
        $emailInfo->addTo((string)$customer->getEmail(),(string)$customer->getName());
        //$emailInfo->addBcc($copyTo);
        //$emailInfo->addBcc('switch@finelineprintinggroup.com');
        $mailer->addEmailInfo($emailInfo);


        // Set all required params and send emails
        $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Orders'));
        $mailer->setStoreId($storeId);
        // original $mailer->setTemplateId((string) 'myapprovals_approve_order_email_template');
        $mailer->setTemplateId(1);
        $mailer->setTemplateParams(array(
                'order'        => $order,
                'invoice'      => $this,
                'comment'      => $comment,
                'billing'      => $order->getBillingAddress(),
                'payment_html' => $paymentBlockHtml
                )
        );
        //var_dump($copyTo);
        $mailer->send();
    }

    public function sendPartialApprovalEmail($quote, $order) {

        $storeId=Mage::app()->getStore()->getId();
        $customer = $order->getCustomer();
        $approvingCustomer = Mage::getSingleton('customer/session')->getCustomer();
        $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
        $paymentBlock->getMethod()->setStore($storeId);
        $paymentBlockHtml = $paymentBlock->toHtml();

        $quote->setApprovalApprovedById($approvingCustomer->getId());
        $quote->save();

        $itemsString = "<ul>";
        $items = $order->getAllItems();
        foreach ($items as $item) {
            $name = $item->getName();
            $itemsString .= "<li>$name</li>";
        }
        $itemsString .= "</ul>";

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $copyTo = Mage::getStoreConfig('sales_email/order/copy_to'); 
        if (!empty($copyTo) && isset($copyTo)) { 
            // Add bcc to customer email 
            $emailInfo->addBcc($copyTo);
        }
        $emailInfo->addTo((string)$customer->getEmail(),(string)$customer->getName());
        $emailInfo->addBcc('switch@finelineprintinggroup.com');
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Orders'));
        $mailer->setStoreId($storeId);
        //$mailer->setTemplateId(1);
        $mailer->setTemplateId((string) 'myapprovals_approve_part_email_template');
        $mailer->setTemplateParams(array(
            'order'=>$order, 
            'quote'=>$quote, 
            'items'=>$itemsString,
            'billing'      => $order->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
            )
        );
        $mailer->send();
    }

    public function sendRejectionEmail($quote, $note) {

        $storeId=Mage::app()->getStore()->getId();
        $customer = $quote->getCustomer();

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo((string)$customer->getEmail(),(string)$customer->getName());
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Finelink 2.0'));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId((string) 'myapprovals_reject_order_email_template');
        //$mailer->setTemplateId(1);
        $mailer->setTemplateParams(array(
            'order'=>$quote, 
            'note'=>$note

            )
        );
        $mailer->send();
    }

    public function sendPartialRejectionEmail($quote, $items, $note) {

        $storeId=Mage::app()->getStore()->getId();
        $customer = $quote->getCustomer();
        $paymentBlock = Mage::helper('payment')->getInfoBlock($quote->getPayment())
                ->setIsSecureMode(true);
        $paymentBlock->getMethod()->setStore($storeId);
        $paymentBlockHtml = $paymentBlock->toHtml();

        $itemsString = "<ul>";
        foreach ($items as $item) {
            $itemsString .= "<li>$item</li>";
        }
        $itemsString .= "</ul>";

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $copyTo = Mage::getStoreConfig('sales_email/order/copy_to'); 
        if (!empty($copyTo) && isset($copyTo)) { 
            // Add bcc to customer email 
            $emailInfo->addBcc($copyTo);
        }
        $emailInfo->addTo((string)$customer->getEmail(),(string)$customer->getName());
        //$emailInfo->addBcc('switch@finelineprintinggroup.com');
        $mailer->addEmailInfo($emailInfo);

        // Set all required params and send emails
        $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Finelink 2.0'));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId((string) 'myapprovals_reject_part_email_template');
        //$mailer->setTemplateId(1);
        $mailer->setTemplateParams(array(
            'order'=>$quote, 
            'note'=>$note, 
            'items'=>$itemsString,
            'billing'      => $quote->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
            )
        );
        $mailer->send();
    }

    //for gateway first// - we're altering this function to check for the store first to work for Gateway's approval process. 

    public function sendRequisitionReadyEmail($quote) {
        $storeId=Mage::app()->getStore()->getId();
        if ($storeId == 12) {
            $newCostcenter = $quote->getData('customer_cost_center');
            // $newCostcenter = Mage::app()->getRequest()->getParam('customer_cost_center');
            $gatewayCostcenters = Mage::getModel('gatewayapproval/gatewaycostcenters')->getCollection();
            foreach ($gatewayCostcenters as $gatewayCostcenter) {
                if ($gatewayCostcenter->getCostcenter() == $newCostcenter) {
                    $gatewayApprovers = $gatewayCostcenter->getApprovers();
                } 
            }
            $approvalsURL = Mage::getUrl('myapprovals/index/');

            $itemTableHeader = "<td>Name</td>
                      <td>Qty</td>
                      <td>Price</td>";
            $itemsString = "";
            $items = $quote->getAllVisibleItems();
            foreach ($items as $item) {
                $name = $item->getName();
                $qty = $item->getQty();
                $price = $item->getPrice();
                $lotPrice = $qty * $price;
                setlocale(LC_MONETARY, 'en_US.UTF-8');
                $formatPrice = money_format('%.2n', $lotPrice);
                        
                $itemsString .= "<tr style='margin:20px;'><td align='left' valign='top' style='font-size:11px; border-bottom:1px dotted #CCCCCC;'>$name</td>
                                <td align='left' valign='top' style='font-size:11px;  border-bottom:1px dotted #CCCCCC;'>$qty</td>
                                <td align='left' valign='top' style='font-size:11px;  border-bottom:1px dotted #CCCCCC;'>$formatPrice</td></tr>";
            }
            //$itemsString .= "</tr>";
            //$itemsFooter = "</table>";
            $unformatGrandTotal = $quote->getGrandTotal();
            $grandTotal = money_format('%.2n', $unformatGrandTotal);

            /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
            $mailer = Mage::getModel('core/email_template_mailer');
            $emailInfo = Mage::getModel('core/email_info');

            $gatewayAppEmails = explode(",", $gatewayApprovers);
            // Approvers get the email.
            foreach ($gatewayAppEmails as $gatewayAppEmail) {
                $emailInfo->addTo($gatewayAppEmail);
            }
            $mailer->addEmailInfo($emailInfo);

            // Set all required params and send emails
            $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Finelink 2.0'));
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId(15);
            $mailer->setTemplateParams(array(
                'order'=>$quote, 
                'approvalsURL'=>$approvalsURL,
               // 'tax'=>$quote->getShippingAddress()->getBaseTaxAmount();
                //'shipping'=>$quote->getShippingAddress()->getBaseShippingAmount();
                'grandtotal'=>$grandTotal,
                'items'=>$itemsString,
                'itemheader'=>$itemTableHeader,
                'itemfooter'=>$itemsFooter,
                'shippingAddress'=>$quote->getShippingAddress(),
                'billingAddress'=>$quote->getBillingAddress()
                )
            );
            $mailer->send();
        } else {
            $storeId=Mage::app()->getStore()->getId();
            $customer = $quote->getCustomer();
            $approvalsURL = Mage::getUrl('myapprovals/index/');

            $groupId = $customer->getGroupId();
            $group = Mage::getModel('customer/group')->load($groupId);
            $approverId = $group->getFcrApprover();
            $itemTableHeader = "<td>Name</td>
                      <td>Qty</td>
                      <td>Price</td>";
            $itemsString = "";
            $items = $quote->getAllVisibleItems();
            foreach ($items as $item) {
                $name = $item->getName();
                $qty = $item->getQty();
                $price = $item->getPrice();
                $lotPrice = $qty * $price;
                setlocale(LC_MONETARY, 'en_US.UTF-8');
                $formatPrice = money_format('%.2n', $lotPrice);
                        
                $itemsString .= "<tr style='margin:20px;'><td align='left' valign='top' style='font-size:11px; border-bottom:1px dotted #CCCCCC;'>$name</td>
                                <td align='left' valign='top' style='font-size:11px;  border-bottom:1px dotted #CCCCCC;'>$qty</td>
                                <td align='left' valign='top' style='font-size:11px;  border-bottom:1px dotted #CCCCCC;'>$formatPrice</td></tr>";
            }
            //$itemsString .= "</tr>";
            //$itemsFooter = "</table>";
            $unformatGrandTotal = $quote->getGrandTotal();
            $grandTotal = money_format('%.2n', $unformatGrandTotal);
            $approvers = Mage::getModel('customer/customer')->getCollection()
                ->addFieldToFilter('group_id', $approverId);

            /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
            $mailer = Mage::getModel('core/email_template_mailer');
            $emailInfo = Mage::getModel('core/email_info');

            // Approvers get the email.
            foreach ($approvers as $approver) {
                $emailInfo->addTo((string)$approver->getEmail(),(string)$approver->getName());
            }

            $mailer->addEmailInfo($emailInfo);

            // Set all required params and send emails
            $mailer->setSender(array('email'=>(string) 'noreply@finelineprintinggroup.com','name'=> (string)'Finelink 2.0'));
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId((string) 'myapprovals_requisition_ready_email_template');
            $mailer->setTemplateParams(array(
                'order'=>$quote, 
                'approvalsURL'=>$approvalsURL,
               // 'tax'=>$quote->getShippingAddress()->getBaseTaxAmount();
                //'shipping'=>$quote->getShippingAddress()->getBaseShippingAmount();
                'grandtotal'=>$grandTotal,
                'items'=>$itemsString,
                'itemheader'=>$itemTableHeader,
                'itemfooter'=>$itemsFooter,
                'shippingAddress'=>$quote->getShippingAddress(),
                'billingAddress'=>$quote->getBillingAddress()
                )
            );
            $mailer->send();
        }
    }

    //////////////////////////////////////////////
    //////////// END EMAILS
    //////////////////////////////////////////////


    public function getRequisitionsForCustomer($customerId) {
        $quote = Mage::getModel('sales/quote')
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('approval_is_requisition', 1)
        ;
        return $quote;
    }

    public function getRequisitionsForApprovalByCustomer($customerId) {
        $storeId=Mage::app()->getStore()->getId();
        if ($storeId == 12) {

            $custEmail = Mage::getModel('customer/customer')->load($customerId)->getEmail();
            $costcentersApprove = Mage::getModel('gatewayapproval/gatewayapprovers')->getCollection();
            foreach ($costcentersApprove as $costcenterApprove) {
                if ($costcenterApprove->getApprover()==$custEmail) {
                    $costcenterString = $costcenterApprove->getCostcenters();
                }
            }

                if ($costcenterString !== NULL) {
                    $costcenterArray = explode(",", trim($costcenterString, ""));
                    
                $requisitions = Mage::getModel('sales/quote')
                    ->getCollection()
                    ->addFieldToFilter('approval_is_requisition', 1)
                     ->addFieldToFilter('approval_state', array('in' => array(0,3)))
                     ->addFieldToFilter('customer_cost_center', array('in' => $costcenterArray));
                
                return $requisitions;
            }

        } else {
            $groupId = Mage::getModel('customer/customer')->load($customerId)->getGroupId();
            $group = Mage::getModel('customer/group')->load($groupId);
            $approvees = $group->getFcrApprovees();
            $approvalGroupsIds = explode(',', $approvees);

            $customers = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToFilter('group_id', array('in'=>$approvalGroupsIds));

            $customerIds = array();
            foreach ($customers->getData() as $key => $customer) {
                $customerIds[] = $customer['entity_id'];
            }

            $requisitions = Mage::getModel('sales/quote')
                ->getCollection()
                ->addFieldToFilter('customer_id', array('in'=>$customerIds))
                ->addFieldToFilter('approval_is_requisition', 1)
                ->addFieldToFilter('approval_state', array('in' => array(0,3)))
            ;

            return $requisitions;
        }
}

    public function customerCanMakePurchase($customerId, $quoteId) {

        $customer = Mage::getModel('customer/customer')->load($customerId);
        $groupId = $customer->getGroupId();
        $group = Mage::getModel('customer/group')->load($groupId);
        $groupType = $group->getFcrRoletype();

        // First, if they are of senior level, they can always purchase
        if ($groupType == 2) {
            return true;
        }

        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $items = $quote->getAllItems();

        $requiresSenior = false;

        foreach ($items as $item) {
            // @var $item Mage_Sales_Model_Quote_Item
            $product = $item->getProduct();
            $cats = $product->getCategoryIds();
            foreach ($cats as $category_id) {
                $cat = Mage::getModel('catalog/category')->load($category_id) ;
                $requiresSenior = $cat->getRequiresSeniorApproval();
                if ($requiresSenior) {
                    $requiresSenior = true;
                    break;
                }
            }
            if ($requiresSenior) {
                break;
            }
        }

        // Seniors returned true above, so only non seniors remain...so no purchase for you
        if ($requiresSenior) {
            return false;
        }

        // Not a senior, order doesn't require senior, so just check if I'm a purchaser
        if ($groupType == 1) {
            return true;
        }

        // No go for anyone else
        return false;
    }

    public function approvalStateName($stateId) {
        switch ($stateId) {
            case 0:
                return 'Pending Approval';
            break;
            case 1:
                return 'Approved';
            break;
            case 2:
                return 'Denied';
            break;
            case 3:
                return 'Partial Review';
            break;
            case 4:
                return 'Complete';
            break;
        }
    }
}