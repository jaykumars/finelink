<?php
class Cybersource_Paypal_Model_Onepage extends Mage_Checkout_Model_Type_Onepage
{
    public $_checkoutSession;

    /**
     * Specify quote payment method
     *
     * @param   array $data
     * @return  array
     */
    public function savePayment($data)
    {
        parent::savePayment($data);

        //Reserve Order ID for Quote, to use as Merchant Ref in Cybersource.
        if (!$this->getQuote()->getReservedOrderId()) {
            $this->getQuote()->reserveOrderId()->save();
        }

        parent::savePayment($data);
    }


    public function saveOrder() {
        if ($this->getQuote()->getPayment()->getMethod() != 'cybersourcepaypal') {
            parent::saveOrder();
        } else {
            $this->_checkoutSession = Mage::getSingleton('checkout/session');
            $this->validate();
            $isNewCustomer = false;
            switch ($this->getCheckoutMethod()) {
                case self::METHOD_GUEST:
                    $this->_prepareGuestQuote();
                    break;
                case self::METHOD_REGISTER:
                    $this->_prepareNewCustomerQuote();
                    $isNewCustomer = true;
                    break;
                default:
                    $this->_prepareCustomerQuote();
                    break;
            }

            $service = Mage::getModel('sales/service_quote', $this->getQuote());
            $service->submitAll();

            if ($isNewCustomer) {
                try {
                    $this->_involveNewCustomer();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            $this->_checkoutSession->setLastQuoteId($this->getQuote()->getId())
                ->setLastSuccessQuoteId($this->getQuote()->getId())
                ->clearHelperData();

            $order = $service->getOrder();
            if ($order) {
                try {
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true,'Authorised Amount: '.$order->getGrandTotal())->save();
                    $paymentAction = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('payment_action',Mage::app()->getStore()->getId());
                    $response = Mage::getModel('cybersourcepaypal/soapapi_paypal')->payPalEcDoPaymentService($order);
                    if($response){
                        $payment = $order->getPayment();
                        //order successful
                        //set data to payment object
                        $data = array(
                            'merchantReferenceCode' => $response->merchantReferenceCode,
                            'requestID' => $response->requestID,
                            'requestToken' => $response->requestToken,
                            'orderId' => $response->payPalEcDoPaymentReply->orderId,
                            'amount' => $response->payPalEcDoPaymentReply->amount,
                            'transactionId' => $response->payPalEcDoPaymentReply->transactionId,
                            'paypalToken' => $response->payPalEcDoPaymentReply->paypalToken,
                            'currency' => $response->payPalEcDoPaymentReply->currency,
                            'paypalPaymentStatus' => $response->payPalEcDoPaymentReply->paypalPaymentStatus,
                            'correlationID' => $response->payPalEcDoPaymentReply->correlationID
                        );
                        try {
                            $payment->setAdditionalInformation($data)->save();
                            $payment->registerAuthorizationNotification($order->getGrandTotal());
                            $paymentAction = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('payment_action',Mage::app()->getStore()->getId());
                            if ($paymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
                                try {
                                    //do capture if checkout type set to auth + capture.
                                    $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                                    $invoice->register();
                                    $transactionSave = Mage::getModel('core/resource_transaction')
                                        ->addObject($invoice)
                                        ->addObject($invoice->getOrder());
                                    $transactionSave->save();
                                } catch (Exception $e) {
                                    $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                                    $error = Mage::helper('cybersourcepaypal')->__('Could not capture your payment. Please try again or contact us. '.$errorMsg);
                                    $this->_checkoutSession->addError($error);
                                }
                            }
                        } catch (Exception $e) {
                            $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                            $error = Mage::helper('cybersourcepaypal')->__('Could not capture your payment. Please try again or contact us. '.$errorMsg);
                            $this->_checkoutSession->addError($error);
                        }
                    }else{
                        $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true,'Pending Sale Amount of:'. $order->getGrandTotal())->save();
                        $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                        $error = Mage::helper('cybersourcepaypal')->__('Could not authorize amount on your card. Please try again or contact us. '.$errorMsg);
                        $this->_checkoutSession->addError($error);
                    }
                }catch (Exception $e){
                    $this->_checkoutSession->addError($this->__('Could not prepare your order. Please check your PayPal Details.'));
                    Mage::logException($e);
                }
                Mage::dispatchEvent('checkout_type_onepage_save_order_after',
                    array('order'=>$order, 'quote'=>$this->getQuote()));

                /**
                 * a flag to set that there will be redirect to third party after confirmation
                 * eg: paypal standard ipn
                 */
                $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();
                /**
                 * we only want to send to customer about new order when there is no redirect to third party
                 */
                if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
                    try {
                        $order->sendNewOrderEmail();
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

                // add order information to the session
                $this->_checkoutSession->setLastOrderId($order->getId())
                    ->setRedirectUrl($redirectUrl)
                    ->setLastRealOrderId($order->getIncrementId());

                // as well a billing agreement can be created
                $agreement = $order->getPayment()->getBillingAgreement();
                if ($agreement) {
                    $this->_checkoutSession->setLastBillingAgreementId($agreement->getId());
                }
            }

            // add recurring profiles information to the session
            $profiles = $service->getRecurringPaymentProfiles();
            if ($profiles) {
                $ids = array();
                foreach ($profiles as $profile) {
                    $ids[] = $profile->getId();
                }
                $this->_checkoutSession->setLastRecurringProfileIds($ids);
                // TODO: send recurring profile emails
            }

            Mage::dispatchEvent(
                'checkout_submit_all_after',
                array('order' => $order, 'quote' => $this->getQuote(), 'recurring_profiles' => $profiles)
            );
            $smessages = Mage::getSingleton('checkout/session')->getMessages()->getItems();
            if(!empty($smessages))  {
                return false;
            }
            return true;
        }

    }
}