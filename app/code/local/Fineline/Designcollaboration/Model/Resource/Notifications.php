<?php

class Fineline_Designcollaboration_Model_Resource_Notifications extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init('designcollaboration/notifications', 'notification_id');
    }

}