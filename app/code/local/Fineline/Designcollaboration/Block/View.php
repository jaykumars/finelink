<?php

class Fineline_Designcollaboration_Block_View extends Mage_Core_Block_Template
{
    public function showBtnEditDesign($statusId)
    {
        return $statusId != 5;
    }

    public function showBtnSendForApproval($statusId)
    {
        return Mage::helper('designcollaboration')->getRequireApproval() && $statusId == 1;
    }

    public function showBtnAddToShoppingCart($statusId)
    {
        return !Mage::helper('designcollaboration')->getRequireApproval() && $statusId == 1 || $statusId == 4 || $statusId == 5;
    }

    public function showBtnCreateNewDesign($statusId)
    {
        return $statusId == 5;
    }

    public function showBtnApproveDesign($statusId, $designID)
    {
        return Mage::helper('designcollaboration')->getIsApproval($designID) && $statusId == 3;
    }

    public function showBtnPendingApproval($statusId, $designID)
    {
        return !Mage::helper('designcollaboration')->getIsApproval($designID) && $statusId == 3;
    }

    public function showBtnRevokeApproval($statusId, $designID)
    {
        return Mage::helper('designcollaboration')->getIsApproval($designID) && $statusId == 4;
    }

    public function showBtnDenyDesign($statusId, $designID)
    {
        return Mage::helper('designcollaboration')->getIsApproval($designID) && $statusId == 3;
    }

    public function isStatusPending($statusId)
    {
        return $statusId == 3;
    }

    public function isStatusApproved($statusId)
    {
        return $statusId == 4;
    }

}