(function($){

$(function(){
    Event.stopObserving($('#opc-billing .step-title')[0], 'click');
    billing.save();
    $(document).on('DOMNodeInserted', function(e) {
        if (e.target.id == 'billing-progress-opcheckout') {
           $(e.target).find('.changelink').html('<span class="separator">|</span><span>preset</span>');
        }
        $('#shipping-buttons-container .back-link').hide();
    });
    $('#checkout-step-shipping').show();
});

})(jQuery.noConflict());
