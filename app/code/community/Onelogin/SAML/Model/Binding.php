<?php
class Onelogin_SAML_Model_Binding
{
    public function toOptionArray()
    {
    $values = array(
                'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
    );

    $options = array();

    foreach ($values as $value) {
        $option = array('value' => $value, 'label' => $value);
        $options[] = $option;
    }

        return $options;
    }
}
