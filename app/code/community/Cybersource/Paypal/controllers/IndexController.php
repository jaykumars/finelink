<?php
class Cybersource_Paypal_IndexController extends Mage_Core_Controller_Front_Action
{
    const METHOD_REGISTER = 'register';

    /**
     * @var Mage_Checkout_Model_Session
     */
    protected $_checkout = null;

    /**
     * @var Mage_Sales_Model_Quote
     */
    protected $_quote = false;


    protected $_orderRef = null;

    /**
     * @var Mage_Customer_Model_Session
     */
    protected $_customerSession;

    public function indexAction()
    {
        $session = $this->_getCheckout();
        $quoteId = $session->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);

        try {
            //Set Paypal Service via request to CyberSource.
            $response = Mage::getModel('cybersourcepaypal/soapapi_paypal')->payPalEcSetService($quote);

            if($response && $response->reasonCode == Cybersource_Paypal_Model_Source_Consts::STATUS_ACCEPT){
                if(isset($response->payPalEcSetReply->reasonCode) && $response->payPalEcSetReply->reasonCode == Cybersource_Paypal_Model_Source_Consts::REASON_CODE_ACCEPT){

                    //Set PayPal EcSet Reply values to quote_payment.
                    $payment = $quote->getPayment();
                    $data = array(
                        'merchantReferenceCode' => $response->merchantReferenceCode,
                        'requestID' => $response->requestID,
                        'requestToken' => $response->requestToken,
                        'correlationID' => $response->payPalEcSetReply->correlationID
                    );
                    $payment->setAdditionalInformation($data);
                    $payment->save();

                    $quote->save();

                    if($response->payPalEcSetReply->paypalToken){
                        //Redirect to Paypal Sandbox Url.
                        $url = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('sandbox_url').$response->payPalEcSetReply->paypalToken;
                        $this->_redirectUrl($url);
                        return;
                    }else{
                        $session->addError(Mage::helper('cybersourcepaypal')->__('Unable to start Express Checkout. Please try again or contact us.'));
                    }

                }else{
                    $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->payPalEcSetReply->reasonCode);
                    $error = Mage::helper('cybersourcepaypal')->__('Could not set Paypal Express Checkout. Please try again or use a different payment method. '.$errorMsg);
                    $session->addError($error);
                }

            }else{
                $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                $session->addError(Mage::helper('cybersourcepaypal')->__('Could not set Paypal Express Checkout. Please try again or use a different payment method. '.$errorMsg));
            }

        }catch (Exception $e) {
            $session->addError($this->__('Unable to start Express Checkout.'));
            Mage::logException($e);
        }

        $this->_redirect('checkout/');
    }

    public function returnAction()
    {
        $session = $this->_getCheckout();
        $quoteId = $session->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);

        try {
            if(isset($_GET['token']) && $_GET['token'] != null){

                //Get PayPal Details
                $response = Mage::getModel('cybersourcepaypal/soapapi_paypal')->payPalEcGetDetailsService($quote,$_GET['token']);

                if($response){

                    if ($response->reasonCode==Cybersource_Paypal_Model_Source_Consts::STATUS_ACCEPT) {
                        if($response->payPalEcGetDetailsReply->reasonCode==Cybersource_Paypal_Model_Source_Consts::REASON_CODE_ACCEPT){
                            try {

                                $details = $response->payPalEcGetDetailsReply;
                                $tempObject = new Cybersource_Paypal_Model_Soapapi_Abstract();
                                $pullPayPalAddress     = $tempObject->getAddressToCyberSource($quote->getStoreId(),"");
                                $getPaypalAddress = ($pullPayPalAddress==Cybersource_Paypal_Model_Source_AddressDetails::PAYPAL_ADDRESS?true:false);

                                //Set PayPal EcSet Reply values to quote_payment.
                                $payment = $quote->getPayment();
                                $data = array(
                                    'merchantReferenceCode' => $response->merchantReferenceCode,
                                    'requestID' => $response->requestID,
                                    'requestToken' => $response->requestToken,
                                    'paypalToken' => $details->paypalToken,
                                    'payerId' => $details->payerId,
                                    'payer' => $details->payer,
                                    'correlationID' => $details->correlationID);

                                if($getPaypalAddress==true){
                                    $data['payerFirstname'] = $details->payerFirstname;
                                    $data['playerLastname'] = $details->payerLastname;
                                    $data['email'] = $details->payer;

                                    if(property_exists($details,'street1'))
                                        $data['street1'] = $details->street1;
                                    if(property_exists($details,'street2')){
                                        $data['street2'] = $details->street2;
                                    }
                                    if(property_exists($details,'city'))
                                        $data['city'] =  $details->city;
                                    if(property_exists($details,'state'))
                                        $data['state'] =  $details->state;
                                    if(property_exists($details,'postalCode'))
                                        $data['postcode'] = $details->postalCode;
                                    if(property_exists($details,'countryName'))
                                        $data['country'] = $details->countryName;
                                    if(property_exists($details,'payerPhone'))
                                        $data['phonenumber'] = $details->payerPhone;
                                    if(property_exists($details,'addressID'))
                                        $data['company']= $details->addressID;
                                }
                                $payment->setAdditionalInformation($data)->save();
                                $quote->save();

                                $this->_redirect('*/*/review');
                                return;

                            }
                            catch (Mage_Core_Exception $e) {
                                Mage::getSingleton('checkout/session')->addError($e->getMessage());
                            }
                            catch (Exception $e) {
                                Mage::getSingleton('checkout/session')->addError(
                                    $this->__('Unable to initialize Express Checkout review.')
                                );
                                Mage::logException($e);
                            }

                        }else{
                            $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                            $error = Mage::helper('cybersourcepaypal')->__('There is an error in processing the payment. Please try again or contact us. '.$errorMsg);
                            $session->addError($error);
                        }

                    }else{
                        $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($response->reasonCode);
                        $error = Mage::helper('cybersourcepaypal')->__('There is an error in processing the payment. Please try again or contact us. '.$errorMsg);
                        $session->addError($error);
                    }

                }else{
                    $session->addError($this->__('There is an error in processing the payment. Please try again or contact us.'));
                }
            }

        }catch (Exception $e) {
            $session->addError($this->__('Unable to start Express Checkout.'));
            Mage::logException($e);
        }
        //TODO:: Retrieve quote and redirect to basket.
        $this->_redirect('checkout/cart');
    }

    public function reviewAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function placeOrderAction(){
        $onePage = Mage::getSingleton('cybersourcepaypal/onepage');
        $session = Mage::getSingleton('checkout/session');
        $quote = $session->getQuote()->collectTotals()->save();
        try {
                $result = $onePage->setQuote($quote)->saveOrder();
                if ($result) {
                    $this->_redirect('checkout/onepage/success');
                } else {
                    $this->_redirect('checkout/cart');
                }
        }catch (Exception $e){
                $session->addError($this->__('There is an error in processing the payment. Please try again or contact us. '));
                Mage::logException($e);
                $this->_redirect('checkout/cart');
        }
    }


    public function cancelAction()
    {
        $session = $this->_getCheckout();

        if ($session->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($this->_session->getLastRealOrderId());
            if ($order->getId()) {
                $order->cancel()->save();
            }
            Mage::helper('cybersourcepaypal')->restoreQuote();
        }
        $this->_redirect('checkout/cart');;
    }

    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }



}