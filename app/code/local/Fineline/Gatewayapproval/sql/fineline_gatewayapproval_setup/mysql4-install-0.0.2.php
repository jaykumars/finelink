<?php

$installer = $this;

$installer->startSetup();

$quoteTable = $installer->getTable('sales/quote');
$orderTable = $installer->getTable('sales/order');
$table = $installer->getConnection();

$table->addColumn($quoteTable, 'reason_request', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Order Reason Request'
));
$table->addColumn($orderTable, 'reason_request', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Order Reason Request'
));

$installer->run(
    "
    CREATE TABLE IF NOT EXISTS `{$this->getTable('gatewaycostcenters')}` (
      `id` int(10) unsigned NOT NULL auto_increment,
      `costcenter` int(11) NOT NULL,
      `approvers` varchar(255) NOT NULL,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);
$installer->run(
    "
    CREATE TABLE IF NOT EXISTS `{$this->getTable('gatewayapprovers')}` (
      `id` int(10) unsigned NOT NULL auto_increment,
      `approver` varchar(255) NOT NULL,
      `costcenters` varchar(255) NOT NULL,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);

$installer->run(
    "
INSERT INTO `staging_magento`.`gatewaycostcenters` (`id`, `costcenter`, `approvers`) 
VALUES 
(NULL, '71000', 'ACrispeno@GatewayHealthPlan.com, JHynek@GatewayHealthPlan.com'),
(NULL, '71050', 'SBarron@GatewayHealthPlan.com,  MBurroway@GatewayHealthPlan.com'),
(NULL, '73205', 'JSaullo@GatewayHealthPlan.com,  CNeubauer@GatewayHealthPlan.com'),
(NULL, '71100', 'SBarron@GatewayHealthPlan.com,  DMcCloskey@gatewayhealthplan.com'),
(NULL, '71200', 'ACrispeno@GatewayHealthPlan.com,    TLucas@gatewayhealthplan.com'),
(NULL, '71400', 'DClark@gatewayhealthplan.com'),
(NULL, '71420', 'CWebster@gatewayhealthplan.com, JZeff@GatewayHealthPlan.com'),
(NULL, '71500', 'CMcVay@gatewayhealthplan.com'),
(NULL, '71600', 'ACrispeno@GatewayHealthPlan.com,    BPici@gatewayhealthplan.com'),
(NULL, '71700', 'DHunt@gatewayhealthplan.com,    JGlinka@gatewayhealthplan.com'),
(NULL, '71800', 'ACrispeno@GatewayHealthPlan.com,TLucas@gatewayhealthplan.com'),
(NULL, '72000', 'APetka-Simons@GatewayHealthPlan.com, SColaizzi@gatewayhealthplan.com'),
(NULL, '72100', 'APetka-Simons@GatewayHealthPlan.com, PBoody@gatewayhealthplan.com'),
(NULL, '72200', 'MMays@highmarkhealthoptions.com, ACiconte@highmarkhealthoptions.com'),
(NULL, '72210', 'APetka-Simons@GatewayHealthPlan.com, TCampbell@gatewayhealthplan.com'),
(NULL, '72220', 'APetka-Simons@GatewayHealthPlan.com, RWiehagen@gatewayhealthplan.com'),
(NULL, '72230', 'APetka-Simons@GatewayHealthPlan.com,DDaugherty@GatewayHealthPlan.com'),
(NULL, '72300', 'APinigis@GatewayHealthPlan.com, SConley@gatewayhealthplan.com'),
(NULL, '72350', 'APinigis@GatewayHealthPlan.com, BMazzoni@gatewayhealthplan.com'),
(NULL, '72400', 'MMays@highmarkhealthoptions.com,    NDuko@highmarkhealthoptions.com'),
(NULL, '72500', 'EHanzel@GatewayHealthPlan.com'),
(NULL, '72600', 'DSnyder@GatewayHealthPlan.com,  LPreston@GatewayHealthPlan.com'),
(NULL, '72700', 'MMays@highmarkhealthoptions.com,    JHaer@highmarkhealthoptions.com'),
(NULL, '73100', 'DSnyder@GatewayHealthPlan.com,  DWhalen@gatewayhealthplan.com'),
(NULL, '73200', 'RMilligan@gatewayhealthplan.com,    CDavis@gatewayhealthplan.com'),
(NULL, '73300', 'RMilligan@gatewayhealthplan.com,    KBL@GatewayHealthPlan.com'),
(NULL, '73400', 'ACrispeno@GatewayHealthPlan.com,    JBriglia@gatewayhealthplan.com'),
(NULL, '73700', 'RMilligan@gatewayhealthplan.com,    DGesek@GatewayHealthPlan.com'),
(NULL, '73900', 'SBarron@GatewayHealthPlan.com,  MBurroway@GatewayHealthPlan.com'),
(NULL, '74000', 'DOwen@GatewayHealthPlan.com,    SHyland@GatewayHealthPlan.com'),
(NULL, '74100', 'DOwen@GatewayHealthPlan.com,    PYurkovich@gatewayhealthplan.com'),
(NULL, '74110', 'DOwen@GatewayHealthPlan.com,    KMcCrae@gatewayhealthplan.com'),
(NULL, '74120', 'CMcVay@gatewayhealthplan.com'),
(NULL, '74200', 'DOwen@GatewayHealthPlan.com,    DPierce@gatewayhealthplan.com'),
(NULL, '74300', 'WSota@GatewayHealthPlan.com'),
(NULL, '75100', 'EHanzel@GatewayHealthPlan.com,  JKilgore@GatewayHealthPlan.com'),
(NULL, '75200', 'djacobs@gatewayhealthplan.com,  DHurley@GatewayHealthPlan.com'),
(NULL, '75300', 'DRoda@gatewayhealthplan.com ,EAllen@gatewayhealthplan.com'),
(NULL, '76000', 'SSt.John@gatewayhealthplan.com, MCicolini@gatewayhealthplan.com'),
(NULL, '76100', 'JWischmann@gatewayhealthplan.com,   TPil@gatewayhealthplan.com'),
(NULL, '77000', 'DHunt@gatewayhealthplan.com,    RCawley@GatewayHealthPlan.com'),
(NULL, '77100', 'SPeruri@GatewayHealthPlan.com'),
(NULL, '77300', 'DHunt@gatewayhealthplan.com,    RCawley@GatewayHealthPlan.com'),
(NULL, '77500', 'DHunt@gatewayhealthplan.com,    RCawley@GatewayHealthPlan.com'), 
(NULL, '71205', 'LBinda@GatewayHealthPlan.com,    RStevenson@gatewayhealthplan.com'), 
(NULL, '71210', 'ARectenwald@GatewayHealthPlan.com,    JArnott@gatewayhealthplan.com'), 
(NULL, '71215', 'ACrispeno@GatewayHealthPlan.com,    TLucas@gatewayhealthplan.com'),
(NULL, '71230', 'DCoast@GatewayHealthPlan.com,    JFox@gatewayhealthplan.com'),
(NULL, '71900', 'KHahn@gatewayhealthplan.com'),
(NULL, '74105', 'DOwen@GatewayHealthPlan.com,    SHyland@GatewayHealthPlan.com'), 
(NULL, '75305', 'DUsaite@gatewayhealthplan.com') 
 "
);

$installer->run(
    "
INSERT INTO `staging_magento`.`gatewayapprovers` (`id`, `approver`, `costcenters`) 
VALUES 
(NULL, 'ACrispeno@GatewayHealthPlan.com', '71000,71200,71600,71800,73400,71215'),
(NULL, 'JHynek@GatewayHealthPlan.com', '71000'),
(NULL, 'EHanzel@GatewayHealthPlan.com', '72500,75100'),
(NULL, 'TPil@gatewayhealthplan.com', '77000'),
(NULL, 'RCawley@GatewayHealthPlan.com', '77000,77300,77500'),
(NULL, 'SPeruri@GatewayHealthPlan.com', '77100'),
(NULL, 'JWischmann@gatewayhealthplan.com', '76100'),
(NULL, 'MCicolini@gatewayhealthplan.com', '76000'),
(NULL, 'SSt.John@gatewayhealthplan.com', '76000'),
(NULL, 'EAllen@gatewayhealthplan.com', '75300'),
(NULL, 'DRoda@gatewayhealthplan.com', '75300'),
(NULL, 'DHurley@GatewayHealthPlan.com', '75200'),
(NULL, 'djacobs@gatewayhealthplan.com', '75200'),
(NULL, 'JKilgore@GatewayHealthPlan.com', '75100'),
(NULL, 'WSota@GatewayHealthPlan.com', '74300'),
(NULL, 'DPierce@gatewayhealthplan.com', '74200'),
(NULL, 'KMcCrae@gatewayhealthplan.com', '74110'),
(NULL, 'PYurkovich@gatewayhealthplan.com', '74100'),
(NULL, 'SHyland@GatewayHealthPlan.com', '74000, 74105'),
(NULL, 'DOwen@GatewayHealthPlan.com', '74000,74100,74110,74200,74105'),
(NULL, 'DGesek@GatewayHealthPlan.com', '73700'),
(NULL, 'JBriglia@gatewayhealthplan.com', '73400'),
(NULL, 'KBL@GatewayHealthPlan.com', '73300'),
(NULL, 'CDavis@gatewayhealthplan.com', '73200'),
(NULL, 'RMilligan@gatewayhealthplan.com', '73200,73300,73700'),
(NULL, 'DWhalen@gatewayhealthplan.com', '73100'),
(NULL, 'JHaer@highmarkhealthoptions.com', '72700'),
(NULL, 'LPreston@GatewayHealthPlan.com', '72600'),
(NULL, 'DSnyder@GatewayHealthPlan.com', '72600,73100'),
(NULL, 'NDuko@highmarkhealthoptions.com', '72400'),
(NULL, 'BMazzoni@gatewayhealthplan.com', '72350'),
(NULL, 'SConley@gatewayhealthplan.com', '72300'),
(NULL, 'APinigis@GatewayHealthPlan.com', '72300,72350'),
(NULL, 'DDaugherty@GatewayHealthPlan.com', '72230'),
(NULL, 'RWiehagen@gatewayhealthplan.com', '72220'),
(NULL, 'TCampbell@gatewayhealthplan.com', '72210'),
(NULL, 'ACiconte@highmarkhealthoptions.com', '72200'),
(NULL, 'MMays@highmarkhealthoptions.com', '72200,72400,72700'),
(NULL, 'PBoody@gatewayhealthplan.com', '72100'),
(NULL, 'SColaizzi@gatewayhealthplan.com','72000'),
(NULL, 'APetka-Simons@GatewayHealthPlan.com', '72000,72100,72210,72220,72230'),
(NULL, 'JGlinka@gatewayhealthplan.com', '71700'),
(NULL, 'DHunt@gatewayhealthplan.com', '71700,77000,77300,77500'),
(NULL, 'BPici@gatewayhealthplan.com', '71600'),
(NULL, 'CMcVay@gatewayhealthplan.com', '71500,74120'),
(NULL, 'JZeff@GatewayHealthPlan.com', '71420'),
(NULL, 'CWebster@gatewayhealthplan.com', '71420'),
(NULL, 'DClark@gatewayhealthplan.com', '71400'),
(NULL, 'TLucas@gatewayhealthplan.com', '71200,71800, 71215'),
(NULL, 'DMcCloskey@gatewayhealthplan.com', '71100'),
(NULL, 'CNeubauer@GatewayHealthPlan.com', '73205'),
(NULL, 'MBurroway@GatewayHealthPlan.com','71050,73900'),
(NULL, 'JSaullo@GatewayHealthPlan.com', '73205'),
(NULL, 'SBarron@GatewayHealthPlan.com', '71050,71100,73900'),
(NULL, 'LBinda@GatewayHealthPlan.com', '71205'),
(NULL, 'RStevenson@gatewayhealthplan.com', '71205'),
(NULL, 'ARectenwald@GatewayHealthPlan.com', '71210'),
(NULL, 'JArnott@gatewayhealthplan.com', '71210'),
(NULL, 'DCoast@GatewayHealthPlan.com', '71230'),
(NULL, 'KHahn@gatewayhealthplan.com', '71900'),
(NULL, 'DUsaite@gatewayhealthplan.com', '75305'),
(NULL, 'JHynek@GatewayHealthPlan.com', '71000')
 "
);


$installer->endSetup();