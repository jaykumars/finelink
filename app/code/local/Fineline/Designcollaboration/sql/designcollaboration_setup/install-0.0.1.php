<?php

$installer = $this;
$installer->startSetup();

$tableDesigns = $installer->getTable('designcollaboration/designs');
$tableHistory = $installer->getTable('designcollaboration/history');
$tableComments = $installer->getTable('designcollaboration/comments');
$tableCommentStatuses = $installer->getTable('designcollaboration/commentstatuses');
$tableSharings = $installer->getTable('designcollaboration/sharings');
$tableUnregisteredUsers = $installer->getTable('designcollaboration/unregisteredusers');
$tableStatuses = $installer->getTable('designcollaboration/statuses');
$tableNotifications = $installer->getTable('designcollaboration/notifications');

$installer->getConnection()->dropTable($tableDesigns);
$installer->getConnection()->dropTable($tableHistory);
$installer->getConnection()->dropTable($tableComments);
$installer->getConnection()->dropTable($tableCommentStatuses);
$installer->getConnection()->dropTable($tableSharings);
$installer->getConnection()->dropTable($tableUnregisteredUsers);
$installer->getConnection()->dropTable($tableStatuses);
$installer->getConnection()->dropTable($tableNotifications);

$table = $installer->getConnection()
    ->newTable($tableDesigns)
    ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('designname', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => true,
    ))
    ->addColumn('finelink_item_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => true,
    ))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('status_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default' => 1,
    ))
    ->addColumn('locked_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
    ))
    ->addColumn('locked_till', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
    ))
    ->addColumn('locked_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ))
    ->addColumn('created_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('modified_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
    ))
    ->addColumn('modified_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('send_for_approve_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('approved_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => true,
    ))
    ->addColumn('approved_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'approved_by', 'customer/entity', 'entity_id'),
        'approved_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'locked_by', 'customer/entity', 'entity_id'),
        'locked_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'created_by', 'customer/entity', 'entity_id'),
        'created_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'modified_by', 'customer/entity', 'entity_id'),
        'modified_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'send_for_approve_by', 'customer/entity', 'entity_id'),
        'send_for_approve_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'status_id', 'designcollaboration/statuses', 'status_id'),
        'status_id',
        $tableStatuses,
        'status_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/designs', 'product_id', 'catalog/product', 'entity_id'),
        'product_id',
        $installer->getTable('catalog/product'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);


$table = $installer->getConnection()
    ->newTable($tableHistory)
    ->addColumn('history_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('finelink_item_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => true,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ))
    ->addColumn('created_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addForeignKey(
        $installer->getFkName('designcollaboration/history', 'design_id', 'designcollaboration/designs', 'design_id'),
        'design_id',
        $tableDesigns,
        'design_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/history', 'created_by', 'customer/entity', 'entity_id'),
        'created_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);


$table = $installer->getConnection()
    ->newTable($tableComments)
    ->addColumn('comment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('history_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('comment_status_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => true,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ))
    ->addColumn('created_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addForeignKey(
        $installer->getFkName('designcollaboration/comments', 'design_id', 'designcollaboration/designs', 'design_id'),
        'design_id',
        $tableDesigns,
        'design_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/comments', 'created_by', 'customer/entity', 'entity_id'),
        'created_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/comments', 'comment_status_id', 'designcollaboration/commentstatuses', 'comment_status_id'),
        'comment_status_id',
        $tableCommentStatuses,
        'comment_status_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);


$table = $installer->getConnection()
    ->newTable($tableCommentStatuses)
    ->addColumn('comment_status_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => true,
    ));
$installer->getConnection()->createTable($table);

$installer->run("INSERT INTO " . $tableCommentStatuses . " (`comment_status_id`, `name`) VALUES
    (1, 'Design Created'),
    (2, 'Changes Saved'),
    (3, 'Send for Approval'),
    (4, 'Approve Design'),
    (5, 'Deny Design'),
    (6, 'Revoke Approval'),
    (7, 'Design Restored');");


$table = $installer->getConnection()
    ->newTable($tableUnregisteredUsers)
    ->addColumn('unregistered_user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ))
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => false,
    ));
$installer->getConnection()->createTable($table);


$table = $installer->getConnection()
    ->newTable($tableSharings)
    ->addColumn('sharing_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('unregistered_user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => true,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
    ))
    ->addColumn('created_by', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addForeignKey(
        $installer->getFkName('designcollaboration/sharings', 'design_id', 'designcollaboration/designs', 'design_id'),
        'design_id',
        $tableDesigns,
        'design_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/sharings', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/sharings', 'unregistered_user_id', 'designcollaboration/unregisteredusers', 'unregistered_user_id'),
        'unregistered_user_id',
        $tableUnregisteredUsers,
        'unregistered_user_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/sharings', 'created_by', 'customer/entity', 'entity_id'),
        'created_by',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);


$table = $installer->getConnection()
    ->newTable($tableStatuses)
    ->addColumn('status_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, '255', array(
        'nullable' => true,
    ));
$installer->getConnection()->createTable($table);

$installer->run("INSERT INTO " . $tableStatuses . " (`status_id`, `name`) VALUES
    (1, 'Unlocked'),
    (2, 'Locked'),
    (3, 'Pending Approval'),
    (4, 'Approved'),
    (5, 'Completed');");


$table = $installer->getConnection()
    ->newTable($tableNotifications)
    ->addColumn('notification_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addForeignKey(
        $installer->getFkName('designcollaboration/notifications', 'design_id', 'designcollaboration/designs', 'design_id'),
        'design_id',
        $tableDesigns,
        'design_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('designcollaboration/notifications', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id',
        $installer->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );
$installer->getConnection()->createTable($table);

//
$tableDesignQuoteItems = $installer->getTable('designcollaboration/quotes');

$installer->getConnection()->dropTable($tableDesignQuoteItems);

$table = $installer->getConnection()
  ->newTable($tableDesignQuoteItems)
  ->addColumn('design_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable' => false,
    'primary' => true,
  ))
  ->addColumn('quote_item_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable' => false,
    'primary' => true,
  ));
$installer->getConnection()->createTable($table);


//Start "Extended Design Approval"

$installer->getConnection()->dropColumn('core_website', 'dc_enable_design');
$installer->getConnection()->dropColumn('core_website', 'dc_require_design_approval');
$installer->getConnection()->dropColumn('core_website', 'dc_default_design_approval_group');
$installer->getConnection()->dropColumn('core_website', 'dam_access');

$installer->getConnection()->dropColumn('customer_group', 'dc_can_approve_designs');
$installer->getConnection()->dropColumn('customer_group', 'dc_can_approve');
$installer->getConnection()->dropColumn('customer_group', 'dam_access');

$installer->getConnection()->addColumn('core_website', 'dc_enable_design', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => true,
    'default'   => 0,
    'comment'   => 'Enable Design Collaboration'
));

$installer->getConnection()->addColumn('core_website', 'dc_require_design_approval', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => true,
    'default'   => 0,
    'comment'   => 'Require Design Approval'
));

$installer->getConnection()->addColumn('core_website', 'dc_default_design_approval_group', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => false,
    'default'   => -1,
    'comment'   => 'Default Design Approval Group'
));

$installer->getConnection()->addColumn('core_website', 'dam_access', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => true,
    'default'   => 0,
    'comment'   => 'Digital Asset Management'
));

$installer->getConnection()->addColumn('customer_group', 'dc_can_approve_designs', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => true,
    'default'   => 0,
    'comment'   => 'Can Approve Designs'
));

$installer->getConnection()->addColumn('customer_group', 'dc_can_approve_groups', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'comment'   => 'Design Approvees'
));

$installer->getConnection()->addColumn('customer_group', 'dam_access', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
    'nullable'  => false,
    'unsigned'  => true,
    'default'   => 0,
    'comment'   => 'Digital Asset Management'
));

$tableDesignApprovees = $installer->getTable('designcollaboration/approvees');

$installer->getConnection()->dropTable($tableDesignApprovees);

$table = $installer->getConnection()
    ->newTable($tableDesignApprovees)
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'primary' => true,
    ))
    ->addColumn('approver_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'primary' => true,
    ));
$installer->getConnection()->createTable($table);

//End "Extended Design Approval"

$installer->endSetup();
