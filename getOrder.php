<?php


// $callbackUrl = "http://localhost:80/finelink/oauth_customer.php";
// $temporaryCredentialsRequestUrl = "http://localhost:8888/finelink/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
// $adminAuthorizationUrl = 'http://localhost:80/finelink/oauth/authorize';
// $accessTokenRequestUrl = 'http://localhost:80/finelink/oauth/token';
// $apiUrl = 'http://localhost:80/finelink/api/rest';
// $consumerKey = '904d4b603fd96c4bee1ca1ef55f3d02a';
// $consumerSecret = '4618473e6bce56da708996004b14d30c';
$callbackUrl = "http://localhost:8888/finelink/oauth_customer.php";
$temporaryCredentialsRequestUrl = "http://localhost:8888/finelink/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
$adminAuthorizationUrl = 'http://localhost:8888/finelink/oauth/authorize';
$accessTokenRequestUrl = 'http://localhost:8888/finelink/oauth/token';
$apiUrl = 'http://localhost:8888/finelink/api/rest';
$consumerKey = '904d4b603fd96c4bee1ca1ef55f3d02a';
$consumerSecret = '4618473e6bce56da708996004b14d30c';

session_start();
if (!isset($_GET['oauth_token']) && isset($_SESSION['state']) && $_SESSION['state'] == 1) {
    $_SESSION['state'] = 0;
}
try {
    $authType = ($_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
    $oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
    $oauthClient->enableDebug();

    if (!isset($_GET['oauth_token']) && !$_SESSION['state']) {
        $requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);
        $_SESSION['secret'] = $requestToken['oauth_token_secret'];
        $_SESSION['state'] = 1;
        header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
        exit;
    } else if ($_SESSION['state'] == 1) {
        $oauthClient->setToken($_GET['oauth_token'], $_SESSION['secret']);
        $accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
        $_SESSION['state'] = 2;
        $_SESSION['token'] = $accessToken['oauth_token'];
        $_SESSION['secret'] = $accessToken['oauth_token_secret'];
        header('Location: ' . $callbackUrl);
        exit;
    } else {
        $oauthClient->setToken($_SESSION['token'], $_SESSION['secret']);
        $resourceUrl = "$apiUrl/products";
        $oauthClient->fetch($resourceUrl);
        $productsList = json_decode($oauthClient->getLastResponse());
        print_r($productsList);
    }
} catch (OAuthException $e) {
    print_r($e);
}

//testing testing 11_23
