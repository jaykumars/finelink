<?php

class Fineline_Designcollaboration_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{

    public function getprofilesAction()
    {
        $result['result'] = '';
        try {
            $webservice = Mage::getModel('designcollaboration/api');

            if ($this->getRequest()->getParam('website') && $this->getRequest()->getParam('website') != 'null'){
                $webservice->setWebsite($this->getRequest()->getParam('website'));
            } else {
                $webservice->setWebsite(null);
            }

            $params = $this->getRequest()->getParams('element');
            $blockParams['element'] = $params['element'];

            $xml = new SimpleXMLElement($webservice->searchForResource('ImageConversionProfiles'));

            if ($xml->item->count()) {
                $result['result'] .= $this->getLayout()
                    ->createBlock('designcollaboration/adminhtml_profiles_modal')
                    ->setTemplate('designcollaboration/profiles/modal.phtml')
                    ->setItem($xml)
                    ->setParams($blockParams)
                    ->toHtml();
            } else {
                $result['result'] .= 'No resources found';
            }

            $result['status'] = 'success';
        } catch (Exception $e) {
            $result['status'] = 'error';
            $result['result'] = '<ul class="messages"><li class="error-msg"><ul><li><span>' . $e->getMessage() . '</span></li></ul></li></ul>';
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

    /**
     * returns a json object with tree structure
     */
    public function getresourceseditorAction()
    {

        $result = Array();

        $result['result'] = '';
        try {
            $webservice = Mage::getModel('designcollaboration/api');

            if ($this->getRequest()->getParam('website') && $this->getRequest()->getParam('website') != 'null') {
                $webservice->setWebsite($this->getRequest()->getParam('website'));
            } else {
                $webservice->setWebsite(null);
            }

            $params = $this->getRequest()->getParams('element');
            $blockParams['visibility'] = 'open';
            $blockParams['element'] = $params['element'];
            $blockParams['resourceType'] = $params['type'];

            if ($params['type'] === 'Documents') {
                $xml = new SimpleXMLElement($webservice->getResourceTree($params['type']));
            } else {
                $xml = new SimpleXMLElement($webservice->searchForResource($params['type']));
            }

            $this->loadLayout();

            if ($xml->item->count()) {
                $result['result'] .= $this->getLayout()
                    ->createBlock('designcollaboration/adminhtml_resourcebrowser_modal')
                    ->setTemplate('designcollaboration/resourcebrowser/modal.phtml')
                    ->setItem($xml)
                    ->setParams($blockParams)
                    ->toHtml();
            } else {
                $result['result'] .= 'No resources found';
            }

            $result['status'] = 'success';
        } catch (Exception $e) {
            $result['status'] = 'error';
            $result['result'] = '<ul class="messages"><li class="error-msg"><ul><li><span>' . $e->getMessage() . '</span></li></ul></li></ul>';
        }
        $this->getResponse()->setBody(Zend_Json::encode($result));
    }

}
