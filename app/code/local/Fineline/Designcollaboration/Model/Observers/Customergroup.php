<?php
class Fineline_Designcollaboration_Model_Observers_Customergroup extends Varien_Event_Observer
{
    /**
     * Used to track recursive saves.
     * @var boolean
     */
    private $recursed = false;
    private $formDefaultValues = array();

    //Needed for setting default values
    private function dcCanApproveDesignsScript() {
        return <<<EOT
            <script type="text/javascript">
                var website_list = [];
                
                function disableApproveGroups(is_disabled, website_id) {
                    jQuery('input[name="dc_can_approve_groups_' + website_id + '[]"]').attr('disabled', is_disabled);
                }
                
                function toggleApproveGroups(is_visible, website_id) {
                    var form_element = jQuery('input[name="dc_can_approve_groups_' + website_id + '[]"]');
                    if (!is_visible) {
                        form_element.eq(0).parents('tr').hide();
                    } else {
                        form_element.eq(0).parents('tr').show();
                    }
                }
                
                function toggleCanApproveDesigns(is_visible, website_id) {
                    var form_element = jQuery('#dc_can_approve_designs_' + website_id);
                    if (!is_visible) {
                        form_element.parents('tr').hide();
                        toggleApproveGroups(false, website_id);
                    } else {
                        if (form_element.length) {
                            toggleDesignCollaborationFormset(true);
                            form_element.parents('tr').show();
                            toggleApproveGroups(form_element.find('option:selected').val() == 1, website_id);
                            disableApproveGroups(form_element.find('option').length == 1, website_id);
                        } else {
                            toggleDesignCollaborationFormset(false);
                        }
                    }
                }
                
                function toggleDesignCollaborationFormset(is_visible) {
                    if (!is_visible) {
                        jQuery('#dc_fieldset').hide().prev().hide();
                        
                    } else {
                        jQuery('#dc_fieldset').show().prev().show();
                    }
                }
                
                function prepareDesignCollaborationFormset() {
                    var selected_website = jQuery('#fcr_store :selected').val();
                    toggleDesignCollaborationFormset(selected_website == 0);
                    for (var index = 0; index < website_list.length; index++) {
                        toggleCanApproveDesigns(selected_website == website_list[index], website_list[index]);
                    }
                }
                
                function dcCanApproveDesignsToggle(website_id) {
                    toggleApproveGroups(jQuery('#dc_can_approve_designs_' + website_id + ' :selected').val() == 1, website_id);
                }
                
                jQuery(document).ready(function() {
                    jQuery('#fcr_store option').each(function() {
                        website_list.push(jQuery(this).val());
                    });
                    jQuery('#fcr_store').bind("change", function() {
                        prepareDesignCollaborationFormset();
                    });
                    prepareDesignCollaborationFormset();
                });
            </script>
EOT;
    }

    private function prepareFormDefaultValues()
    {
        $this->formDefaultValues['yesno'] = array(
            array(
                'value' => 0,
                'label' => Mage::helper('core')->__('No'),
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('core')->__('Yes'),
            )
        );
    }

    private function prepareDesignCollaborationFormValues($storedData, $website)
    {
        $result = array();

        //Check for default group
        $result['can_change_approve_designs'] = true;
        if ($website['dc_require_design_approval'] == '1' && $website['dc_default_design_approval_group'] == $storedData['customer_group_id']) {
            $result['can_change_approve_designs'] = false;
        }

        //--------------------------------------------------------------------------------------------------------------
        if ($result['can_change_approve_designs']) {
            $result['yesno__can_approve_designs'][] = array(
                'value' => 0,
                'label' => Mage::helper('core')->__('No'),
            );
        }
        $result['yesno__can_approve_designs'][] = array(
            'value' => 1,
            'label' => Mage::helper('core')->__('Yes'),
        );

        //--------------------------------------------------------------------------------------------------------------
        $result['yesno__can_approve_designs_value'] = 0;
        if ($website['website_id'] == $storedData['fcr_store']) {
            $result['yesno__can_approve_designs_value'] = $result['can_change_approve_designs'] ? $storedData['dc_can_approve_designs'] : 1;
        }

        //--------------------------------------------------------------------------------------------------------------
        $result['customer_groups'] = array();
        $customerGroups = Mage::getModel('customer/group')
            ->getCollection()
            ->addFieldToFilter('customer_group_id', array('neq' => $storedData['customer_group_id']))
            ->addFieldToFilter('customer_group_id', array(
                'neq' => $website['dc_require_design_approval'] == '1' ? $website['dc_default_design_approval_group'] : -1
            ))
            ->addFieldToFilter('fcr_store', $website['website_id'])
            ->addFieldToFilter('dc_can_approve_designs', 0)
            ->getData();
        foreach($customerGroups as $value) {
            $result['customer_groups'][] = array(
                'value' => $value['customer_group_id'],
                'label' => $value['customer_group_code']
            );
        }

        //--------------------------------------------------------------------------------------------------------------
        $result['customer_groups_value'] = array();
        if ($result['can_change_approve_designs']) {
            if ($website['website_id'] == $storedData['fcr_store']) {
                $approveGroups = explode(',', $storedData['dc_can_approve_groups']);
                foreach ($approveGroups as $value) {
                    $result['customer_groups_value'][] = $value;
                }
            }
        } else {
            foreach ($result['customer_groups'] as $value) {
                $result['customer_groups_value'][] = $value['value'];
            }
        }

        return $result;
    }

    private function prepareDesignCollaborationFormset($storedData, $dcFieldSet, $website)
    {
        if ($website['dc_require_design_approval'] == '1') {
            $formDefaultValues = $this->prepareDesignCollaborationFormValues($storedData, $website);

            $dcFieldSet->addField('dc_can_approve_designs_'.$website['website_id'], 'select', array(
                'name' => 'dc_can_approve_designs_'.$website['website_id'],
                'label' => Mage::helper('customer')->__('Can Approve Designs'),
                'title' => Mage::helper('customer')->__('Can Approve Designs'),
                'required' => true,
                'disabled' => !$formDefaultValues['can_change_approve_designs'],
                'values' => $formDefaultValues['yesno__can_approve_designs'],
                'value' => $formDefaultValues['yesno__can_approve_designs_value'],
                'onchange' => 'dcCanApproveDesignsToggle('.$website['website_id'].')',
            ));

            $dcFieldSet->addField('dc_can_approve_groups_'.$website['website_id'], 'checkboxes', array(
                'name' => 'dc_can_approve_groups_'.$website['website_id'].'[]',
                'label' => Mage::helper('customer')->__('Design Approvees'),
                'title' => Mage::helper('customer')->__('Design Approvees'),
                'values' => $formDefaultValues['customer_groups'],
                'value' => $formDefaultValues['customer_groups_value'],
            ));
        }
    }

    public function appendCustomForm(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'adminhtml/customer_group_edit_form') {
            $form = $block->getForm();
            $storedData = Mage::registry('current_group')->getData();
            $this->prepareFormDefaultValues();

            //----------------------------------------------------------------------------------------------------------
            // Design Collaboration
            $dcFieldSet = $form->addFieldset('dc_fieldset', array(
                'legend' => Mage::helper('customer')->__('Design Collaboration'),
            ));

            $websites = Mage::getModel('core/website')->getCollection()->getData();
            foreach($websites as $value) {
                $this->prepareDesignCollaborationFormset($storedData, $dcFieldSet, $value);
            }

            //----------------------------------------------------------------------------------------------------------
            // Digital Asset Management
            $damFieldSet = $form->addFieldset('dam_fieldset', array(
                'legend' => Mage::helper('customer')->__('Digital Asset Management'),
            ));

            $damAccess = $damFieldSet->addField('dam_access', 'select', array(
                'name' => 'dam_access',
                'label' => Mage::helper('customer')->__('DAM Access'),
                'title' => Mage::helper('customer')->__('DAM Access'),
                'class' => '',
                'required' => true,
                'values' => $this->formDefaultValues['yesno'],
                'value' => $storedData['dam_access'],
                'after_element_html' => $this->dcCanApproveDesignsScript(),
            ));
        }
    }

    /**
     * Runs whenever a Customer Group is saved. Relies on being a singleton to avoid infinite loop.
     * @param  Varien_Event_Observer $observer Magento observer object.
     * @return none Returns early if recursive saves are detected.
     */
    public function onCustomerGroupSave(Varien_Event_Observer $observer)
    {
        //Taken from Customerroles module
        if ($this->recursed) {
            return;
        }

        $this->recursed = true;
        $savedGroup = $observer->getEvent()->getObject();
        $id = $savedGroup->getId();

        $dcCanApproveDesigns = Mage::app()->getRequest()->getParam('dc_can_approve_designs_'.Mage::app()->getRequest()->getParam('fcr_store'));
        $dcCanApproveGroups = Mage::app()->getRequest()->getParam('dc_can_approve_groups_'.Mage::app()->getRequest()->getParam('fcr_store'));
        $damAccess = Mage::app()->getRequest()->getParam('dam_access');

        $write = Mage::getSingleton( 'core/resource' )->getConnection( 'core_write' );
        $query = "UPDATE customer_group
                  SET dc_can_approve_designs = :dc_can_approve_designs,
                      dc_can_approve_groups = :dc_can_approve_groups,
                      dam_access = :dam_access
                  WHERE customer_group_id = :customer_group_id";
        $binds = array(
            'dc_can_approve_designs' => $dcCanApproveDesigns,
            'dc_can_approve_groups' => $dcCanApproveDesigns == '0' ? '' : implode(',', $dcCanApproveGroups),
            'dam_access' => $damAccess,
            'customer_group_id' => $id
        );
        $write->query($query, $binds);

        //Save into DB list of approvers
        $query = "DELETE FROM designcollaboration_design_approvees
                  WHERE group_id = :group_id";
        $binds = array(
            'group_id' => $id
        );
        $write->query($query, $binds);

        if ($dcCanApproveDesigns == '1') {
            foreach ($dcCanApproveGroups as $approver) {
                $query = "INSERT INTO designcollaboration_design_approvees(group_id, approver_group_id)
                          VALUES(:group_id, :approver_group_id)";
                $binds = array(
                    'group_id' => $id,
                    'approver_group_id' => $approver
                );
                $write->query($query, $binds);
            }
        }
    }
}
