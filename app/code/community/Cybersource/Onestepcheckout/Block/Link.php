<?php

class Cybersource_Onestepcheckout_Block_Link extends Mage_Checkout_Block_Onepage_Link
{
    /**
     * Adds a link to the one step checkout
     *
     * @return Cybersource_Onestepcheckout_Block_Link
     */
    public function addOnestepCheckoutLink()
    {
        if (!$this->helper('cybersource_onestepcheckout')->oneStepCheckoutEnabled()) {
            return $this;
        }

        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Cybersource_Onestepcheckout')) {
            $text = $this->__('Checkout in One Step');
            $parentBlock->addLink(
                $text,
                'checkout/onestep',
                $text,
                true,
                array('_secure' => true),
                60,
                null,
                'class="top-link-checkout"'
            );
        }
        return $this;
    }

    function isPossibleOnestepcheckout()
    {
        return $this->helper('cybersource_onestepcheckout')->oneStepCheckoutEnabled();
    }

    function getCheckoutUrl()
    {
        if($this->isPossibleOnestepcheckout()){
            return $this->getUrl('checkout/onestep', array('_secure'=>true));

        }
        return parent::getCheckoutUrl();
    }
}
