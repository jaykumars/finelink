<?php

class Fineline_Designcollaboration_Model_Comments extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/comments');
    }

    public function addPost($designId, $customerId, $message = null, $commentsStatusId = null, $historyId = null)
    {
        $postData = array('design_id' => $designId,
            'history_id' => $historyId,
            'comment_status_id' => $commentsStatusId,
            'comment' => $message,
            'created_at' => Mage::getModel('core/date')->timestamp(time()),
            'created_by' => $customerId);
        $commentsModel = Mage::getModel('designcollaboration/comments')->setData($postData);

        try {
            $commentsModel->save();
            Mage::getModel('designcollaboration/notifications')->sendNotifications($designId, $customerId);
        } catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError(
                $this->__('Error on added comment: %s', $e->getMessage())
            );
        }
    }

    public function updateLastCommentByHistoryId($designId, $newHistoryId) {

        $lastComment = Mage::getModel('designcollaboration/comments')->getCollection()
            ->addFieldToFilter('design_id', $designId)
            ->addFieldToFilter('comment_status_id', array('in' => array(1,2,7)))
            ->setOrder('created_at', 'DESC')
            ->setPageSize(1)
            ->getFirstItem()
            ->getData();

        if (count($lastComment) > 0) {

            $updateData = array('history_id' => $newHistoryId);
            $updateComments = Mage::getModel('designcollaboration/comments')->load($lastComment['comment_id'])->addData($updateData);

            try {
                $updateComments->setId($lastComment['comment_id'])->save();
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }

}