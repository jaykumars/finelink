<?php
class Rocketbuild_Reports_Block_Index extends Mage_Core_Block_Template{

	public function __construct()
	{
		parent::__construct();

		$this->setTemplate('reports/index.phtml');

	}

	public function getOrdersColumns($moduleModel) {
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		/**
		 * Get the table name
		 */
		$tableName = $resource->getTableName($moduleModel);

		$saleafield=$readConnection->describeTable($tableName);

		return $saleafield;
	}
}