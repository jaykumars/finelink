<?php

class Rocketbuild_Reports_Helper_Export extends Mage_Core_Helper_Abstract {
	/**
	 * Contains current collection
	 * @var string
	 */
	protected $_list = null;

	public function __construct() {

	}

	/**
	 * Sets current collection
	 *
	 * @param $query
	 */
	public function setList( $collection ) {
		$this->_list = $collection;
	}

	/**
	 * Returns indexes of the fetched array as headers for CSV
	 *
	 * @param array $products
	 *
	 * @return array
	 */
	protected function _getCsvHeaders( $products ) {
		$product = current( $products );
		$headers = array_keys( $product );

		return $headers;
	}

	protected function _getInventoryLevelCollection( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {
		$storeId = Mage::app()->getStore()->getId();
		$collection = Mage::getModel( 'catalog/product' )
		                  ->getCollection()
						  ->addStoreFilter($storeId)
						//   ->addAttributeToFilter('created_at', array('from'=>$date_range_start, 'to'=>$date_range_end))
						  ->addAttributeToSelect('*')
		                  ->joinField(
			                  'qty',
			                  'cataloginventory/stock_item',
			                  'qty',
			                  'product_id=entity_id',
			                  '{{table}}.stock_id=1',
			                  'left'
		                  );

		$items = array();
		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}
			}
			$items[] = $i;
		}

		return $items;
	}


	protected function _getOrdersCollection( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {

		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('sales/order')
			->getCollection()
			->addAttributeToSelect('*')
			->addFieldToFilter('store_id', $storeId)
			->addFieldToFilter('created_at', array('from'=>$date_range_start, 'to'=>$date_range_end))
		;

		$items = array();
		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}

				// special cases

				// Addresses
				$addressKeys = array(
					"fax",
					"region",
					"postcode",
					"lastname",
					"street",
					"city",
					"telephone",
					"country_id",
					"company",
					"middlename",
					"firstname"
				);

				if ($attributeKey == 'billing_address') {
					$billingAddressData = $item->getBillingAddress()->getData();
					foreach ($addressKeys as $key) {
						if (array_key_exists($key, $billingAddressData)) {
							$i['billing_'.$key] = $billingAddressData[$key];
						}
					}
				}

				if ($attributeKey == 'shipping_address') {
					$shippingAddressData = $item->getShippingAddress()->getData();
					foreach ($addressKeys as $key) {
						if (array_key_exists($key, $shippingAddressData)) {
							$i['shipping_'.$key] = $shippingAddressData[$key];
						}
					}
				}
			}
			$items[] = $i;
		}

		return $items;
	}

	protected function _getSalesByProductCollection( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {
		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getResourceModel('reports/product_collection')
		    ->addOrderedQty($date_range_start, $data_range_end)
			->addStoreFilter($storeId)
			// ->addAttributeToFilter('created_at', array('from'=>$date_range_start, 'to'=>$date_range_end))
		    ->setOrder('ordered_qty', 'desc');

		$items = array();
		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}
			}
			$items[] = $i;
		}

		return $items;
	}

	protected function _getDigitalDownloads( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {
		$storeId = Mage::app()->getStore()->getId();
		$storeProducts = Mage::getResourceModel('catalog/product_collection')
		    ->addStoreFilter($storeId)
		    ->addAttributeToSelect('entity_id')
			->addAttributeToFilter('type_id', 'downloadable')
		;

		$productIds = array();
		foreach ($storeProducts as $product) {
			$productIds[] = $product->getEntityId();
		}

		$collection = Mage::getModel('downloadable/link')->getCollection()
			->addFieldToFilter('product_id', array('in'=>$productIds))
		;

		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}
			}
			$items[] = $i;
		}
	}

	protected function _getOpenRequisitions ( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {

		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getModel('sales/quote')->getCollection()
			->addFieldToFilter('store_id', $storeId)
			->addFieldToFilter('created_at', array('from'=>$date_range_start, 'to'=>$date_range_end))
			->addFieldToFilter('approval_is_requisition', 1)
			->addFieldToFilter('approval_state', array('in'=>array(0, 3)));

		$items = array();
		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}
			}
			$items[] = $i;
		}

		return $items;

	}

	protected function _getUserActivityCollection( $attributesToSelect = [ ], $date_range_start, $date_range_end ) {

		$storeId = Mage::app()->getStore()->getId();

		$collection = Mage::getResourceModel('customer/customer_collection')
		                  ->addFieldToFilter('store_id', $storeId);

		// Retrieve data from log_customer
		$sql = '(SELECT MAX(logout_at) FROM log_customer WHERE log_customer.customer_id = e.entity_id GROUP BY log_customer.customer_id)';


		$collection->getSelect()->columns(array('last_login' => new Zend_Db_Expr($sql)));

		$items = array();
		foreach ($collection as $item) {
			$itemData = $item->getData();
			$i = array();
			foreach($attributesToSelect as $attributeKey) {
				if (array_key_exists($attributeKey,$itemData)) {
					$i[$attributeKey] = $itemData[$attributeKey];
				}
			}
			$items[] = $i;
		}

		return $items;
	}

	/**
	 * Generates CSV file of orders
	 *
	 * @param array $attributesToSelect
	 *
	 * @param $key
	 *
	 * @return array
	 */
	public function generateCSV( $attributesToSelect = [ ], $key, $date_range_start, $date_range_end ) {
		$collection = null;

		$date_range_start = date('Y-m-d H:i:s', strtotime($date_range_start));
		$date_range_end = date('Y-m-d H:i:s', strtotime($date_range_end));

		switch ( $key ) {
			case "salesByProduct":
				$collection = $this->_getSalesByProductCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "orders":
				$collection = $this->_getOrdersCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "inventoryLevels":
				$collection = $this->_getInventoryLevelCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "userActivity":
				$collection = $this->_getUserActivityCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "openRequisitions":
				$collection = $this->_getOpenRequisitions( $attributesToSelect, $date_range_start, $date_range_end );
				break;
		}

		$this->setList( $collection );

		if ( ! is_null( $this->_list ) ) {
			$items = $this->_list;

			$io   = new Varien_Io_File();
			$path = Mage::getBaseDir( 'var' ) . DS . 'export' . DS;
			$name = md5( microtime() );
			$file = $path . DS . $name . '.csv';
			$io->setAllowCreateFolders( true );
			$io->open( array( 'path' => $path ) );
			$io->streamOpen( $file, 'w+' );
			$io->streamLock( true );

			if ( count( $items ) > 0 ) {
				$io->streamWriteCsv( $this->_getCsvHeaders( $items ) );
				foreach ( $items as $product ) {
					$io->streamWriteCsv( $product );
				}
			}else {
				$io->streamWriteCsv( $attributesToSelect );
			}

			$io->streamClose();

			return array(
				'type'  => 'filename',
				'value' => $file,
				'rm'    => true // can delete file after use
			);
		}
	}

	/**
	 * Generates Excel XML file of orders
	 *
	 * @param array $attributesToSelect
	 *
	 * @return array
	 */
	public function generateXML( $attributesToSelect = [ ], $key, $date_range_start, $date_range_end ) {
		require_once('XLSX/xlsxwriter.class.php');
		$collection = null;

		$date_range_start = date('Y-m-d H:i:s', strtotime($date_range_start));
		$date_range_end = date('Y-m-d H:i:s', strtotime($date_range_end));

		switch ( $key ) {
			case "salesByProduct":
				$collection = $this->_getSalesByProductCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "orders":
				$collection = $this->_getOrdersCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "inventoryLevels":
				$collection = $this->_getInventoryLevelCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "userActivity":
				$collection = $this->_getUserActivityCollection( $attributesToSelect, $date_range_start, $date_range_end );
				break;
			case "openRequisitions":
				$collection = $this->_getOpenRequisitions( $attributesToSelect, $date_range_start, $date_range_end );
		}

		$this->setList( $collection );

		if ( ! is_null( $this->_list ) ) {
			$items = $this->_list;

			$path = Mage::getBaseDir( 'var' ) . DS . 'export' . DS;
			$name = md5( microtime() );
			$file = $path . DS . $name . '.xlsx';

			if ( count( $items ) > 0 ) {
				$headers = $this->_getCsvHeaders( $items );
				array_unshift($items,$headers);
			} else {
				$items = array($attributesToSelect);
			}

			$writer = new XLSXWriter();
			$writer->writeSheet($items);
			$writer->writeToFile($file);

			return array(
				'type'  => 'filename',
				'value' => $file,
				'rm'    => true // can delete file after use
			);
		}
	}
}
