<?php

$installer = $this;
$installer->startSetup();

$quoteTable = $installer->getTable('sales/quote');
$table = $installer->getConnection();

$this->addAttribute('catalog_category', 'requires_senior_approval', array(
    'group'             => 'General Information',
    'type'              => 'int',//can be int, varchar, decimal, text, datetime
    'backend'           => '',
    'frontend_input'    => '',
    'frontend'          => '',
    'label'             => 'Requires senior approval?',
    'input'             => 'select', //text, textarea, select, file, image, multilselect
    'default' => array(0),
    'class'             => '',
    'source'            => 'eav/entity_attribute_source_boolean',//this is necessary for select and multilelect, for the rest leave it blank
    'global'             => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,//scope can be SCOPE_STORE or SCOPE_GLOBAL or SCOPE_WEBSITE
    'visible'           => true,
    'frontend_class'     => '',
    'required'          => false,//or true
    'user_defined'      => true,
    'default'           => '',
    'position'            => 100,//any number will do
));

$table->addColumn($quoteTable, 'approval_state', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Approval State'
));

$table->addColumn($quoteTable, 'approval_reject_note', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => false,
  'comment' => 'Reject Note'
));

$table->addColumn($quoteTable, 'approval_approve_note', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => false,
  'comment' => 'Approve Note'
));

$table->addColumn($quoteTable, 'approval_approved_items', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => false,
  'comment' => 'Approved Items'
));

$table->addColumn($quoteTable, 'approval_rejected_items', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => false,
  'comment' => 'Approved Items'
));

$table->addColumn($quoteTable, 'approval_is_requisition', array(
  'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
  'nullable' => false,
  'comment' => 'Is Requisition'
));

$table->addColumn($quoteTable, 'approval_approved_by_id', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Approval Approved By Id'
));

$installer->endSetup();
