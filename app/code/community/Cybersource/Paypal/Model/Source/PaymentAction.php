<?
class Cybersource_Paypal_Model_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE,
                'label' =>Mage::helper('cybersourcepaypal')->__('Authorize')
            ),
            array(
                'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('cybersourcepaypal')->__('Authorize and Capture')
            ),
        );
    }
}