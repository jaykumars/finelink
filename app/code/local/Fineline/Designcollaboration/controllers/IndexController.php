<?php

class Fineline_Designcollaboration_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction()
    {
        if (Mage::helper('designcollaboration')->isEnabledModule()) {
            if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            }

            $this->loadLayout();
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }

    public function viewAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();

        if (Mage::helper('designcollaboration')->isEnabledModule() && $designModel->getId() > 0) {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                $this->loadLayout();
                $this->getLayout()->getBlock('designcollaboration.content')->assign(array(
                    "isAccess" => Mage::helper('designcollaboration')->checkCustomerAccess($designId),
                    "design" => $designModel,
                    "urlImage" => Mage::helper('designcollaboration')->getViewImages($designModel->getFinelinkItemId(), 'preview')
                ));
                $this->getLayout()->getBlock('designcollaboration.comments')->assign(array(
                    "design" => $designModel,
                ));
                $this->renderLayout();
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function historyAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();

        if (Mage::helper('designcollaboration')->isEnabledModule() && $designModel->getId() > 0) {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                $dataHistory = array();
                $designHistory = Mage::getModel('designcollaboration/history')->getCollection()
                    ->addFieldToFilter('design_id', $designId)
                    ->setOrder('created_at', 'DESC')
                    ->getData();

                if (count($designHistory) > 0) {
                    foreach ($designHistory as $item) {
                        $dataHistory[] = array(
                            'history_id' => $item['history_id'],
                            'url_img_full' => Mage::helper('designcollaboration')->getViewImages($item['finelink_item_id'], 'fullscreen'),
                            'url_img_min' => Mage::helper('designcollaboration')->getViewImages($item['finelink_item_id'], 'list'),
                            'last_comment' => Mage::helper('designcollaboration')->getLastComment($designId, 0, $item['history_id']),
                            'created_at' => $item['created_at'],
                            'created_by' => $item['created_by']);
                    }
                }

                $this->loadLayout();
                $this->getLayout()->getBlock('designcollaboration.content')->assign(array(
                    "isAccess" => Mage::helper('designcollaboration')->checkCustomerAccess($designId),
                    "design" => $designModel,
                    "dataHistory" => $dataHistory
                ));
                $this->renderLayout();
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function editAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();

        if (Mage::helper('designcollaboration')->isEnabledModule() && $designModel->getId() > 0) {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                $this->loadLayout();
                $this->getLayout()->getBlock('designcollaboration.content')->assign(array(
                    "isAccess" => Mage::helper('designcollaboration')->checkCustomerAccess($designId),
                    "design" => $designModel
                ));
                $this->getLayout()->getBlock('designcollaboration.comments')->assign(array(
                    "design" => $designModel
                ));
                $this->renderLayout();
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function removeAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();

        if (Mage::helper('designcollaboration')->isEnabledModule() && $designModel->getId() > 0) {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                if (Mage::helper('designcollaboration')->checkCustomerAccess($designId)) {

                    if ($designModel->getCreatedBy() == $customerData->getId()) {
                        try {
                            Mage::helper('designcollaboration')->deleteQuotesByDesignId($designId);
                            $designModel->delete();
                        } catch (Exception $e) {
                            Mage::getSingleton('customer/session')->addError(
                                $this->__('An error occurred while deleting the item from designs page: %s', $e->getMessage())
                            );
                        }
                    } else {
                        $sharingId = Mage::getModel('designcollaboration/sharings')->getCollection()
                            ->addFieldToFilter('customer_id', $customerData->getId())
                            ->setPageSize(1)
                            ->getFirstItem()
                            ->getSharingId();
                        $sharingsModel = Mage::getModel('designcollaboration/sharings')->load($sharingId);

                        try {
                            $sharingsModel->delete();
                        } catch (Exception $e) {
                            Mage::getSingleton('customer/session')->addError(
                                $this->__('An error occurred while deleting the item from designs page: %s', $e->getMessage())
                            );
                        }
                    }
                    $this->_redirectReferer(Mage::getUrl('*/*'));
                } else {
                    $this->_forward('noRoute');
                }
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function addpostAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $message = $this->getRequest()->getPost('message');

        if (Mage::helper('designcollaboration')->isEnabledModule() && $designModel->getId() > 0 && isset($message) && $message != '') {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                if (Mage::helper('designcollaboration')->checkCustomerAccess($designId)) {
                    Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), $message);
                    $this->_redirectReferer(Mage::getUrl('*/*'));
                } else {
                    $this->_forward('noRoute');
                }
            }
        } else {
            $this->_forward('noRoute');
        }
    }

    public function cancelandunlockAction()
    {
        $designId = Mage::app()->getRequest()->getParam('id', 0);
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);
        $customerData = Mage::getSingleton('customer/session')->getCustomer();

        if ($designModel->getId() > 0 && Mage::helper('designcollaboration')->isLockedDesign($designModel->getId())) {
            if (is_null($customerData->getId())) {
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            } else {
                if (!Mage::helper('designcollaboration')->isLockedDesign($designModel->getId(), true)) {
                    Mage::getModel('designcollaboration/designs')->setStatusUnlocked($designModel->getId(), $customerData->getId(), false);
                    Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::helper('designcollaboration')->getAllDesignsUrl());
                } else {
                    $this->_forward('noRoute');
                }
            }
        } else {
            $this->_redirectUrl(Mage::helper('designcollaboration')->getAllDesignsUrl());
        }
    }

}
