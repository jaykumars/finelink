<?php

class Fineline_Designcollaboration_Model_Observers_Observer extends Varien_Event_Observer
{

    public function prepareLink(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('designcollaboration');
        if ($helper->isEnabledModule()) {
            $observer->getEvent()->getLayout()->getUpdate()->addHandle('designcollaboration_customer_account');
        }
    }

    public function salesQuoteItemSetProduct(Varien_Event_Observer $observer)
    {
        $item = $observer->getQuoteItem();
        $helper = Mage::helper('designcollaboration');
        if ($helper->isEnabledModule()) {
            $model = $helper->getDesignByQuoteItemId($item->getId());
            if ((bool) $model) {
                $item->setName($model['designname']);
            }
        }

        return $this;
    }

    public function salesQuoteItemSaveAfter(Varien_Event_Observer $observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        $product = $quoteItem->getProduct();
        $designId = $product->getDcDesignId();
        if ($designId) {
            Mage::helper('designcollaboration')->setQuoteItemId($designId, $quoteItem->getItemId());
        }
    }

    public function predispatchSalesOrderReorder($observer) {
        $orderId = Mage::app()->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);

        $designIds = array();

        foreach ($order->getItemsCollection() as $orderItem) {
            $design = Mage::helper('designcollaboration')->getDesignByQuoteItemId($orderItem->getQuoteItemId());
            $designIds[] = $design !== false ? $design['design_id'] : null;
        }

        Mage::register('dc_reorder_design_array', $designIds);
    }

    public function checkoutCartProductAddAfter($cartItem) {
        $product = $cartItem->getProduct();

        //Get document id of current item
        //If reorder, take from order item
        //If add to cart, take from session
        if (Mage::app()->getRequest()->getRouteName() == 'sales' && Mage::app()->getRequest()->getActionName() == 'reorder') {
            $designIds = Mage::registry('dc_reorder_design_array');
            $designId = $designIds[0];
            array_shift($designIds);
            Mage::unregister('dc_reorder_design_array');
            Mage::register('dc_reorder_design_array', $designIds);

            if ($designId !== null) {
                //For connecting quote item with added product
                $product->setDcDesignId($designId);

                $cartItem->getQuoteItem()->setProduct($product);
            }
        }
    }

    /**
     * EVENT LISTENER
     *
     * When saving an order, create for each order item a pdf and save url
     * @todo    activate this method again (system.xml) / changes have already been made
     */
    public function orderPlaceAfterCreatePdf($observer) {
        // Load the order and order items related to the event
        $order = $observer->getOrder();
        $orderItems = $order->getAllItems();
        $website = $order->getStore()->getWebsite();
        $storeId = $order->getStore()->getId();

        foreach ($orderItems as $orderItem) {
            $design = Mage::helper('designcollaboration')->getDesignByQuoteItemId($orderItem->getQuoteItemId());

            if ($design !== false) {
                $documentId = $design['finelink_item_id'];

                try {
                    if (!empty($documentId)) {
                        //Frontend
                        $pdfFrontend = Mage::getModel('web2print/pdf')->getCollection()
                            ->addFieldToFilter('document_id', $documentId)
                            ->addFieldToFilter('export_type', 'frontend')
                            ->getFirstItem();
                        if (!$pdfFrontend) {
                            $pdfFrontend = Mage::getModel('web2print/pdf');
                        }
                        $pdfFrontend->setDocumentId($documentId);
                        $pdfFrontend->setOrderItemId($orderItem->getId());
                        $pdfFrontend->setOrderId($order->getId());
                        $pdfFrontend->setOrderIncrementId($order->getIncrementId());
                        $pdfFrontend->setCreatedAt(date("Y-m-d H:i:s"));
                        $pdfFrontend->setExportType('frontend');

                        if (Mage::helper('web2print')->isValidExportType($website, 'frontend')) {
                            $pdfFrontend->setStatus('queued');
                            $pdfFrontend->setExportProfile(Mage::helper('web2print')->getPdfExportProfile('frontend', $website, $orderItem->getProductId(), $storeId));
                            $pdfFrontend->save();
                        } else {
                            $pdfFrontend->setStatus('no-pdf-export-settings-found');
                            $pdfFrontend->save();
                        }

                        //Backend
                        $pdfBackend = Mage::getModel('web2print/pdf');
                        $pdfBackend->setDocumentId($documentId);
                        $pdfBackend->setOrderItemId($orderItem->getId());
                        $pdfBackend->setOrderId($order->getId());
                        $pdfBackend->setOrderIncrementId($order->getIncrementId());
                        $pdfBackend->setCreatedAt(date("Y-m-d H:i:s"));
                        $pdfBackend->setExportType('backend');

                        if (Mage::helper('web2print')->isValidExportType($website, 'backend')) {
                            $pdfBackend->setStatus('queued');
                            $pdfBackend->setExportProfile(Mage::helper('web2print')->getPdfExportProfile('backend', $website, $orderItem->getProductId(), $storeId));
                            $pdfBackend->save();
                        } else {
                            $pdfBackend->setStatus('no-pdf-export-settings-found');
                            $pdfBackend->save();
                        }
                    }
                } catch (Exception $e) {
                    Mage::log($e->getMessage());
                }
            }
        }
    }
}