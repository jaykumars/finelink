<?php

$installer = $this;
$installer->startSetup();

$customerGroupTable = $installer->getTable('customer/customer_group');
$table = $installer->getConnection();


// Customer Group
//

$table->addColumn($customerGroupTable, 'fcr_view', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Customer Group View'
));


$installer->endSetup();
