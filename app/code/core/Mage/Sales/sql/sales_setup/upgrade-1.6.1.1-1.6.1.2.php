<?php
$installer = $this;
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'customer_number', array(
          	 'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			     'nullable' => true,
        	   'comment' => 'Customer Number'
    ));