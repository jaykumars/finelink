<?php

class Fineline_Loggr_Model_Observer extends Varien_Event_Observer
{
    private $modules = array();

    public function modelLoadBefore($observer)
    {
        $object = $observer->getObject();
        $this->modules[get_class($object)] = false;
    }

    public function modelLoadAfter($observer)
    {
        $object = $observer->getObject();
        $this->modules[get_class($object)] = true;
    }

    public function sendData($observer)
    {
        //Get config options
        $logKey = Mage::getStoreConfig('loggr_config/connection/logKey');
        $apiKey = Mage::getStoreConfig('loggr_config/connection/apiKey');
        $triggeredEvents = Mage::getStoreConfig('loggr_config/connection/triggeredEvents');

        if ($logKey != '' && $apiKey != '') {
            //Init class
            $class = Mage::helper('loggr/loggr');
            $loggr = new $class($logKey, $apiKey);

            //Get additional data for logs
            $url = Mage::helper('core/url')->getCurrentUrl();
            $area = Mage::getSingleton('core/design_package')->getArea();
            $user = '';
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $session = Mage::getSingleton('customer/session')->getCustomer();
                $data = Mage::getModel('customer/customer')->load($session->getId())->getData();
                $user = $data['email'];
            }
            if (Mage::getSingleton('admin/session')->isLoggedIn()) {
                $session = Mage::getSingleton('admin/session');
                $data = $session->getUser()->getData();
                $user = $data['email'];
            }

            //Sending logs
            foreach ($this->modules as $key => $value) {
                try {
                    if ($triggeredEvents == 0 || $triggeredEvents == 1 && !$value) {
                        $loggr->Events->Create()
                            ->TextF("Module: %s", $key)
                            ->Tags($value ? 'success' : 'error')
                            ->Link($url)
                            ->Source($area . ($user ? ' - '.$user : ''))
                            ->Post();
                    }
                } catch (Exception $e) {
                }
            }
        }
    }
}

?>
