<?php
/**
 * Created by PhpStorm.
 * User: jays
 * Date: 10/23/2017
 * Time: 1:31 PM
 */


class Fineline_MasterRedirect_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Admin controller Index Action
     * @access public
     * @return void
     */
    public function indexAction()
    {
        echo "Here's where I change the URL in Admin Side<br>";
//        $this->getResponse()->setRedirect("http://www.google.com");
    }

}