<?php

$installer = $this;
$installer->startSetup();

$customerGroupTable = $installer->getTable('customer/customer_group');
$table = $installer->getConnection();


// Customer Group
//

$table->addColumn($customerGroupTable, 'fcr_can_override_billing', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "backend"  => "",
    "label"    => "Can override billing?",
    "default"  => 0,
    "comment"     => "Enables editable billing address even if in requisitioner group."
));

$table->addColumn($customerGroupTable, 'fcr_can_view_reports', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    "backend"  => "",
    "label"    => "Can view reports?",
    "input"    => "boolean",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default"  => 0,
    "frontend" => "",
    "unique"   => false,
    "comment"     => "Can view site reports?"
));





$installer->endSetup();
