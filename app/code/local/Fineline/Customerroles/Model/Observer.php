<?php
class Fineline_Customerroles_Model_Observer extends Varien_Event_Observer
{

    /**
     * Used to track recursive saves.
     * @var boolean
     */
    private $recursed = false;


    public function getRegionUpdaterScript($type) {
        $json = Mage::helper('directory')->getRegionJson();
        return <<<EOT
        <script type="text/javascript">
         window.{$type}RegionUpdater = new RegionUpdater('fcr_{$type}_country_id', 'fcr_{$type}_region', 'fcr_{$type}_region_id', $json, undefined, 'fcr_{$type}_postcode');
        </script>
EOT;
    }


    /**
     * Runs when displaying the 'Edit' or 'New' form for Customer Groups.
     * @param  Varien_Event_Observer $observer  Magento observer object.
     * @return
     */
    public function appendCustomColumn(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'adminhtml/customer_group_grid') {
            $block->addColumnAfter('fcr_roletype', array(
                'header' => Mage::helper('customer')->__('Role Type'),
                'index' => 'fcr_roletype',
                'width' => '200px',
                'renderer' => 'Fineline_Customerroles_Block_Customer_Group_Renderer_RoleType',
            ), 'type');

            $block->addColumnAfter('fcr_approver', array(
                'header' => Mage::helper('customer')->__('Approver'),
                'index' => 'fcr_approver',
                'width' => '200px',
                'renderer' => 'Fineline_Customerroles_Block_Customer_Group_Renderer_Approver',
            ), 'fcr_roletype');

            $block->addColumnAfter('fcr_store', array(
                'header' => Mage::helper('customer')->__('Site'),
                'index' => 'fcr_store',
                'width' => '200px',
                'type'    => 'options',
                'options'   => Mage::getModel('core/website')->getCollection()->toOptionHash(),
                'renderer' => 'Fineline_Customerroles_Block_Customer_Group_Renderer_Store',
            ), 'fcr_approver');

        }

        if ($block->getType() == 'adminhtml/customer_group_edit_form') {

            $form = $block->getForm();

            $elements = $form->getElements();
            $fieldset = $elements->searchById('base_fieldset');


            if (!Mage::app()->isSingleStoreMode()) {
                $websites = Mage::app()->getWebsites();
                $websiteArray = array(array('value'=>0, 'label'=>'--'));
                foreach ($websites as $website) {
                    $websiteArray[] = array('value'=>$website->getId(), 'label'=>$website->getName());
                }
                $fieldset->addField('fcr_store', 'select', array(
                    'name' => 'fcr_store',
                    'label' => Mage::helper('customer')->__('Site'),
                    'title' => Mage::helper('customer')->__('Site'),
                    'required' => true,
                    'values' => $websiteArray,
                    // 'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                ));
            }else {
                $fieldset->addField('fcr_store', 'hidden', array(
                    'name' => 'store',
                    'value' => Mage::app()->getStore(true)->getId()
                ));
            }

           if (!Mage::app()->isSingleStoreMode()) {
                $stores = Mage::app()->getStores();
                $storeArray = array(array('value'=>0, 'label'=>'--'));
                    foreach ($stores as $store) {
                        $storeArray[] = array('value'=>$store->getId(), 'label'=>$store->getName());
                    }


                $fieldset->addField('fcr_view', 'select', array(
                    'name'  => 'fcr_view',
                    'label' => Mage::helper('customer')->__('Store View'),
                    'title' => Mage::helper('customer')->__('Store View'),
                     
                    'required' => true,
                    'values' => $storeArray,
                ));
                }else {
                $fieldset->addField('fcr_view', 'hidden', array(
                    'name' => 'view',
                    'value' => Mage::app()->getStore(true)->getId()
                ));
            }

            $customerGroupModel = Mage::getModel('customer/group');
            $allCustomerGroups = $customerGroupModel->getCollection();
            $customerGroup = Mage::registry('current_group');

            $approverGroupSelection = array(array('value'=>'0','label'=>'--', 'roletype'=>'2', 'store'=>'0'));
            $approveeGroupSelection = array();

            foreach ($allCustomerGroups as $key => $group) {
                if($customerGroup->getId() != $group->getId() && $group->getId() != 0){
                    $new = array(
                        'value' => $group->getId(),
                        'label' => $group->getCode(),
                        'roletype' => $group->getFcrRoletype(),
                        'store' => $group->getFcrStore(),
                    );
                    $approverGroupSelection[] = $new;
                    $approveeGroupSelection[] = $new;
                }
            }

            $approverGroupsJSON = json_encode($approverGroupSelection);
            $approveeGroupsJSON = json_encode($approveeGroupSelection);


            $fieldset->addField('fcr_roletype', 'select', array(
                'name'  => 'fcr_roletype',
                'label' => Mage::helper('customer')->__('Role Type'),
                'title' => Mage::helper('customer')->__('Role Type'),
                'class' => 'roletype',
                'required' => false,
                'values' => array(
                    0 => 'Requisitioner',
                    1 => 'Purchaser',
                    2 => 'Senior Purchaser',
                ),
                'after_element_html' => "<script type=\"text/javascript\">window.approvers=$approverGroupsJSON;window.approvees=$approveeGroupsJSON;</script>",
            ));

            $fieldset->addField('fcr_approver', 'select', array(
                'name'  => 'fcr_approver',
                'label' => Mage::helper('customer')->__('Approver Group'),
                'title' => Mage::helper('customer')->__('Approver Group'),
                'class' => 'purchaser-hide',
                'required' => false,
                'values' => $approverGroupSelection,
            ));

            $billingFieldset = $form->addFieldset('billing_fieldset', array('legend'=>Mage::helper('customer')->__('Default Billing')));

            $billingFieldset->addField('fcr_can_override_billing', 'checkbox', array(
                'name'  => 'fcr_can_override_billing',
                'label' => Mage::helper('customer')->__('Can Override Billing'),
                'title' => Mage::helper('customer')->__('Can Override Billing'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_first_name', 'text', array(
                'name'  => 'fcr_billing_first_name',
                'label' => Mage::helper('customer')->__('First Name'),
                'title' => Mage::helper('customer')->__('First Name'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_last_name', 'text', array(
                'name'  => 'fcr_billing_last_name',
                'label' => Mage::helper('customer')->__('Last Name'),
                'title' => Mage::helper('customer')->__('Last Name'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_middle_name', 'text', array(
                'name'  => 'fcr_billing_middle_name',
                'label' => Mage::helper('customer')->__('Middle Name/Initial'),
                'title' => Mage::helper('customer')->__('Middle Name/Initial'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_company', 'text', array(
                'name'  => 'fcr_billing_company',
                'label' => Mage::helper('customer')->__('Company'),
                'title' => Mage::helper('customer')->__('Company'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_street', 'text', array(
                'name'  => 'fcr_billing_street',
                'label' => Mage::helper('customer')->__('Street'),
                'title' => Mage::helper('customer')->__('Street'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_city', 'text', array(
                'name'  => 'fcr_billing_city',
                'label' => Mage::helper('customer')->__('City'),
                'title' => Mage::helper('customer')->__('City'),
                'class' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_region', 'text', array(
                'name'  => 'fcr_billing_region',
                'label' => Mage::helper('customer')->__('State/Province'),
                'title' => Mage::helper('customer')->__('State/Province'),
                'class' => '',
                'style' => 'display:none;',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_region_id', 'select', array(
                'name'  => 'fcr_billing_region_id',
                'label' => Mage::helper('customer')->__('State/Province'),
                'title' => Mage::helper('customer')->__('State/Province'),
                'class' => '',
                'style' => 'display:none;',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_postcode', 'text', array(
                'name'  => 'fcr_billing_postcode',
                'label' => Mage::helper('customer')->__('Postcode'),
                'title' => Mage::helper('customer')->__('Postcode'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            $billingCountry = $billingFieldset->addField('fcr_billing_country_id', 'select', array(
                'name'  => 'fcr_billing_country_id',
                'label' => Mage::helper('customer')->__('Country'),
                'title' => Mage::helper('customer')->__('Country'),
                'class' => '',
                'style' => '',
                'required' => false,
                'values' => Mage::getSingleton('directory/country')->getResourceCollection()->loadByStore()->toOptionArray(),
                'after_element_html' => $this->getRegionUpdaterScript('billing'),
            ));

            $billingFieldset->addField('fcr_billing_telephone', 'text', array(
                'name'  => 'fcr_billing_telephone',
                'label' => Mage::helper('customer')->__('Telephone'),
                'title' => Mage::helper('customer')->__('Telephone'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            $billingFieldset->addField('fcr_billing_fax', 'text', array(
                'name'  => 'fcr_billing_fax',
                'label' => Mage::helper('customer')->__('Fax'),
                'title' => Mage::helper('customer')->__('Fax'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            // Shipping


            $shippingFieldset = $form->addFieldset('shipping_fieldset', array('legend'=>Mage::helper('customer')->__('Default Shipping')));


            $shippingFieldset->addField('fcr_same_as_billing', 'checkbox', array(
                'name'  => 'fcr_same_as_billing',
                'label' => Mage::helper('customer')->__('Same as billing'),
                'title' => Mage::helper('customer')->__('Same as billing'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_first_name', 'text', array(
                'name'  => 'fcr_shipping_first_name',
                'label' => Mage::helper('customer')->__('First Name'),
                'title' => Mage::helper('customer')->__('First Name'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_last_name', 'text', array(
                'name'  => 'fcr_shipping_last_name',
                'label' => Mage::helper('customer')->__('Last Name'),
                'title' => Mage::helper('customer')->__('Last Name'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_middle_name', 'text', array(
                'name'  => 'fcr_shipping_middle_name',
                'label' => Mage::helper('customer')->__('Middle Name/Initial'),
                'title' => Mage::helper('customer')->__('Middle Name/Initial'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_company', 'text', array(
                'name'  => 'fcr_shipping_company',
                'label' => Mage::helper('customer')->__('Company'),
                'title' => Mage::helper('customer')->__('Company'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_street', 'text', array(
                'name'  => 'fcr_shipping_street',
                'label' => Mage::helper('customer')->__('Street'),
                'title' => Mage::helper('customer')->__('Street'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_city', 'text', array(
                'name'  => 'fcr_shipping_city',
                'label' => Mage::helper('customer')->__('City'),
                'title' => Mage::helper('customer')->__('City'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_region', 'text', array(
                'name'  => 'fcr_shipping_region',
                'label' => Mage::helper('customer')->__('State/Province'),
                'title' => Mage::helper('customer')->__('State/Province'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_region_id', 'select', array(
                'name'  => 'fcr_shipping_region_id',
                'label' => Mage::helper('customer')->__('State/Province'),
                'title' => Mage::helper('customer')->__('State/Province'),
                'class' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_postcode', 'text', array(
                'name'  => 'fcr_shipping_postcode',
                'label' => Mage::helper('customer')->__('Postcode'),
                'title' => Mage::helper('customer')->__('Postcode'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            $shippingCountry = $shippingFieldset->addField('fcr_shipping_country_id', 'select', array(
                'name'  => 'fcr_shipping_country_id',
                'label' => Mage::helper('customer')->__('Country'),
                'title' => Mage::helper('customer')->__('Country'),
                'class' => '',
                'style' => '',
                'required' => false,
                'values' => Mage::getSingleton('directory/country')->getResourceCollection()->loadByStore()->toOptionArray(),
                'after_element_html' => $this->getRegionUpdaterScript('shipping'),
            ));

            $shippingFieldset->addField('fcr_shipping_telephone', 'text', array(
                'name'  => 'fcr_shipping_telephone',
                'label' => Mage::helper('customer')->__('Telephone'),
                'title' => Mage::helper('customer')->__('Telephone'),
                'class' => '',
                'style' => '',
                'required' => false
            ));

            $shippingFieldset->addField('fcr_shipping_fax', 'text', array(
                'name'  => 'fcr_shipping_fax',
                'label' => Mage::helper('customer')->__('Fax'),
                'title' => Mage::helper('customer')->__('Fax'),
                'class' => '',
                'style' => '',
                'required' => false
            ));



            if( Mage::getSingleton('adminhtml/session')->getCustomerGroupData() ) {
                $form->addValues(Mage::getSingleton('adminhtml/session')->getCustomerGroupData());
                Mage::getSingleton('adminhtml/session')->setCustomerGroupData(null);
            } else {
                $form->addValues($customerGroup->getData());
            }

            if ($shippingCountry->getValue() == NULL) {
                $shippingCountry->setValue('US');
            }

            if ($billingCountry->getValue() == NULL) {
                $billingCountry->setValue('US');
            }

            $fieldset->addField('fcr_approvees', 'checkboxes', array(
                'name'  => 'fcr_approvees[]',
                'label' => Mage::helper('customer')->__('Approvees'),
                'title' => Mage::helper('customer')->__('Approvees'),
                'class' => 'requisitioner-hide',
                'required' => false,
                'values' => $approveeGroupSelection,
                'checked' => explode(',', $customerGroup->getFcrApprovees()),
            ));
        }
    }


    /**
     * Runs whenever a Customer Group is saved. Relies on being a singleton to avoid infinite loop.
     * @param  Varien_Event_Observer $observer Magento observer object.
     * @return none Returns early if recursive saves are detected.
     */
    public function onCustomerGroupSave(Varien_Event_Observer $observer)
    {
        if ($this->recursed) {
            return;
        }
        try {
            $customerGroup = Mage::getModel('customer/group');
            $savedGroup = $observer->getEvent()->getObject();
            $id = $savedGroup->getId();
            $customerGroup->load((int)$id);

            $roleType = (int)Mage::app()->getRequest()->getParam('fcr_roletype');
            $approver = (int)Mage::app()->getRequest()->getParam('fcr_approver');
            $store = (int)Mage::app()->getRequest()->getParam('fcr_store');
            $view = (int)Mage::app()->getRequest()->getParam('fcr_view');
            $approveeArray = Mage::app()->getRequest()->getParam('fcr_approvees');
            $approvees = implode(',', $approveeArray);

            $billingChange = Mage::app()->getRequest()->getParam('fcr_can_override_billing');
            $billingFirstName = Mage::app()->getRequest()->getParam('fcr_billing_first_name');
            $billingMiddleName = Mage::app()->getRequest()->getParam('fcr_billing_middle_name');
            $billingLastName = Mage::app()->getRequest()->getParam('fcr_billing_last_name');
            $billingCompany = Mage::app()->getRequest()->getParam('fcr_billing_company');
            $billingStreet = Mage::app()->getRequest()->getParam('fcr_billing_street');
            $billingCity = Mage::app()->getRequest()->getParam('fcr_billing_city');
            $billingRegion = Mage::app()->getRequest()->getParam('fcr_billing_region');
            $billingRegionId = (int)Mage::app()->getRequest()->getParam('fcr_billing_region_id');
            $billingPostcode = Mage::app()->getRequest()->getParam('fcr_billing_postcode');
            $billingCountryId = Mage::app()->getRequest()->getParam('fcr_billing_country_id');
            $billingTelephone = Mage::app()->getRequest()->getParam('fcr_billing_telephone');
            $billingFax = Mage::app()->getRequest()->getParam('fcr_billing_fax');

            if (empty($billingRegion) && !empty($billingRegionId)) {
                $region = Mage::getModel('directory/region')->load($billingRegionId);
                $billingRegion = $region->getName();
            }

            $shippingFirstName = Mage::app()->getRequest()->getParam('fcr_shipping_first_name');
            $shippingMiddleName = Mage::app()->getRequest()->getParam('fcr_shipping_middle_name');
            $shippingLastName = Mage::app()->getRequest()->getParam('fcr_shipping_last_name');
            $shippingCompany = Mage::app()->getRequest()->getParam('fcr_shipping_company');
            $shippingStreet = Mage::app()->getRequest()->getParam('fcr_shipping_street');
            $shippingCity = Mage::app()->getRequest()->getParam('fcr_shipping_city');
            $shippingRegion = Mage::app()->getRequest()->getParam('fcr_shipping_region');
            $shippingRegionId = (int)Mage::app()->getRequest()->getParam('fcr_shipping_region_id');
            $shippingPostcode = Mage::app()->getRequest()->getParam('fcr_shipping_postcode');
            $shippingCountryId = Mage::app()->getRequest()->getParam('fcr_shipping_country_id');
            $shippingTelephone = Mage::app()->getRequest()->getParam('fcr_shipping_telephone');
            $shippingFax = Mage::app()->getRequest()->getParam('fcr_shipping_fax');

            if (empty($shippingRegion) && !empty($shippingRegionId)) {
                $region = Mage::getModel('directory/region')->load($shippingRegionId);
                $shippingRegion = $region->getName();
            }


            $this->recursed = true;
            $customerGroup->setFcrRoletype($roleType);
            $oldapproverId = $customerGroup->getFcrApprover();
            $customerGroup->setFcrApprover($approver);
            $customerGroup->setFcrStore($store);
            $customerGroup->setFcrView($view);
            $customerGroup->setFcrApprovees($approvees);

            $customerGroup->setFcrCanOverrideBilling($billingChange);
            $customerGroup->setFcrBillingFirstName($billingFirstName);
            $customerGroup->setFcrBillingLastName($billingLastName);
            $customerGroup->setFcrBillingMiddleName($billingMiddleName);
            $customerGroup->setFcrBillingCompany($billingCompany);
            $customerGroup->setFcrBillingStreet($billingStreet);
            $customerGroup->setFcrBillingCity($billingCity);
            $customerGroup->setFcrBillingRegion($billingRegion);
            $customerGroup->setFcrBillingRegionId($billingRegionId);
            $customerGroup->setFcrBillingPostcode($billingPostcode);
            $customerGroup->setFcrBillingCountryId($billingCountryId);
            $customerGroup->setFcrBillingTelephone($billingTelephone);
            $customerGroup->setFcrBillingFax($billingFax);

            $customerGroup->setFcrShippingFirstName($shippingFirstName);
            $customerGroup->setFcrShippingLastName($shippingLastName);
            $customerGroup->setFcrShippingMiddleName($shippingMiddleName);
            $customerGroup->setFcrShippingCompany($shippingCompany);
            $customerGroup->setFcrShippingStreet($shippingStreet);
            $customerGroup->setFcrShippingCity($shippingCity);
            $customerGroup->setFcrShippingRegion($shippingRegion);
            $customerGroup->setFcrShippingRegionId($shippingRegionId);
            $customerGroup->setFcrShippingPostcode($shippingPostcode);
            $customerGroup->setFcrShippingCountryId($shippingCountryId);
            $customerGroup->setFcrShippingTelephone($shippingTelephone);
            $customerGroup->setFcrShippingFax($shippingFax);

            $customerGroup->save();

            foreach ($approveeArray as $approveeId) {
                $approvee = Mage::getModel('customer/group')->load($approveeId);
                $approvee->setFcrApprover($customerGroup->getId());
                $approvee->save();
            }

            if ($oldapproverId != $approver){
                $oldapprover = Mage::getModel('customer/group')->load($oldapproverId);
                $oldapprovees = $oldapprover->getFcrApprovees();
                $oldapproveesarray = explode(',', $oldapprovees);
                if(($key = array_search($customerGroup->getId(), $oldapproveesarray)) !== false) {
                    unset($oldapproveesarray[$key]);
                }
                $oldapprover->setFcrApprovees(implode(',', $oldapproveesarray));
                $oldapprover->save();
            }

            $newapprover = Mage::getModel('customer/group')->load($approver);
            $newapprovees = $newapprover->getFcrApprovees();
            $newapproveesarray = explode(',', $newapprovees);
            if (array_search($customerGroup->getId(), $newapproveesarray) === false){
                $newapproveesarray[] = $customerGroup->getid();
                $newapprover->setFcrApprovees(implode(',', $newapproveesarray));
                $newapprover->save();
            }

        }catch(Exception $e){
            var_dump($e->getMessage());
        }
    }
}
