<?php
class Cybersource_Paypal_Model_Soapapi_Paypal extends Cybersource_Paypal_Model_Soapapi_Abstract
{
    public function payPalEcSetService(Varien_Object $quote)
    {
        $error = false;
        $this->_storeId = $quote->getStoreId();

        $soapClient = $this->getSoapApi();
        $this->buildPayPalSetServiceRequest($quote);

        try {
            $result = $soapClient->runTransaction($this->_request);
            return $result;

        } catch (Exception $e) {
            Mage::throwException(
                Mage::helper('cybersourcepaypal')->__('Gateway request error: %s', $e->getMessage())
            );
        }

        if ($error !== false) {
            Mage::throwException($error);
        }
        return $this;
    }

    public function payPalEcGetDetailsService($quote,$paypalToken)
    {
        $error = false;
        $this->_storeId = $quote->getStoreId();

        $soapClient = $this->getSoapApi();
        $this->buildPayPalGetDetailsRequest($quote,$paypalToken);

        try {
            $result = $soapClient->runTransaction($this->_request);
            return $result;

        } catch (Exception $e) {
            Mage::throwException(
                Mage::helper('cybersourcepaypal')->__('Gateway request error: %s', $e->getMessage())
            );
        }

        if ($error !== false) {
            Mage::throwException($error);
        }
        return $this;
    }

    //Auth
    public function payPalEcDoPaymentService($order){
        $session = Mage::getSingleton('checkout/session');
        $soapClient = $this->getSoapApi();

        $payment = $order->getPayment();
        $paypalInfo = $payment->getAdditionalInformation();

        $this->_request = new stdClass();
        $this->_request->merchantID = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('merchant_id');
        $this->_request->merchantReferenceCode = $paypalInfo['merchantReferenceCode'];
        $this->_request->clientLibrary = "PHP";
        $this->_request->clientLibraryVersion = phpversion();

        $payPalEcDoPaymentService = new stdClass();
        $payPalEcDoPaymentService->run = "true";
        $payPalEcDoPaymentService->paypalToken = $paypalInfo['paypalToken'];
        $payPalEcDoPaymentService->paypalPayerId = $paypalInfo['payerId'];
        $payPalEcDoPaymentService->paypalEcSetRequestID = $paypalInfo['requestID'];
        $payPalEcDoPaymentService->paypalEcSetRequestToken = $paypalInfo['requestToken'];
        $payPalEcDoPaymentService->paypalCustomerEmail = $paypalInfo['payer'];
        $this->_request->payPalEcDoPaymentService = $payPalEcDoPaymentService;

        $purchaseTotals = new stdClass();
        $purchaseTotals->currency = $order->getOrderCurrencyCode();
        $purchaseTotals->grandTotalAmount = $order->getGrandTotal();
        $this->_request->purchaseTotals = $purchaseTotals;



        try {
            $shippingAddressToSend  = $this->getAddressToCyberSource($order->getStoreId(),"request_shipping_address");
            $billingAddressToSend  =  $this->getAddressToCyberSource($order->getStoreId(),"request_bill_address");

            if($shippingAddressToSend==Cybersource_Paypal_Model_Source_AddressDetails::PAYPAL_ADDRESS){
                $this->_request->shipTo=$this->getPayPalAddressObject($paypalInfo,$order,false);
            }
            else if($shippingAddressToSend==Cybersource_Paypal_Model_Source_AddressDetails::MAGENTO_ADDRESS){
                $shippingAddress = $order->getShippingAddress();
                if($shippingAddress->getData()){
                    $this->_request->shipTo=$this->getMagentoAddressObject($shippingAddress,$order,false);
                }
            }

            if($billingAddressToSend==Cybersource_Paypal_Model_Source_AddressDetails::PAYPAL_ADDRESS){
                $this->_request->billTo=$this->getPayPalAddressObject($paypalInfo,$order,true);
            }
            else if($billingAddressToSend==Cybersource_Paypal_Model_Source_AddressDetails::MAGENTO_ADDRESS){
                $billingAddress = $order->getBillingAddress();
                if($billingAddress->getData()){
                    $this->_request->billTo=$this->getMagentoAddressObject($billingAddress,$order,true);
                }
            }
            $result = $soapClient->runTransaction($this->_request);
            if ($result->reasonCode==Cybersource_Paypal_Model_Source_Consts::STATUS_ACCEPT) {

                if($result->payPalEcDoPaymentReply->reasonCode==Cybersource_Paypal_Model_Source_Consts::REASON_CODE_ACCEPT){
                    return $result;
                }

            } else {
                $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($result->reasonCode);
                $session->addError(Mage::helper('cybersourcepaypal')->__('There is an error in processing the payment. Please try again or contact us. '.$errorMsg));
            }
        } catch (Exception $e) {
            $session->addError(Mage::helper('cybersourcepaypal')->__('Gateway request error: %s', $e->getMessage()));
            return false;
        }

        return false;
    }

    //Capture
    public function payPalDoCaptureService($payment, $amount){
        $session = Mage::getSingleton('checkout/session');
        $soapClient = $this->getSoapApi();
        $info = $payment->getData('additional_information');
        $this->_request = new stdClass();
        $this->_request->merchantID = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('merchant_id');
        $this->_request->merchantReferenceCode = $info['merchantReferenceCode'];
        $this->_request->clientLibrary = "PHP";
        $this->_request->clientLibraryVersion = phpversion();

        $payPalDoCaptureService = new stdClass();
        $payPalDoCaptureService->run = "true";
        $payPalDoCaptureService->completeType = "Complete";
        $payPalDoCaptureService->paypalAuthorizationId = $info['transactionId'];
        $payPalDoCaptureService->paypalEcDoPaymentRequestID = $info['requestID'];
        $payPalDoCaptureService->paypalEcDoPaymentRequestToken = $info['requestToken'];
        $this->_request->payPalDoCaptureService = $payPalDoCaptureService;

        $purchaseTotals = new stdClass();
        $purchaseTotals->currency = $info['currency'];
        $purchaseTotals->grandTotalAmount = $amount;
        $this->_request->purchaseTotals = $purchaseTotals;

        try {
            $result = $soapClient->runTransaction($this->_request);
            if ($result->reasonCode==Cybersource_Paypal_Model_Source_Consts::STATUS_ACCEPT) {

                if($result->payPalDoCaptureReply->reasonCode==Cybersource_Paypal_Model_Source_Consts::REASON_CODE_ACCEPT){
                    return $result->payPalDoCaptureReply;
                }

            } else {
                $errorMsg = Cybersource_Paypal_Model_Source_Consts::getErrorCode($result->reasonCode);
                $session->addError(Mage::helper('cybersourcepaypal')->__('There is an error in processing the payment. Please try again or contact us. '.$errorMsg));
            }
        } catch (Exception $e) {
            $session->addError(Mage::helper('cybersourcepaypal')->__('Gateway request error: %s', $e->getMessage()));
            return false;
        }

        return $this;
    }

    //Auth+Capture
    public function payPalSale(Varien_Object $payment){
        $session = Mage::getSingleton('checkout/session');
        $soapClient = $this->getSoapApi();

        $paymentInfo = $payment->getAdditionalInformation();

        $this->_request = new stdClass();
        $this->_request->merchantID = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('merchant_id');
        $this->_request->merchantReferenceCode = $paymentInfo->merchantReferenceCode;
        $this->_request->clientLibrary = "PHP";
        $this->_request->clientLibraryVersion = phpversion();

        $payPalEcDoPaymentService = new stdClass();
        $payPalEcDoPaymentService->run = "true";
        $payPalEcDoPaymentService->paypalToken = $paymentInfo->payPalEcGetDetailsReply->paypalToken;
        $payPalEcDoPaymentService->paypalPayerId = $paymentInfo->payPalEcGetDetailsReply->payerId;
        $payPalEcDoPaymentService->paypalEcSetRequestID = $paymentInfo->requestID;
        $payPalEcDoPaymentService->paypalEcSetRequestToken = $paymentInfo->requestToken;
        $payPalEcDoPaymentService->paypalCustomerEmail = $paymentInfo->payPalEcGetDetailsReply->payer;
        $this->_request->payPalEcDoPaymentService = $payPalEcDoPaymentService;

        $order = Mage::getModel('sales/order')->loadByIncrementId($paymentInfo->merchantReferenceCode);

        $purchaseTotals = new stdClass();
        $purchaseTotals->currency = $order->getOrderCurrencyCode();
        $purchaseTotals->grandTotalAmount = $order->getGrandTotal();
        $this->_request->purchaseTotals = $purchaseTotals;

        $payPalDoCaptureService = new stdClass();
        $payPalDoCaptureService->run = "true";
        $payPalDoCaptureService->completeType = Cybersource_Paypal_Model_Source_Consts::COMPLETE_CAPTURE;
        $this->_request->payPalDoCaptureService = $payPalDoCaptureService;

        try {
            $result = $soapClient->runTransaction($this->_request);

        } catch (Exception $e) {
            $session->addError(Mage::helper('cybersourcepaypal')->__('Gateway request error: %s', $e->getMessage()));
            return false;
        }

        return $result;
    }

}