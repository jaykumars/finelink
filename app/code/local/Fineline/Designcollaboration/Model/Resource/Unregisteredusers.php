<?php

class Fineline_Designcollaboration_Model_Resource_Unregisteredusers extends Mage_Core_Model_Mysql4_Abstract
{

    public function _construct()
    {
        $this->_init('designcollaboration/unregisteredusers', 'unregistered_user_id');
    }

}