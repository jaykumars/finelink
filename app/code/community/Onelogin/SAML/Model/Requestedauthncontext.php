<?php
class Onelogin_SAML_Model_Requestedauthncontext
{

    public function toOptionArray()
    {
        $values = array(
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified',
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:Password',
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport',
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:X509',
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:Smartcard',
                            'urn:oasis:names:tc:SAML:2.0:ac:classes:Kerberos',
                            'urn:federation:authentication:windows'
        );

        $options = array();

        foreach ($values as $value) {
            $option = array('value' => $value, 'label' => $value);
            $options[] = $option;
        }
        array_unshift($options, array('value' => '-1', 'label' => ''));

        return $options;
    }
}
