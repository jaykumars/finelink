<?php

class Fineline_Designcollaboration_Model_Notifications extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/notifications');
    }

    public function sendNotifications($designId, $customerId)
    {
        $senderData = Mage::getModel('customer/customer')->load($customerId);
        if (is_null($senderData->getId())) {
            return false;
        }

        $notifications = Mage::getModel('designcollaboration/notifications')->getCollection()
            ->addFilter('design_id', $designId, 'AND');

        $notifications->addFieldToFilter('customer_id', array('neq' => $customerId));
        $notifications->getData();

        if (count($notifications) <= 0) return false;

        foreach ($notifications as $value) {
            $customerData = Mage::getModel('customer/customer')->load($value['customer_id']);
            $designModel = Mage::getModel('designcollaboration/designs')->getCollection()
                ->addFilter('design_id', $value['design_id'])
                ->setPageSize(1)
                ->getFirstItem();

            $templateParams['designName'] = $designModel->getDesignname();
            $templateParams['customerName'] = $customerData->getFirstname() . " " . $customerData->getLastname();
            $templateParams['viewLink'] = Mage::helper('designcollaboration')->getViewUrl($designModel->getDesignId());

            $this->sendMailNotification($customerData, $senderData, $templateParams);
        }
    }

    public function sendNotificationsForApprovers($designModel, $customerId)
    {
        $senderData = Mage::getModel('customer/customer')->load($customerId);
        if (is_null($senderData->getId())) {
            return false;
        }
        $senderName = $senderData->getFirstname() . " " . $senderData->getLastname();

        $approvers = Mage::helper('designcollaboration')->getApprovalList($designModel->getDesignId());

        if (count($approvers) > 0) {
            $subject = 'A new design is waiting for your approval!';

            foreach ($approvers as $value) {

                if (!is_null($value['entity_id'])) {
                    $customerData = Mage::getModel('customer/customer')->load($value['entity_id']);
                    $customerName = $customerData->getFirstname() . " " . $customerData->getLastname();

                    $templateParams['designName'] = $designModel->getDesignname();
                    $templateParams['customerName'] = $customerName;
                    $templateParams['senderName'] = $senderName;
                    $templateParams['viewLink'] = Mage::helper('designcollaboration')->getViewUrl($designModel->getDesignId());

                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('notification_for_approver');
                    $processedTemplate = $emailTemplate->getProcessedTemplate($templateParams);

                    Mage::helper('designcollaboration')->sendMail($value['email'], $subject, $customerName, $processedTemplate);
                }
            }
        }

        return true;
    }

    public function sendMailNotification($customerData, $senderData, $templateParams = array())
    {
        $senderName = $senderData->getFirstname() . " " . $senderData->getLastname();
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('notification_info');
        $processedTemplate = $emailTemplate->getProcessedTemplate($templateParams);
        $subject = $senderName . ' added a new comment to ' . $templateParams['designName'];

        Mage::helper('designcollaboration')->sendMail($customerData->getEmail(), $subject, $senderName, $processedTemplate);
    }

    public function removeNotification($designId, $customerId)
    {
        $notificationId = Mage::getModel('designcollaboration/notifications')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->addFilter('customer_id', $customerId, 'AND')
            ->setPageSize(1)
            ->getFirstItem()
            ->getNotificationId();
        $notificationModel = Mage::getModel('designcollaboration/notifications')->load($notificationId);

        try {
            $notificationModel->delete();
        } catch (Exception $e) {
            return array('success' => 0, 'message' => $e->getMessage());
        }

        return array('success' => 1);
    }

    public function subscribeNotification($designId, $customerId)
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return false;
        }

        if (!Mage::helper('designcollaboration')->checkSubscribeNotificationByCustomer($designId)) {
            $data = array('design_id' => $designId,
                'customer_id' => $customerId);

            $notificationModel = Mage::getModel('designcollaboration/notifications')->setData($data);

            try {
                $notificationModel->save();
            } catch (Exception $e) {
                return array('success' => 0, 'message' => $e->getMessage());
            }
        }

        return array('success' => 1);
    }

}