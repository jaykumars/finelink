<?php

class Cybersource_Onestepcheckout_Block_Onestep_Cyber extends Mage_Page_Block_Html {

    public $_configvalues = false;

    protected function _construct()
    {
        parent::_construct();

    }

    /**
     * Force cache to be disabled
     * @see Mage_Core_Block_Abstract::getCacheLifetime()
     */
    public function getCacheLifetime()
    {

        return null;

    }

    //@pytodo improve layout and add to system config
    protected function _toHtml()
    {
        $this->getConfig();

        if($this->_configvalues == false)
        {
            $returnlink = sprintf('<br/><a href="%s">%s</a>', Mage::helper('checkout/cart')->getCartUrl(), $this->__('Please click here to go back to your cart.'));
            $html = $this->getTemplateHtml($this->__('There has been an error with your payment'). $returnlink,'','');

        }else {

            $form = new Varien_Data_Form();
            $form->setAction($this->getCyberUrl())
                ->setId('cybersourceform')
                ->setName('cybersourceform')
                ->setMethod('POST')
                ->setUseContainer(true);

            $fields = $this->getFields();
            $config = Mage::getModel('cybersourcesop/config');
            if ($config->isMobile()) {
                unset($fields['card_cvn']);
                unset($fields['card_number']);
                unset($fields['card_expiry_date']);
                unset($fields['card_type']);
            }

            foreach($fields as $name => $value) {
                $form->addField($name, 'hidden', array('name'=>$name, 'value'=>$value));
            }
            //add signed signature
            $form->addField('signature', 'hidden', array('name'=>'signature', 'value'=>$this->getSignature()));
            //javascript to pull form values from cybersource form.
            $redirectcode = <<<EOD
                            <script type="text/javascript">
                                $('card_type').value = $('cybersourcesop_cc_type').value;
                                $('card_number').value = $('cybersourcesop_cc_number').value;
                                $('card_expiry_date').value = $('cybersourcesop_expiration').value + '-' + $('cybersourcesop_expiration_yr').value;
                                $('card_cvn').value = $('cybersourcesop_cc_cid').value;
                            </script>
EOD;
            $html = $this->getTemplateHtml($form->toHtml(),$redirectcode);
        }

        return $html;
    }

    /**
     * Get the config or bail on error
     *
     * @return boolean
     */
    protected function getConfig()
    {
        if($this->_configvalues == false)
        {

            $this->_configvalues = Mage::getModel('cybersourcesop/config')->assignAllData();


        }

        return $this->_configvalues;
    }

    protected function getTemplateHtml($form, $redirect)
    {
        $str = <<<EOD
		<div>
		$form
		</div>
		$redirect
EOD;
        return $str;
    }

    public function getCyberUrl()
    {

        return $this->getConfig()->getCyberUrl();

    }

    public function getCyberCCCode($ccin)
    {
        return $this->getConfig()->getCCVals($ccin);
    }

    public function getSignature()
    {
        return $this->getConfig()->getSignature();
    }

    public function getFields()
    {

        return $this->getConfig()->getCheckoutFormFields();

    }
}