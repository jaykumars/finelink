<?php
$installer = $this;
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'e32_template', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
  			'nullable' => false,
        	'comment' => 'E32 Template'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'lead_time', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
  			'nullable' => false,
        	'comment' => 'Lead Time'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'customizable', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  			'nullable' => false,
        	'comment' => 'Customizable'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'inventory', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  			'nullable' => false,
        	'comment' => 'Inventory'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'fgi_number', array(
          	 'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
  			     'nullable' => false,
        	   'comment' => 'FGI Number'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'colors_front_select', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			'nullable' => false,
        	'comment' => 'Front Colors'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'colors_back_select', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			'nullable' => false,
        	'comment' => 'Back Colors'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'sides', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  			'nullable' => false,
        	'comment' => 'Sides'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'stock', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
  			'nullable' => false,
        	'comment' => 'Stock'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'finish_size', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			'nullable' => false,
        	'comment' => 'Finish Size'
    ));
    $installer->getConnection()
        ->addColumn($installer->getTable('sales/order_item'), 'chili_document_id', array(
            'type' => Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,
        'nullable' => true,
          'comment' => 'Chili Document ID'
    ));