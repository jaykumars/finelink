<?php

class Fineline_Designcollaboration_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $loggedCustomer = array();
    private $requireApproval = false;
    private $enableModule = false;

    public function __construct()
    {
        //Get customer information
        $this->loggedCustomer = Mage::helper('customer')->getCustomer()->getData();

        //Get customer website
        $website = Mage::getModel('core/website')->load($this->loggedCustomer['website_id'])->getData();

        //Check if this website has module enabled
        if ($website['dc_enable_design'] == '1') {
            $this->enableModule = true;
        }

        //Check if this website needs approval
        if ($website['dc_require_design_approval'] == '1') {
            $this->requireApproval = true;
        }
    }

    public function getChiliEnvironment($website = null) {
        if($website){
            return $website->getConfig('designcollaboration/connection/environment');
        }else{
            return Mage::getStoreConfig('designcollaboration/connection/environment');
        }
    }

    public function getChiliDomain($website = null) {
        if($website && $website->getId()){
            return $website->getConfig('designcollaboration/connection/domain');
        }else{
            return Mage::getStoreConfig('designcollaboration/connection/domain');
        }
    }

    public function getChiliWsdl($website = null) {
        return $this->getChiliDomain($website) . '/main.asmx?wsdl';
    }

    public function getChiliWebserviceUrl($website = null) {
        return $this->getChiliDomain($website) . '/main.asmx';
    }

    public function getChiliUsername($website = null) {
        if($website){
            return $website->getConfig('designcollaboration/connection/username');
        }else{
            return Mage::getStoreConfig('designcollaboration/connection/username');
        }
    }

    public function getChiliPassword($website = null) {
        if($website){
            return $website->getConfig('designcollaboration/connection/password');
        }else{
            return Mage::getStoreConfig('designcollaboration/connection/password');
        }
    }

    public function isSameAsDefaultConfig($website){
        if($this->getChiliDomain() != $this->getChiliDomain($website)){
            return false;
        }
        if($this->getChiliEnvironment()!=$this->getChiliEnvironment($website)){
            return false;
        }
        if($this->getChiliUsername()!=$this->getChiliUsername($website)){
            return false;
        }
        if($this->getChiliPassword()!=$this->getChiliPassword($website)){
            return false;
        }
        return true;
    }

    public function formatDateTime($timestamp)
    {
        return date("m/d/Y h:i A", strtotime($timestamp));
    }

    public function getAllDesignsUrl()
    {
        return Mage::getUrl('designcollaboration/');
    }

    public function getViewUrl($designId)
    {
        return Mage::getUrl('designcollaboration/index/view', array('id' => $designId));
    }

    public function getEditUrl($designId)
    {
        return Mage::getUrl('designcollaboration/index/edit', array('id' => $designId));
    }

    public function getCancelAndUnlockUrl($designId)
    {
        return Mage::getUrl('designcollaboration/index/cancelandunlock', array('id' => $designId));
    }

    public function getHistoryUrl($designId)
    {
        return Mage::getUrl('designcollaboration/index/history', array('id' => $designId));
    }

    public function getRemoveUrl($designId)
    {
        return Mage::getUrl('designcollaboration/index/remove', array('id' => $designId));
    }

    public function getUrlImgWithPort($url)
    {
        return preg_replace('/^(.*chili)/s', Mage::getStoreConfig('designcollaboration/connection/domain'), $url);
    }

    /**
     * get the image url
     * @param type $itemId
     * @param (list|fullscreen|preview|default) $profile
     * @return string
     */
    public function getViewImages($itemId, $profile = 'default')
    {
        $defaultImage = Mage::getDesign()->getSkinUrl('images/finelink/designcollaboration/no-image-icon.png');

        if (is_null($itemId) || empty($itemId)) {
            return $defaultImage;
        }

        $profileValue = Mage::getStoreConfig("designcollaboration/profiles/$profile");
        $profileValue = $profileValue == '' ? Mage::getStoreConfig("designcollaboration/profiles/default") : $profileValue;
        if ($profileValue == '') {
            return $defaultImage;
        }

        $profileValue = explode( '|', $profileValue);
        $profileValue = $profileValue[1];

        $url = Mage::getModel('designcollaboration/api')->getResourceImageUrl($itemId, $profileValue);

        return $this->getUrlImgWithPort($url);
    }

    public function getEditorUrl($documentId, $productId = false)
    {
        return preg_replace('/^(.*chili)\//s', '/CHILI/', Mage::getModel('designcollaboration/api')->getEditorUrl($documentId, $productId));
    }

    public function sendMail($email, $subject, $name, $template)
    {
        $mail = Mage::getModel('core/email')
            ->setToName($name)
            ->setToEmail($email)
            ->setBody($template)
            ->setSubject($subject)
            ->setFromEmail($this->__('helpdesk@finelineprintinggroup.com'))
            ->setFromName($this->__('Fineline Printing Group'))
            ->setType('html');
        try {
            $mail->send();
        } catch (Exception $error) {
            Mage::getSingleton('core/session')->addError($error->getMessage());
            return false;
        }

        return true;
    }

    //Check access for logged customer
    public function checkCustomerAccess($designID)
    {
        $readConnection = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
        $query = "SELECT COUNT(DISTINCT main_table.design_id) as count
                    FROM designcollaboration_designs AS main_table
                      LEFT JOIN designcollaboration_sharings AS sharings ON main_table.design_id = sharings.design_id
                      LEFT JOIN customer_entity ON main_table.send_for_approve_by = customer_entity.entity_id
                      LEFT JOIN customer_group ON customer_entity.group_id = customer_group.customer_group_id
                      LEFT JOIN core_website ON customer_group.fcr_store = core_website.website_id
                      LEFT JOIN designcollaboration_design_approvees ON designcollaboration_design_approvees.approver_group_id = customer_entity.group_id
                    WHERE main_table.design_id = :design_id AND
                          (
                             main_table.created_by = :customer_id OR
                             sharings.customer_id = :customer_id OR
                             main_table.approved_by = :customer_id AND main_table.status_id = 4 OR
                             main_table.status_id = 3 AND core_website.dc_enable_design = 1 AND core_website.dc_require_design_approval = 1 AND core_website.dc_default_design_approval_group = :customer_group_id OR
                             main_table.status_id = 3 AND core_website.dc_enable_design = 1 AND designcollaboration_design_approvees.group_id = :customer_group_id
                          )";
        $binds = array(
            'design_id' => $designID,
            'customer_id' => $this->loggedCustomer['entity_id'],
            'customer_group_id' => $this->loggedCustomer['group_id']
        );
        $result = $readConnection->fetchRow($query, $binds);

        return $result['count'] > 0;
    }

    public function getCustomerById($customerId)
    {
        if ($customerId != 0) {
            $customerData = Mage::getModel('customer/customer')->load($customerId)->getData();

            return $customerData['firstname'] . " " . $customerData['lastname'];
        }

        return "Not found";
    }

    public function checkSubscribeNotificationByCustomer($designId)
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return false;
        }

        $notificationsCollection = Mage::getModel('designcollaboration/notifications')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->addFilter('customer_id', $customerData->getId(), 'AND');

        return $notificationsCollection->getSize() > 0;
    }

    public function getCommentStatus($commentStatusId)
    {
        $commentstatusesCollection = Mage::getModel('designcollaboration/commentstatuses')->getCollection()
            ->addFieldToFilter('comment_status_id', $commentStatusId)
            ->setPageSize(1)
            ->getFirstItem()
            ->getData();

        return $commentstatusesCollection['name'];
    }

    public function getLockedIsByCustomer($designId)
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return false;
        }

        $designData = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFieldToFilter('design_id', $designId)
            ->setPageSize(1)
            ->getFirstItem()
            ->getData();

        if (count($designData) > 0) {
            if ($designData['locked_by'] == $customerData->getId()) {
                return true;
            }
        }

        return false;
    }

    public function getLastComment($designId, $status = 0, $historyId = null)
    {
        $lastComment = Mage::getModel('designcollaboration/comments')->getCollection()
            ->addFilter('design_id', $designId, 'AND');

        if ($status > 0) $lastComment->addFilter('comment_status_id', $status, 'AND');

        if (!is_null($historyId)) $lastComment->addFilter('history_id', $historyId, 'AND');

        $lastComment = $lastComment->setOrder('created_at', 'DESC')
            ->setPageSize(1)
            ->getFirstItem()
            ->getData();

        if (count($lastComment) > 0) {
            $lastComment['created_name'] = $this->getCustomerById($lastComment['created_by']);
            $lastComment['comment_status'] = $this->getCommentStatus($lastComment['comment_status_id']);
        }

        return $lastComment;
    }

    public function hasSharings($designId, $customerId = null)
    {
        $countSharingsCollection = Mage::getModel('designcollaboration/sharings')->getCollection()
            ->addFilter('design_id', $designId, 'AND');

        if (!is_null($customerId)) {
            $countSharingsCollection->addFilter('customer_id', $customerId, 'AND');
        };

        return $countSharingsCollection->getSize() != 0;
    }

    public function isLockedDesign($designId, $lockedByCustomer = false)
    {
        $designData = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFieldToFilter('design_id', $designId)
            ->addFieldToFilter('locked_till', array('gt' => Mage::getModel('core/date')->date()));

        if ($lockedByCustomer) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            if (is_null($customerData->getId())) {
                return true;
            }
            $designData->addFieldToFilter('locked_by', array('neq' => $customerData->getId()));
        }

        $designData->setPageSize(1);
        return $designData->getSize() > 0;
    }

    public function getApprovalList($designID = false)
    {
        if (!$designID) {
            return false;
        }

        $readConnection = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );

        $query = "SELECT DISTINCT approver.*
                    FROM designcollaboration_designs AS dc_designs
                      LEFT JOIN customer_entity ON dc_designs.send_for_approve_by = customer_entity.entity_id
                      LEFT JOIN customer_group ON customer_entity.group_id = customer_group.customer_group_id
                      LEFT JOIN core_website ON customer_group.fcr_store = core_website.website_id
                      LEFT JOIN customer_entity AS approver ON core_website.dc_default_design_approval_group = approver.group_id
                    WHERE dc_designs.design_id = :design_id AND
                          dc_designs.status_id = 3 AND
                          core_website.dc_enable_design = 1 AND
                          core_website.dc_require_design_approval = 1 AND
                          approver.entity_id IS NOT NULL";
        $binds = array(
            'design_id' => $designID
        );
        $default_group = $readConnection->fetchAll($query, $binds);

        $query = "SELECT DISTINCT approver.*
                    FROM designcollaboration_designs AS dc_designs
                      LEFT JOIN customer_entity AS customer ON dc_designs.send_for_approve_by = customer.entity_id
                      LEFT JOIN customer_group ON customer.group_id = customer_group.customer_group_id
                      LEFT JOIN core_website ON customer_group.fcr_store = core_website.website_id
                      LEFT JOIN designcollaboration_design_approvees ON designcollaboration_design_approvees.approver_group_id = customer.group_id
                      LEFT JOIN customer_entity AS approver ON designcollaboration_design_approvees.group_id = approver.group_id
                    WHERE dc_designs.design_id = :design_id AND
                          dc_designs.status_id = 3 AND
                          core_website.dc_enable_design = 1 AND
                          core_website.dc_require_design_approval = 1 AND
                          approver.entity_id IS NOT NULL";
        $binds = array(
            'design_id' => $designID
        );
        $main_group = $readConnection->fetchAll($query, $binds);

        return array_merge ($default_group, $main_group);
    }

    public function getIsApproval($designID = false)
    {
        if ($designID) {
            $readConnection = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
            $query = "SELECT COUNT(DISTINCT main_table.design_id) as count
                        FROM designcollaboration_designs AS main_table
                          LEFT JOIN customer_entity ON main_table.send_for_approve_by = customer_entity.entity_id
                          LEFT JOIN customer_group ON customer_entity.group_id = customer_group.customer_group_id
                          LEFT JOIN core_website ON customer_group.fcr_store = core_website.website_id
                          LEFT JOIN designcollaboration_design_approvees ON designcollaboration_design_approvees.approver_group_id = customer_entity.group_id
                        WHERE main_table.design_id = :design_id AND
                          (
                             main_table.status_id = 4 AND main_table.approved_by = :customer_id OR
                             main_table.status_id = 3 AND core_website.dc_enable_design = 1 AND core_website.dc_require_design_approval = 1 AND core_website.dc_default_design_approval_group = :customer_group_id OR
                             main_table.status_id = 3 AND core_website.dc_enable_design = 1 AND designcollaboration_design_approvees.group_id = :customer_group_id
                          )";
            $binds = array(
                'design_id' => $designID,
                'customer_id' => $this->loggedCustomer['entity_id'],
                'customer_group_id' => $this->loggedCustomer['group_id']
            );
            $result = $readConnection->fetchRow($query, $binds);

            return $result['count'] > 0;
        }

        return false;
    }

    public function isEnabledModule()
    {
        return $this->enableModule;
    }

    public function getRequireApproval()
    {
        return $this->requireApproval;
    }

    public function getMagentoVersionForCss()
    {
        /* Do not use Mage::getEdition() for compatability reasons.
         * Function does not exist prior to Magento CE 1.7 and EE 1.12
         * and crashes Magento if used.
         */
        if (file_exists('LICENSE_EE.txt')) {
            $edition = 'Enterprise';
        } elseif (file_exists('LICENSE_PRO.html')) {
            $edition = 'Professional';
        } else {
            $edition = 'Community';
        }

        $communityVersion = ($edition == 'Community' && version_compare(Mage::getVersion(), '1.7.0.0', '>=')) ? true : false;
        $enterpriseVersion = ($edition == 'Enterprise' && version_compare(Mage::getVersion(), '1.12.0.0', '>=')) ? true : false;

        return ($communityVersion || $enterpriseVersion);
    }

    /**
     * get workspace preference
     */
    public function getCurrentWorkspacePreference()
    {
        if( $workspacePreference = Mage::getStoreConfig('designcollaboration/editor_page/workspace_preference')) {
            return $this->getItemId( $workspacePreference );
        }
        return false;
    }

    /**
     * get view preference
     */
    public function getCurrentViewPreference() {
        if( $viewPreference = Mage::getStoreConfig('designcollaboration/editor_page/view_preference')) {
            return $this->getItemId( $viewPreference );
        }
        return false;
    }

    /**
     * get document constraints
     */
    public function getCurrentDocumentConstraint() {
        if( $documentConstraint = Mage::getStoreConfig('designcollaboration/editor_page/document_constraint')) {
            return $this->getItemId( $documentConstraint );
        }
        return false;
    }

    /**
     * @param string $type configuration parameter value
     * @return string item ID
     */
    public function getItemId($type){
        $data = explode('|', $type);
        $index = count($data) - 1;
        return $data[$index];
    }

    public function setQuoteItemId($designId, $quoteItemId)
    {
        if (!$this->getDesignByQuoteItemId($quoteItemId)) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $query = "INSERT INTO designcollaboration_design_quotes(design_id, 	quote_item_id)
                      VALUES(:design_id, :quote_item_id)";
            $binds = array(
                'design_id' => $designId,
                'quote_item_id' => $quoteItemId
            );
            $write->query($query, $binds);
        }
    }

    public function getDesignByQuoteItemId($quoteItemId)
    {
        $readConnection = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
        $query = "SELECT dc_designs.*
                  FROM designcollaboration_designs AS dc_designs
                    LEFT JOIN designcollaboration_design_quotes AS quotes ON quotes.design_id = dc_designs.design_id
                  WHERE quotes.quote_item_id = :quote_item_id
                  LIMIT 1";
        $binds = array(
            'quote_item_id' => $quoteItemId,
        );
        $result = $readConnection->fetchRow($query, $binds);

        if (count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function deleteQuotesByDesignId($designId)
    {
        $readConnection = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
        $query = "SELECT *
                  FROM designcollaboration_design_quotes
                  WHERE design_id = :design_id";
        $binds = array(
            'design_id' => $designId,
        );
        $quotes = $readConnection->fetchAll($query, $binds);

        if (count($quotes) > 0) {
            $cart = Mage::getModel('checkout/cart');
            foreach ($quotes as $quote) {
                $cart->removeItem($quote['quote_item_id']);
            }
            $cart->getQuote()->setTotalsCollectedFlag(false);
            $cart->save();
        }
    }

}
