<?php

class Fineline_Designcollaboration_Block_History extends Mage_Core_Block_Template
{

    public function canRestore($statusId)
    {
        return $statusId == 1 || $statusId == 3 || $statusId == 4;
    }

}