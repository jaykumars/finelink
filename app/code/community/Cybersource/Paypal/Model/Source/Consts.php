<?php
abstract class Cybersource_Paypal_Model_Source_Consts extends Varien_Object{

    //soap constants
    const WSDL_URL_TEST = 'https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.115.wsdl';
    const WSDL_URL_LIVE = 'https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.115.wsdl';
    const SOAP_SUCCESS = 100;


    //status codes
    const STATUS_ACCEPT = 100;

    //PayPal status codes
    const REASON_CODE_ACCEPT = 100;

    //PayPal Sale Capture Types
    const COMPLETE_CAPTURE = 'Complete';
    const PARTIAL_CAPTURE = 'NotComplete';

    const PAYPAL_EXPRESS = 'cybersourcepaypal';

    static function getSystemConfig($valin=false,$store = null)
    {
        if($valin)
        {
            if ($store) {
                return Mage::getStoreConfig('payment/cybersourcepaypal/'.$valin,$store);
            } else {
                return Mage::getStoreConfig('payment/cybersourcepaypal/'.$valin);
            }
        }else
        {
            if ($store) {
                return Mage::getStoreConfig('payment/cybersourcepaypal',$store);
            } else {
                return Mage::getStoreConfig('payment/cybersourcepaypal');
            }

        }
    }

    static function getErrorCode($codein)
    {
        //list of error responses here
        $errorArray = array(
            '150'=>Mage::helper('cybersourcepaypal')->__('A general error has occurred'),
            '151'=>Mage::helper('cybersourcepaypal')->__('The communication to your bank has failed, please try again later'),
            '152'=>Mage::helper('cybersourcepaypal')->__('The communication to your bank has failed, please try again later'),
            '250'=>Mage::helper('cybersourcepaypal')->__('The communication to your bank has failed, please try again later'),
            '203'=>Mage::helper('cybersourcepaypal')->__('Sorry your card has been declined by your bank, please try a different card or check with your bank'),
            '201'=>Mage::helper('cybersourcepaypal')->__('Your issuing bank has requested more information about the transaction, please contact them and try again'),
            '202'=>Mage::helper('cybersourcepaypal')->__('Your credit card has expired, please enter a valid card'),
            '204'=>Mage::helper('cybersourcepaypal')->__('You have insufficient funds on the account for this transaction'),
            '207'=>Mage::helper('cybersourcepaypal')->__('Sorry we are unable to reach your bank to verify this transaction, please try again.'),
            '210'=>Mage::helper('cybersourcepaypal')->__('Your card has reached its credit limit and this transaction cannot be processed'),
            '211'=>Mage::helper('cybersourcepaypal')->__('Your CVN (3 digit code) is invalid, please amend and try again'),
            '230'=>Mage::helper('cybersourcepaypal')->__('Your CVN (3 digit code) is invalid, please amend and try again'),
            '200'=>Mage::helper('cybersourcepaypal')->__('Your Billing address does not match the one registered to that card, please amend your address and try again'),
            '476'=>Mage::helper('cybersourcepaypal')->__('Your card failed the authentication process, please try again'),
            '481'=>Mage::helper('cybersourcepaypal')->__('Your card failed the fraud screening process, please check the details or try a new card'),
            '102'=>Mage::helper('cybersourcepaypal')->__('Your billing and/or shipping address details are invalid or missing.'),
            '101'=>Mage::helper('cybersourcepaypal')->__( 'The request is missing one or more required fields.'),
            '223'=>Mage::helper('cybersourcepaypal')->__( 'PayPal rejected the transaction.'),
            '233'=>Mage::helper('cybersourcepaypal')->__( 'General decline by PayPal.'),
            '234'=>Mage::helper('cybersourcepaypal')->__( 'There is a problem with the merchant configuration.'),
            '238'=>Mage::helper('cybersourcepaypal')->__( 'PayPal rejected the transaction. A successful transaction was already completed for this paypalToken value.'),
            '387'=>Mage::helper('cybersourcepaypal')->__( 'PayPal authorization failed.')
        );

        if(array_key_exists($codein,$errorArray))
        {
            return $errorArray[$codein];
        }else
        {
            return '';
        }

    }

}