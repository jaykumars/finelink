<?php

class Fineline_Designcollaboration_AjaxController extends Mage_Core_Controller_Front_Action
{

    private function renderTemplate($additionally = array())
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return false;
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);

        $data = array(
            "design" => $designModel
        );
        $data = array_merge($data, $additionally);

        if ($designModel->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('designcollaboration.content')->assign($data);
            $this->renderLayout();
        } else {
            $this->_redirectReferer(Mage::getUrl('*/*'));
        }
    }

    private function response($success, $data)
    {
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array_merge(array('success' => $success), $data)));
    }

    private function successResponse($data = array())
    {
        return $this->response(1, $data);
    }

    private function errorResponse($message)
    {
        return $this->response(0, array('message' => $this->__($message)));
    }

    private function notAvailableResponse()
    {
        return $this->response(2,
            array('message' => $this->__('Item has been opened for editing and does not need approval at this time'))
        );
    }

    public function modalsavetomydesignAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return false;
        }

        $this->loadLayout();
        $this->getLayout()->getBlock('designcollaboration.content')->assign(array(
            "documentId" => Mage::app()->getRequest()->getParam('documentId'),
            "productId" => Mage::app()->getRequest()->getParam('productId'),
            "designName" => Mage::app()->getRequest()->getParam('designName')
        ));
        $this->renderLayout();
    }

    public function modalsharingAction()
    {
        $this->renderTemplate();
    }

    public function modalapproveAction()
    {
        $this->renderTemplate();
    }

    public function modalsendforapprovalAction()
    {
        $this->renderTemplate();
    }

    public function modalcreatenewdesignAction()
    {
        $this->renderTemplate();
    }

    public function modalsaveandunlockAction()
    {
        $this->renderTemplate();
    }

    public function modalrevokeordenyAction()
    {
        $this->renderTemplate(array("showRevoke" => Mage::app()->getRequest()->getParam('showRevoke')));
    }

    public function modalrestoreAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return false;
        }

        $historyId = Mage::app()->getRequest()->getParam('historyId');
        $historyModel = Mage::getModel('designcollaboration/history')->load($historyId);

        if ($historyModel->getId() > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('designcollaboration.content')->assign(array(
                "history" => $historyModel
            ));
            $this->renderLayout();
        } else {
            $this->_redirectReferer(Mage::getUrl('*/*'));
        }
    }

    public function restoredesignAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $message = Mage::app()->getRequest()->getParam('message');
        if (empty($message) || is_null($message)) {
            return $this->errorResponse('Message is not found');
        }

        $historyId = Mage::app()->getRequest()->getParam('historyId');
        $historyModel = Mage::getModel('designcollaboration/history')->load($historyId);
        $designModel = Mage::getModel('designcollaboration/designs')->load($historyModel->getDesignId());


        if ($customerData->getId() != $designModel->getCreatedBy() &&
            !Mage::helper('designcollaboration')->getIsApproval($designModel->getDesignId()) ) {
            if (!Mage::helper('designcollaboration')->hasSharings($designModel->getDesignId(), $customerData->getId())) {
                return $this->errorResponse('Access denied');
            }
        }

        $updateDesign = array('status_id' => 1,
            'finelink_item_id' => $historyModel->getFinelinkItemId(),
            'modified_at' => Mage::getModel('core/date')->timestamp(time()),
            'modified_by' => $customerData->getId());
        $designModelUpdate = Mage::getModel('designcollaboration/designs')->load($historyModel->getDesignId())->addData($updateDesign);

        try {
            $designModelUpdate->save();

            $newItemId = Mage::getModel('designcollaboration/api')->copyDocument($designModel->getFinelinkItemId(), $designModel->getDesignname());

            $insertHistory = array('design_id' => $designModel->getDesignId(),
                'finelink_item_id' => $newItemId,
                'created_by' => $customerData->getId(),
                'created_at' => Mage::getModel('core/date')->timestamp(time()));
            $historyModelInsert = Mage::getModel('designcollaboration/history')->setData($insertHistory);
            $newHistoryId = $historyModelInsert->save()->getId();
            Mage::getModel('designcollaboration/comments')->updateLastCommentByHistoryId($designModel->getId(), $newHistoryId);
            Mage::getModel('designcollaboration/comments')->addPost($historyModel->getDesignId(), $customerData->getId(), $message, 7, null);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }

        return $this->successResponse();
    }

    public function savedesignAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $newItemId = Mage::getModel('designcollaboration/api')->copyDocument(Mage::app()->getRequest()->getParam('documentId'), Mage::app()->getRequest()->getParam('designName'));
        if (is_null($newItemId)) {
            return $this->errorResponse('Error copy document on API Server');
        }

        $newDesign = array('designname' => trim(Mage::app()->getRequest()->getParam('designName')),
            'finelink_item_id' => $newItemId,
            'product_id' => Mage::app()->getRequest()->getParam('productId'),
            'created_by' => $customerData->getId(),
            'created_at' => Mage::getModel('core/date')->timestamp(time()));

        $designModel = Mage::getModel('designcollaboration/designs')->setData($newDesign);

        try {
            $designModel->save();
            $insertId = $designModel->getId();
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), null, 1);
            return $this->successResponse(array('insertedId' => $insertId));
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function removesharingAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $removeIds = Mage::app()->getRequest()->getParam('remove');
        if (!is_array($removeIds) || count($removeIds) == 0) {
            return $this->errorResponse('RemovesIds is not array');
        }

        $removedId = array();

        foreach ($removeIds as $removeSharringId) {
            $sharingId = Mage::getModel('designcollaboration/sharings')->getCollection()
                ->addFieldToFilter('sharing_id', $removeSharringId)
                ->setPageSize(1)
                ->getFirstItem()
                ->getSharingId();
            $sharingModel = Mage::getModel('designcollaboration/sharings')->load($sharingId);
            $designId = $sharingModel->getDesignId();
            $customerId = $sharingModel->getCustomerId();

            if (Mage::getModel('designcollaboration/sharings')->getLockedBySharingCustomer($designId, $sharingModel->getCustomerId())) {
                Mage::getModel('designcollaboration/designs')->setStatusUnlocked($designId, $customerData->getId(), false);
            }

            try {
                $sharingModel->delete();
                Mage::getModel('designcollaboration/notifications')->removeNotification($designId, $customerId);
                Mage::getModel('designcollaboration/unregisteredusers')->removeUnregisteredUsers($sharingModel->getUnregisteredUserId());
                $removedId[] = $removeSharringId;
            } catch (Exception $e) {
                return $this->errorResponse($e->getMessage());
            }
        }

        return $this->successResponse(array('removedId' => $removedId));
    }

    public function savesharingAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $requestEmails = Mage::app()->getRequest()->getParam('emails');
        $emails = explode(",", $requestEmails);
        if (!is_array($emails) || count($emails) == 0) {
            return $this->errorResponse('Emails is not array');
        }

        $customers = array();
        $templateParams = array();

        foreach ($emails as $email) {
            $email = trim($email);
            $customer = Mage::getModel("customer/customer")
                ->setWebsiteId(Mage::app()->getWebsite()->getId())
                ->loadByEmail($email)
                ->getData();

            if (count($customer) > 0) {
                $customerName = $customer['firstname'] . " " . $customer['lastname'];
                $field = 'customer_id';
                $idCustomer = $customer['entity_id'];
            } else {
                $customerName = 'Unregistered user';
                $field = 'unregistered_user_id';
                $idCustomer = Mage::getModel('designcollaboration/unregisteredusers')->getUnregisteredUserByEmail($email);
            }

            $designsCollection = Mage::getModel('designcollaboration/designs')->getCollection()
                ->addFilter('design_id', Mage::app()->getRequest()->getParam('designId'), 'AND')
                ->addFilter('created_by', $customer['entity_id'], 'AND')
                ->setPageSize(1)
                ->getFirstItem();

            // check customer to creator
            if ($designsCollection->getDesignId() < 1 && $idCustomer > 0) {

                $sharingsCollection = Mage::getModel('designcollaboration/sharings')->getCollection()
                    ->addFilter('design_id', Mage::app()->getRequest()->getParam('designId'), 'AND')
                    ->addFilter($field, $idCustomer, 'AND')
                    ->setPageSize(1)
                    ->getFirstItem();

                // check customer to sharing
                if ($sharingsCollection->getSharingId() < 1) {

                    $newSharring = array('design_id' => Mage::app()->getRequest()->getParam('designId'),
                        'created_at' => Mage::getModel('core/date')->timestamp(time()),
                        'created_by' => $customerData->getId());
                    $newSharring[$field] = $idCustomer;

                    $sharringModel = Mage::getModel('designcollaboration/sharings')->setData($newSharring);

                    try {
                        $sharringModel->save();
                        $customers[] = array('sharingId' => $sharringModel->getId(),
                            'name' => $customerName,
                            'email' => $email);

                        // get data for email
                        $designModel = Mage::getModel('designcollaboration/designs')->getCollection()
                            ->addFilter('design_id', Mage::app()->getRequest()->getParam('designId'))
                            ->setPageSize(1)
                            ->getFirstItem();

                        $templateParams['designName'] = $designModel->getDesignname();
                        $templateParams['customerName'] = $customerName;
                        $templateParams['fromName'] = $customerData->getFirstname() . " " . $customerData->getLastname();
                        $templateParams['viewLink'] = Mage::helper('designcollaboration')->getViewUrl($designModel->getDesignId());

                        // send email to customer
                        Mage::getModel('designcollaboration/sharings')->sendMailSharing($email, $customerData, $templateParams);
                    } catch (Exception $e) {
                        return $this->errorResponse($e->getMessage());
                    }
                }
            }
        }

        return $this->successResponse(array('customers' => $customers));
    }

    public function approvedesignAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        if (is_null($designId) || $designId < 1 || $designId == '') {
            return $this->errorResponse('Design is not found');
        }

        $designModel = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->setPageSize(1)
            ->getFirstItem();

        if (Mage::helper('designcollaboration')->isLockedDesign($designId)) {
            return $this->notAvailableResponse();
        }

        $updateData = array('status_id' => 4,
            'approved_at' => Mage::getModel('core/date')->timestamp(time()),
            'approved_by' => $customerData->getId());
        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);
        try {
            $updateDesign->setId($designId)->save();
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), null, 4);
            return $this->successResponse();
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function revokeordenydesignAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        if (is_null($designId) || $designId < 1 || $designId == '') {
            return $this->errorResponse('Design is not found');
        }

        $designModel = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->setPageSize(1)
            ->getFirstItem();

        if (Mage::helper('designcollaboration')->isLockedDesign($designId)) {
            return $this->notAvailableResponse();
        }
        $commentsStatus = $designModel->getStatusId() == 3 ? 5 : 6;

        $updateData = array('status_id' => 1,
            'approved_at' => null,
            'approved_by' => null);
        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);
        try {
            $updateDesign->setId($designId)->save();
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), null, $commentsStatus);
            return $this->successResponse();
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function sendforapprovalAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            // this demo show error
            Mage::getSingleton("core/session")->addError($this->__('Session timed out. Please try again login'));
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        if (is_null($designId) || $designId < 1 || $designId == '') {
            return $this->errorResponse('Design is not found');
        }

        $designModel = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->setPageSize(1)
            ->getFirstItem();

        if (!Mage::helper('designcollaboration')->checkCustomerAccess($designId)) {
            return $this->errorResponse('Access denied');
        }

        if ($designModel->getStatusId() != 1) {
            return $this->notAvailableResponse();
        }

        $updateData = array(
            'status_id' => 3,
            'modified_at' => Mage::getModel('core/date')->timestamp(time()),
            'modified_by' => $customerData->getId(),
            'send_for_approve_by' => $customerData->getId(),
        );
        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);
        try {
            $updateDesign->setId($designId)->save();
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), null, 3);
            Mage::getModel('designcollaboration/notifications')->sendNotificationsForApprovers($designModel, $customerData->getId());
            return $this->successResponse();
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function receivenotificationsAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);

        if ($designModel->getId() < 1) {
            return $this->errorResponse('Design is not found');
        }

        $notify = Mage::app()->getRequest()->getParam('notify');
        if ($notify != true && $notify != false) {
            return $this->errorResponse('Parameter is not valid');
        }

        if ($notify == 'true') {
            $resultSubscribe = Mage::getModel('designcollaboration/notifications')->subscribeNotification($designModel->getId(), $customerData->getId());
        } else {
            $resultSubscribe = Mage::getModel('designcollaboration/notifications')->removeNotification($designModel->getId(), $customerData->getId());
        }

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($resultSubscribe));
    }

    public function extendtimelockedAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);

        if ($designModel->getId() < 1) {
            return $this->errorResponse('Design is not found');
        }

        if ($designModel->getLockedBy() != $customerData->getId()) {
            return $this->errorResponse('Access denied');
        }

        $result = Mage::getModel('designcollaboration/designs')->extendTimeLocked($designId, $customerData->getId());

        if ($result) {
            return $this->successResponse();
        }

        return $this->errorResponse($result);
    }

    public function saveandunlockAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);

        if ($designModel->getId() < 1) {
            return $this->errorResponse('Design is not found');
        }

        $documentId = Mage::app()->getRequest()->getParam('documentId');
        if (empty($documentId) || is_null($documentId)) {
            return $this->errorResponse('Document is not found');
        }

        $message = Mage::app()->getRequest()->getParam('message');
        if (empty($message) || is_null($message)) {
            return $this->errorResponse('Message is not found');
        }

        $designName = Mage::app()->getRequest()->getParam('designName');
        if (empty($designName) || is_null($designName)) {
            return $this->errorResponse('New design name is not found');
        }

        if (Mage::helper('designcollaboration')->isLockedDesign($designModel->getId(), true)) {
            return $this->errorResponse('Design is not edit');
        }

        if ($designModel->getLockedBy() != $customerData->getId()) {
            return $this->errorResponse('Access denied');
        }

        if ($customerData->getId() != $designModel->getCreatedBy()) {
            if (!Mage::helper('designcollaboration')->hasSharings($designId, $customerData->getId()) &&
                !Mage::helper('designcollaboration')->getIsApproval($designModel->getDesignId()) ) {
                return $this->errorResponse('Access denied');
            }
        }

        $editorXML = Mage::app()->getRequest()->getParam('editorXML');

        Mage::getModel('designcollaboration/history')->addDesignHistory($designModel, $designName, $customerData, $editorXML, $message);

        return $this->successResponse();
    }

    public function addtocardsAction()
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            return $this->errorResponse('Request is not ajax');
        }

        $cart = Mage::getModel('checkout/cart');
        $cart->init();

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (is_null($customerData->getId())) {
            return $this->errorResponse('Please login');
        }

        $designId = Mage::app()->getRequest()->getParam('designId');
        $designModel = Mage::getModel('designcollaboration/designs')->load($designId);

        if ($designModel->getId() < 1) {
            return $this->errorResponse('Design is not found');
        }

//        if ($designModel->getQuoteItemId() > 0) {
//            $cart->removeItem($designModel->getQuoteItemId())->save();
//        }

        $product = Mage::getModel('catalog/product')->load($designModel->getProductId());

        if (!$product->getId()) {
            return $this->errorResponse('Cannot add item to shopping cart');
        }

        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        $addItem = array(
            'product' => $designModel->getProductId(),
            'qty' => $stock->getMinSaleQty()
        );

        try {
            //For web2print
            $product->setNewDocumentId($designModel->getFinelinkItemId());
            //For connecting quote item with added product
            $product->setDcDesignId($designId);

            $cart->addProduct($product, $addItem);
            $cart->getQuote()->setTotalsCollectedFlag(false);
            $cart->save();

            //use checkout/session
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }

        return $this->successResponse();
    }

}
