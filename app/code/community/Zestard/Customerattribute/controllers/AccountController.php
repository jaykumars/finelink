<?php

require_once 'Mage/Customer/controllers/AccountController.php';
class Zestard_Customerattribute_AccountController extends Mage_Customer_AccountController
{
    public function createPostAction()
    {
		
        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));

        if (!$this->_validateFormKey()) {
            $this->_redirectError($errUrl);
            return;
        }

        /** @var $session Mage_Customer_Model_Session */
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        if (!$this->getRequest()->isPost()) {
            $this->_redirectError($errUrl);
            return;
        }

        $customer = $this->_getCustomer();

        try {
            $errors = $this->_getCustomerErrors($customer);
			
            if (empty($errors)) {
                $collections = Mage::getModel('zestard_customerattribute/customerattribute')->getCollection()				
					->addFieldToFilter('is_unique',1)
					->getColumnValues('attribute_code');
				
				foreach($customer->getData() as $key => $value)
                {					
					//~ if($value != NULL)
					//~ {
						if(in_array($key,$collections))
						{
							$customerCollection = Mage::getModel('customer/customer')
								->getCollection()
								->addAttributeToSelect('*')
								->addAttributeToFilter($key,array('eq'=>$value));
							if(count($customerCollection))
							{	
								$attributeData = Mage::helper('zestard_customerattribute')->loadByAttributeCode($key);							
								Mage::throwException("The ".$attributeData['frontend_label']." value is allready used, please try another.");					
							}
						}
					//}
				}
				
                $customer->cleanPasswordsValidationData();                
                $customer->save();                
                $this->_dispatchRegisterSuccess($customer);
                $this->_successProcessRegistration($customer);
                return;
            } else {
                $this->_addSessionError($errors);
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
            } else {
                $message = $this->_escapeHtml($e->getMessage());
            }
            $session->addError($message);
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            $session->addException($e, $this->__('Cannot save the customer.'));
        }

        $this->_redirectError($errUrl);
    }
    
    public function editPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/edit');
        }

        if ($this->getRequest()->isPost()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getSession()->getCustomer();

            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = $this->_getModel('customer/form');
            $customerForm->setFormCode('customer_account_edit')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());

            $errors = array();
            $customerErrors = $customerForm->validateData($customerData);
            if ($customerErrors !== true) {
                $errors = array_merge($customerErrors, $errors);
            } else {
                $customerForm->compactData($customerData);
                $errors = array();

                // If password change was requested then add it to common validation scheme
                if ($this->getRequest()->getParam('change_password')) {
                    $currPass   = $this->getRequest()->getPost('current_password');
                    $newPass    = $this->getRequest()->getPost('password');
                    $confPass   = $this->getRequest()->getPost('confirmation');

                    $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
                    if ( $this->_getHelper('core/string')->strpos($oldPass, ':')) {
                        list($_salt, $salt) = explode(':', $oldPass);
                    } else {
                        $salt = false;
                    }

                    if ($customer->hashPassword($currPass, $salt) == $oldPass) {
                        if (strlen($newPass)) {
                            /**
                             * Set entered password and its confirmation - they
                             * will be validated later to match each other and be of right length
                             */
                            $customer->setPassword($newPass);
                            $customer->setPasswordConfirmation($confPass);
                        } else {
                            $errors[] = $this->__('New password field cannot be empty.');
                        }
                    } else {
                        $errors[] = $this->__('Invalid current password');
                    }
                }

                // Validate account and compose list of errors if any
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                    $errors = array_merge($errors, $customerErrors);
                }
            }

            if (!empty($errors)) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
                foreach ($errors as $message) {
                    $this->_getSession()->addError($message);
                }
                $this->_redirect('*/*/edit');
                return $this;
            }

            try {		
				$collections = Mage::getModel('zestard_customerattribute/customerattribute')->getCollection()				
					->addFieldToFilter('is_unique',1)
					->getColumnValues('attribute_code');
				$customerData = Mage::getSingleton('customer/session')->getCustomer();		
				foreach($customer->getData() as $key => $value)
                {					
					if(in_array($key,$collections))
					{
						
						$customerCollection = Mage::getModel('customer/customer')
							->getCollection()
							->addAttributeToSelect('*')
							->addFieldToFilter('entity_id',array('neq'=>$customerData->getEntityId()))
							->addAttributeToFilter($key,array('eq'=>$value));
						if(count($customerCollection))
						{
							$attributeData = Mage::helper('zestard_customerattribute')->loadByAttributeCode($key);
							Mage::throwException("The ".$attributeData['frontend_label']." value is allready used, please try another.");					
						}
					}
				}						
                $customer->cleanPasswordsValidationData();
                $customer->save();
                $this->_getSession()->setCustomer($customer)
                    ->addSuccess($this->__('The account information has been saved.'));

                $this->_redirect('customer/account');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }
        }

        $this->_redirect('*/*/edit');
    } 
}

?>
