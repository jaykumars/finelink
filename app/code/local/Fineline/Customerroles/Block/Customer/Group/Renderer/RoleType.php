<?php

class Fineline_Customerroles_Block_Customer_Group_Renderer_RoleType extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
      public function render(Varien_Object $row)
      {
          $value =  $row->getData($this->getColumn()->getIndex());
          $out = '';
          switch ($value) {
              case 0:
                $out = 'Requisitioner';
                break;
              case 1:
                $out = 'Purchaser';
                break;
              case 2:
                $out = 'Super Purchaser';
                break;
          }
          return "<h5>$out</h5>";


      }
}
