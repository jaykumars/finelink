<?php

class Fineline_Designcollaboration_Block_Edit extends Mage_Core_Block_Template
{
    public function lockedDesign($designId)
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        if (!is_null($customerData->getId())) {
            Mage::getModel('designcollaboration/designs')->extendTimeLocked($designId, $customerData->getId());
        }
    }

}