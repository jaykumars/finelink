/**
 * Open generic resource browser for each type
 */
function openResourceBrowser(baseUrl, callback) {
    new Ajax.Request(baseUrl, {
        method: 'get',
        parameters: 'jow=jow',
        onSuccess: function (transport, json) {
            var response = transport.responseText.evalJSON();

            windowResource = new Window({
                className: 'magento',
                title: 'Image Conversion Profiles',
                width: 600,
                height: 400,
                minimizable: false,
                maximizable: false,
                showEffectOptions: {duration: 0.4},
                hideEffectOptions: {duration: 0.4}
            });
            windowResource.setHTMLContent(response.result);
            windowResource.setZIndex(100);
            windowResource.showCenter(true);
            document.activeResourceWindow = windowResource.getId();
        }
    });
}

function resourceWindowSelect(val, element) {
    var humanReadableArray = val.split('|');
    var humanReadableValue = humanReadableArray[0];

    $(element).value = val;
    $(element + '_value').value = humanReadableValue;
    Windows.close(document.activeResourceWindow);
}

function clearValue(element) {
    var id = element.id + '_value';
    $(element).value = '';
    $(id).value = '';
}
