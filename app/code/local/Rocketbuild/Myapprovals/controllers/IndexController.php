<?php

class Rocketbuild_Myapprovals_IndexController extends Mage_Core_Controller_Front_Action {

	/**
	 * Checking if user is logged in or not
	 * If not logged in then redirect to customer login
	 */
	public function preDispatch()
	{
		parent::preDispatch();

		if (!Mage::getSingleton('customer/session')->authenticate($this)) {
			$this->setFlag('', 'no-dispatch', true);

			// adding message in customer login page
			Mage::getSingleton('core/session')
			    ->addSuccess(Mage::helper('myapprovals')->__('Please sign in or create a new account'));
		}
	}

	public function IndexAction() {

		$this->loadLayout();
		$this->getLayout()->getBlock("head")->setTitle($this->__("My Approvals"));
		$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
		// $breadcrumbs->addCrumb("account", array(
		// 	"label" => $this->__("My Account"),
		// 	"title" => $this->__("My Account"),
		// 	"link"  => "/customer/account/"
		// ));

		// $breadcrumbs->addCrumb("myapprovals", array(
		// 	"label" => $this->__("My Approvals"),
		// 	"title" => $this->__("My Approvals")
		// ));

		$this->renderLayout();
	}

	public function savePaymentMethodAction() {
		$data = $this->getRequest()->getPost('payment', array());
		$id = $this->getRequest()->getPost('order_id');

		if (empty($data)) {
            $this->_redirect('myapprovals/index/view',array('order_id'=>$id));
        }

		$quote = Mage::getModel('sales/quote')->load($id);

        if ($quote->isVirtual()) {
            $quote->getBillingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        } else {
            $quote->getShippingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        }

        // shipping totals may be affected by payment method
        if (!$quote->isVirtual() && $quote->getShippingAddress()) {
            $quote->getShippingAddress()->setCollectShippingRates(true);
        }

        $payment = $quote->getPayment();
        $payment->importData($data);

        $quote->save();

		$this->_redirect('myapprovals/index/view',array('order_id'=>$id));
	}

	public function SaveShippingMethodAction() {
		$id = $this->getRequest()->getParam('order_id');
		$methodCode = $this->getRequest()->getParam('shippingmethod');

		$quote = Mage::getModel('sales/quote')->load($id);

		$quote->getShippingAddress()->setShippingMethod($methodCode);

		$quote->getBillingAddress();
		$quote->getShippingAddress()->setCollectShippingRates(true);
		$quote->collectTotals();
		$quote->save();

		$this->_redirect('myapprovals/index/view',array('order_id'=>$id));
	}

	public function SaveBillingAddressAction() {
		$id = $this->getRequest()->getParam('order_id');
		$firstname = $this->getRequest()->getParam('billingfirstname');
		$middlename = $this->getRequest()->getParam('billingmiddlename');
		$lastname = $this->getRequest()->getParam('billinglastname');
		$company = $this->getRequest()->getParam('billingcompany');
		$street1 = $this->getRequest()->getParam('billingstreet');
		$street2 = $this->getRequest()->getParam('billingstreet2');
		$city = $this->getRequest()->getParam('billingcity');
		$region = $this->getRequest()->getParam('billingregion');
		$regionId = $this->getRequest()->getParam('billingregionid');
		$postcode = $this->getRequest()->getParam('billingpostcode');
		$country = $this->getRequest()->getParam('billingcountryid');
		$telephone = $this->getRequest()->getParam('billingtelephone');
		$fax = $this->getRequest()->getParam('billingfax');

		if (empty($region) && !empty($regionId)) {
			$regionObj = Mage::getModel('directory/region')->load($regionId);
			$region = $regionObj->getName();
		}


		$quote = Mage::getModel('sales/quote')->load($id);

		$quote->getBillingAddress()->setFirstname($firstname);
		$quote->getBillingAddress()->setMiddlename($middlename);
		$quote->getBillingAddress()->setLastname($lastname);
		$quote->getBillingAddress()->setCompany($company);
		$quote->getBillingAddress()->setStreet(array($street1,$street2));
		$quote->getBillingAddress()->setCity($city);
		$quote->getBillingAddress()->setRegion($region);
		$quote->getBillingAddress()->setRegionId($regionId);
		$quote->getBillingAddress()->setPostcode($postcode);
		$quote->getBillingAddress()->setCountryId($country);
		$quote->getBillingAddress()->setTelephone($telephone);
		$quote->getBillingAddress()->setFax($fax);

		$quote->getBillingAddress();
		$quote->getShippingAddress()->setCollectShippingRates(true);
		$quote->collectTotals();
		$quote->save();

		$this->_redirect('myapprovals/index/view',array('order_id'=>$id));
	}

	public function SaveShippingAddressAction() {
		$id = $this->getRequest()->getParam('order_id');
		$firstname = $this->getRequest()->getParam('shippingfirstname');
		$middlename = $this->getRequest()->getParam('shippingmiddlename');
		$lastname = $this->getRequest()->getParam('shippinglastname');
		$company = $this->getRequest()->getParam('shippingcompany');
		$street1 = $this->getRequest()->getParam('shippingstreet');
		$street2 = $this->getRequest()->getParam('shippingstreet2');
		$city = $this->getRequest()->getParam('shippingcity');
		$region = $this->getRequest()->getParam('shippingregion');
		$regionId = $this->getRequest()->getParam('shippingregionid');
		$postcode = $this->getRequest()->getParam('shippingpostcode');
		$country = $this->getRequest()->getParam('shippingcountryid');
		$telephone = $this->getRequest()->getParam('shippingtelephone');
		$fax = $this->getRequest()->getParam('shippingfax');

		if (empty($region) && !empty($regionId)) {
			$regionObj = Mage::getModel('directory/region')->load($regionId);
			$region = $regionObj->getName();
		}


		$quote = Mage::getModel('sales/quote')->load($id);

		$quote->getShippingAddress()->setFirstname($firstname);
		$quote->getShippingAddress()->setMiddlename($middlename);
		$quote->getShippingAddress()->setLastname($lastname);
		$quote->getShippingAddress()->setCompany($company);
		$quote->getShippingAddress()->setStreet(array($street1,$street2));
		$quote->getShippingAddress()->setCity($city);
		$quote->getShippingAddress()->setRegion($region);
		$quote->getShippingAddress()->setRegionId($regionId);
		$quote->getShippingAddress()->setPostcode($postcode);
		$quote->getShippingAddress()->setCountryId($country);
		$quote->getShippingAddress()->setTelephone($telephone);
		$quote->getShippingAddress()->setFax($fax);

		$quote->getBillingAddress();
		$quote->getShippingAddress()->setCollectShippingRates(true);
		$quote->collectTotals();
		$quote->save();

		$this->_redirect('myapprovals/index/view',array('order_id'=>$id));
	}

	public function ViewAction() {

		$this->loadLayout();
		$this->getLayout()->getBlock("head")->setTitle($this->__("My Approvals"));
		$this->getLayout()->getBlock('head')->addItem('skin_js', 'js/opcheckout.js');
		// $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
		// $breadcrumbs->addCrumb("account", array(
		// 	"label" => $this->__("My Account"),
		// 	"title" => $this->__("My Account"),
		// 	"link"  => "/customer/account/"
		// ));
		//
		// $breadcrumbs->addCrumb("myapprovals", array(
		// 	"label" => $this->__("My Approvals"),
		// 	"title" => $this->__("My Approvals")
		// ));

		$this->renderLayout();
	}

	public function PartialRejectionAction() {
		$id = $this->getRequest()->getParam('id');
		$rejectNote = $this->getRequest()->getParam('note');
		$items = $this->getRequest()->getParam('denied');
		$itemIds = explode(',', $items);

		$quoteObj = Mage::getModel('sales/quote')->load($id); // Mage_Sales_Model_Quote

		$rejected = array();
		foreach ($itemIds as $id) {
			$item = Mage::getModel('sales/quote_item')->load($id);
			$rejected[] = $item->getName();
			$quoteObj->removeItem($id);
		}
		$quoteObj->getShippingAddress()->setCollectShippingRates(true);
		$quoteObj->collectTotals();

		if (count($quoteObj->getAllItems()) == 0) {
			$quoteObj->setApprovalState(4);
			$message = " The requisition has been closed.";
			$redirect = 'myapprovals/index';
		}else{
			$quoteObj->setApprovalState(3);
			$redirect = 'myapprovals/index/view/order_id/'.$quoteObj->getId();
			$message = "";
		}

		$quoteObj->save();

		// TODO: send email;
		Mage::helper('myapprovals')->sendPartialRejectionEmail($quoteObj, $rejected, $rejectNote);

		Mage::getSingleton('core/session')->addSuccess("The selected items have been denied.".$message);
		$this->_redirect($redirect);
	}

	public function PartialApprovalAction() {
		$id = $this->getRequest()->getParam('id');
		$items = $this->getRequest()->getParam('approved');
		$itemIds = explode(',', $items);

		$quoteObj = Mage::getModel('sales/quote')->load($id); // Mage_Sales_Model_Quote
		$customer = Mage::getModel('customer/customer')->load($quoteObj->getCustomerId());

		$newQuote = Mage::getModel('sales/quote');

		foreach ($quoteObj->getAllVisibleItems() as $item) {

			if (array_search($item->getId(), $itemIds) === false){
				continue;
			}

            $found = false;
            foreach ($newQuote->getAllItems() as $quoteItem) {
                if ($quoteItem->compare($item)) {
                    $quoteItem->setQty($quoteItem->getQty() + $item->getQty());
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $newItem = clone $item;
                $newQuote->addItem($newItem);
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        $newChild = clone $child;
                        $newChild->setParentItem($newItem);
                        $newQuote->addItem($newChild);
                    }
                }
            }
        }

		if ($quoteObj->getCouponCode()) {
            $newQuote->setCouponCode($quoteObj->getCouponCode());
        }

		$newQuote->getBillingAddress();
		$newQuote->getShippingAddress();

		$newQuote->setCustomer($customer);

		$newPayment = $newQuote->getPayment();
		$newPayment->addData($quoteObj->getPayment()->getData());
		$newPayment->setQuoteId(null);
		$newPayment->setPaymentId(null);
		$newPayment->setCreatedAt(null);
		$newPayment->setUpdateAt(null);

		$billingaddress = Mage::getModel('sales/quote_address')->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING);
		$billingaddress->addData($quoteObj->getBillingAddress()->getData());

		$billingaddress->setQuoteId(null);
		$billingaddress->setAddressId(null);
		$newQuote->setBillingAddress($billingaddress);

		$shippingaddress = Mage::getModel('sales/quote_address')->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);
		$shippingaddress->addData($quoteObj->getShippingAddress()->getData());

		$shippingaddress->setQuoteId(null);
		$shippingaddress->setAddressId(null);
		$newQuote->setShippingAddress($shippingaddress);

		$shippingMethod = $quoteObj->getShippingAddress()->getShippingMethod();

		$newQuote->getShippingAddress()->setShippingMethod($shippingMethod);
		$newQuote->getShippingAddress()->setCollectShippingRates(true);

		$newQuote->collectTotals();

		$newQuote->setIsActive(0);
		$newQuote->setApprovalIsRequisition(1);
		$newQuote->setApprovalState(1);
		$newQuote->save();

		$orderObj = $this->approveQuote($newQuote);

		foreach ($itemIds as $id) {
			$quoteObj->removeItem($id);
		}
		$quoteObj->getShippingAddress()->setCollectShippingRates(true);
		$quoteObj->collectTotals();

		if (count($quoteObj->getAllItems()) == 0) {
			$quoteObj->setApprovalState(4);
			$redirect = "myapprovals/index";
			$message = " The requisition has been closed.";
		}else{
			$quoteObj->setApprovalState(3);
			$redirect = "myapprovals/index/view/order_id/".$quoteObj->getId();
			$message = "";
		}

		$quoteObj->save();

		// TODO: send email
		Mage::helper('myapprovals')->sendPartialApprovalEmail($quoteObj, $orderObj);

		$orderId = $orderObj->getIncrementId();
		Mage::getSingleton('core/session')->addSuccess("The selected items have been approved! Order #$orderId.".$message);
		$this->_redirect($redirect);

	}

	private function approveQuote($quoteObj) {
		$items = $quoteObj->getAllItems();


		$quoteObj->reserveOrderId();

		// set payment method
		$quotePaymentObj = $quoteObj->getPayment(); // Mage_Sales_Model_Quote_Payment
		$quoteObj->setPayment($quotePaymentObj);

		// convert quote to order
		$convertQuoteObj = Mage::getSingleton('sales/convert_quote');
		$orderObj = $convertQuoteObj->addressToOrder($quoteObj->getShippingAddress());
		$orderPaymentObj = $convertQuoteObj->paymentToOrderPayment($quotePaymentObj);

		// convert quote addresses
		$orderObj->setBillingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getBillingAddress()));
		$orderObj->setShippingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getShippingAddress()));

		// set payment options
		$orderObj->setPayment($convertQuoteObj->paymentToOrderPayment($quoteObj->getPayment()));
//		if ($paymentData) {
//			$orderObj->getPayment()->setCcNumber($paymentData->ccNumber);
//			$orderObj->getPayment()->setCcType($paymentData->ccType);
//			$orderObj->getPayment()->setCcExpMonth($paymentData->ccExpMonth);
//			$orderObj->getPayment()->setCcExpYear($paymentData->ccExpYear);
//			$orderObj->getPayment()->setCcLast4(substr($paymentData->ccNumber,-4));
//		}

		// convert quote items
		foreach ($items as $item) {
			// @var $item Mage_Sales_Model_Quote_Item
			$orderItem = $convertQuoteObj->itemToOrderItem($item);

			$options = array();
			if ($productOptions = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct())) {

				$options = $productOptions;
			}
			if ($addOptions = $item->getOptionByCode('additional_options')) {
				$options['additional_options'] = unserialize($addOptions->getValue());
			}
			if(isset($options["info_buyRequest"])) {
				$info = $options["info_buyRequest"];
				foreach ($info as $key => $value){
					$options[$key] = $value;
				}
			}
			if ($options) {
				$orderItem->setProductOptions( $options );
			}
			if ($item->getParentItem()) {
				$orderItem->setParentItem($orderObj->getItemByQuoteItemId($item->getParentItem()->getId()));
			}
			$orderObj->addItem($orderItem);
		}

		$orderObj->setCanShipPartiallyItem(false);

		try {
			$orderObj->save();
			$orderObj->place();
		} catch (Exception $e){
			Mage::log($e->getMessage());
			Mage::log($e->getTraceAsString());
		}

		$orderObj->save();
		return $orderObj;
	}

	public function ApproveAction() {
		$id = $this->getRequest()->getParam('id');

		$quoteObj = Mage::getModel('sales/quote')->load($id); // Mage_Sales_Model_Quote
		$quoteObj->setApprovalState(1);
		$quoteObj->save();

		$orderObj = $this->approveQuote($quoteObj);

		// TODO send approval email
		Mage::helper('myapprovals')->sendApprovalEmail($orderObj);


		$orderId = $orderObj->getIncrementId();
		Mage::getSingleton('core/session')->addSuccess("Requisition #$id has been approved! Order #$orderId");
		$this->_redirect('myapprovals/index');
	}

	public function DenyAction() {
		$id = $this->getRequest()->getParam('id');
		$rejectNote = $this->getRequest()->getParam('note');

		$quoteObj = Mage::getModel('sales/quote')->load($id); // Mage_Sales_Model_Quote
		$quoteObj->setApprovalState(2);
		$quoteObj->setApprovalRejectNote($rejectNote);
		$quoteObj->save();

		// TODO send rejection email
		Mage::helper('myapprovals')->sendRejectionEmail($quoteObj, $rejectNote);

		Mage::getSingleton('core/session')->addSuccess("Requisition #$id has been denied.");
		$this->_redirect('myapprovals/index');
	}
}
