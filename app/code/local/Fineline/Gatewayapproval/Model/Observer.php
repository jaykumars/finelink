<?php

class Fineline_Gatewayapproval_Model_Observer extends Varien_Event_Observer
{
   public function setCostcenter($observer) {
    $newCostcenter = Mage::app()->getRequest()->getParam('gateway_cost_center');
    $quote = Mage::getSingleton('checkout/session')->getQuote();
    $quote->setData('customer_cost_center', $newCostcenter)->save();
    
   }
    public function setReasonrequested($observer) {
    $reasonRequested = Mage::app()->getRequest()->getParam('reason_request');
    $quote = Mage::getSingleton('checkout/session')->getQuote();
    $quote->setData('reason_request', $reasonRequested)->save();
    
   }

    public function setOrderReasonrequested($observer) {

        $order = $observer->getEvent()->getOrder();
        $orderData = $order->getData();
        $quoteId = $orderData['quote_id'];
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        $quoteData = $quote->getData();
        $reasonRequest = $quoteData['reason_request'];
        $order->setData('reason_request', $reasonRequest)->save();
    }    

    public function setOrderCostcenter($observer) {

    	$order = $observer->getEvent()->getOrder();
    	$orderData = $order->getData();
    	$quoteId = $orderData['quote_id'];
    	$quote = Mage::getModel('sales/quote')->load($quoteId);
    	$quoteData = $quote->getData();
    	$costCenter = $quoteData['customer_cost_center'];
    	$order->setData('cost_center', $costCenter)->save();
	}
}
?>
