<?php

class Fineline_Designcollaboration_Block_Designs extends Mage_Core_Block_Template
{
    private $defaultPageLimits = 5;

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->defaultPageLimits = Mage::getStoreConfig('designcollaboration/general/pagination_limit')
            ? Mage::getStoreConfig('designcollaboration/general/pagination_limit')
            : $this->defaultPageLimits;

        $pager = $this->getLayout()->createBlock('designcollaboration/html_pager', 'custom.pager')
            ->setShowAmounts(true)
            ->setAvailableLimit(array(
                5   => 5,
                10  => 10,
                20  => 20
            ))
            ->addAvailableLimit($this->defaultPageLimits)
            ->setLimit(
                Mage::app()->getRequest()->getParam('limit')
                    ? Mage::app()->getRequest()->getParam('limit')
                    : $this->defaultPageLimits
            )
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);

        $this->getCollection()->load();

        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCollection()
    {
        $curr_page = Mage::app()->getRequest()->getParam('p') ? Mage::app()->getRequest()->getParam('p') : 1;
        $limit = Mage::app()->getRequest()->getParam('limit')
            ? Mage::app()->getRequest()->getParam('limit')
            : $this->defaultPageLimits;

        //Calculate Offset
        $offset = ($curr_page - 1) * $limit;

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $tableSharings = Mage::getSingleton('core/resource')->getTableName('designcollaboration/sharings');

        $collection = Mage::getModel('designcollaboration/designs')->getCollection();

        $collection
            ->getSelect()
            ->joinLeft(array('sharings' => $tableSharings), 'main_table.design_id = sharings.design_id', '')
            ->joinLeft('customer_entity', 'main_table.send_for_approve_by = customer_entity.entity_id', '')
            ->joinLeft('customer_group', 'customer_entity.group_id = customer_group.customer_group_id', '')
            ->joinLeft('core_website', 'customer_group.fcr_store = core_website.website_id', '')
            ->joinLeft('designcollaboration_design_approvees', 'designcollaboration_design_approvees.approver_group_id = customer_entity.group_id', '')
            ->limit($limit, $offset)
            ->distinct(true);

        $collection
            ->addFilter('main_table.created_by', $customerData->getId(), 'OR')
            ->addFilter('sharings.customer_id', $customerData->getId(), 'OR')
            ->addFilter('main_table.approved_by', $customerData->getId(), 'OR')
            ->addFilter('main_table.status_id', 4, 'AND')
            ->addFilter('main_table.status_id', 3, 'OR')
            ->addFilter('core_website.dc_enable_design', 1, 'AND')
            ->addFilter('core_website.dc_require_design_approval', 1, 'AND')
            ->addFilter('core_website.dc_default_design_approval_group', $customerData->getGroupId(), 'AND')
            ->addFilter('main_table.status_id', 3, 'OR')
            ->addFilter('core_website.dc_enable_design', 1, 'AND')
            ->addFilter('designcollaboration_design_approvees.group_id', $customerData->getGroupId(), 'AND')
            ->setOrder('main_table.created_at', 'DESC');

        return $collection;
    }

    public function canRemove($designModel)
    {
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        return Mage::helper('designcollaboration')->hasSharings($designModel['design_id'], $customerData->getId()) || $designModel['created_by'] == $customerData->getId();
    }

    public function showStatusBlockSendForApproval($statusId)
    {
        return $statusId == 1 && Mage::helper('designcollaboration')->getRequireApproval();
    }

    public function showStatusBlockPendingApproval($statusId)
    {
        return $statusId == 3;
    }

    public function showStatusBlockAddToCart($statusId)
    {
        return $statusId == 4 || $statusId == 5 || ($statusId == 1 && !Mage::helper('designcollaboration')->getRequireApproval());
    }

}