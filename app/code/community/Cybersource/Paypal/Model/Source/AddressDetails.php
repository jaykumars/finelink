<?
class Cybersource_Paypal_Model_Source_AddressDetails
{
    const NO = 0;
    const PAYPAL_ADDRESS = 1;
    const MAGENTO_ADDRESS = 2;
    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::NO,
                'label' =>Mage::helper('cybersourcepaypal')->__('No')
            ),
            array(
                'value' => self::PAYPAL_ADDRESS,
                'label' => Mage::helper('cybersourcepaypal')->__('PayPal Address')
            ),
            array(
                'value' => self::MAGENTO_ADDRESS,
                'label' => Mage::helper('cybersourcepaypal')->__('Magento Address')
            ),
        );
    }
}