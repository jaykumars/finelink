<?php

class Onelogin_SAML_Model_Observer extends Mage_Core_Helper_Abstract
{
    public function checkLogin($observer)
    {
        $forceSAML = Mage::getStoreConfig('onelogin_saml/options/force_saml');

        if ($forceSAML) {

            $isLoggedIn = Mage::getSingleton( 'customer/session' )->isLoggedIn();

            $allowedPaths = array(
                '/sso/saml/index',
                '/sso/saml/login',
                '/sso/saml/logout',
                '/sso/saml/acs',
                '/sso/saml/sls',
                '/sso/saml/metadata',
                '/admin/saml/sso/',
                '/customer/account/forgotpassword',
                '/customer/account/resetpassword'
            );

            // call event from observer
            $event = $observer->getEvent();
            // call action from event
            $controller = $event->getAction();

            $currentPath = $controller->getRequest()->getPathInfo();

            $excluded = false;
            foreach ($allowedPaths as $path) {
                if (strpos($currentPath, $path) !== false) {
                    $excluded = true;
                }
            }

            // Redirect to SAML Login
            if (!$isLoggedIn && !$excluded && !Onelogin_SAML_Model_Observer::isAdminPage())
            {
                $controller->getResponse()->setRedirect(Mage::getUrl('sso/saml/login'));
            }
        }
    }

    public function isAdminPage()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }

        return false;
    }

    public function addSSOLink($event)
    {
        /* @var $block Mage_Core_Block_Abstract */
        $block  = $event->getBlock();
        $template = $block->getTemplate();

        $module_enabled = Mage::helper('onelogin_saml')->checkEnabledModule();
        if ($module_enabled && $block instanceof Mage_Customer_Block_Form_Login && strpos($template, 'login.phtml') !== FALSE) {
            $html = $event->getTransport()->getHtml();
            
            $headerText = Mage::getStoreConfig('onelogin_saml/customizations/login_header');
            $linkText = Mage::getStoreConfig('onelogin_saml/customizations/login_link');

            if (empty($headerText)) {
                $headerText = 'External customers';
            }
            if (empty($linkText)) {
                $linkText = 'Login via Identity Provider';
            }

            try {
                $dom = new DOMDocument();
                $dom->loadHTML($html);
                $xml = simplexml_import_dom($dom);

                $container = current($xml->xpath('//form[@id=\'login-form\']//div[@class=\'col-2 registered-users\']//div[@class=\'content\']'));
                if (!$container) {
                    $container = current($xml->xpath('//div//*[contains(@class, "registered-users")]//div[contains(@class, "content")]'));
                }
                if (!$container) {
                    $container = current($xml->xpath('//div//*[contains(@class, "registered-users")]'));
                }
                if ($container) {
                    $dom_container = dom_import_simplexml($container);
                    $dom_container->insertBefore($dom_container->ownerDocument->createElement('br', ''), $dom_container->firstChild);
                    $dom_container->insertBefore($dom_container->ownerDocument->createElement('br', ''), $dom_container->firstChild);
                    $a = $dom_container->insertBefore($dom_container->ownerDocument->createElement('a', Mage::helper('onelogin_saml')->__($linkText)), $dom_container->firstChild);
                    $a->setAttribute('href', Mage::getUrl('sso/saml/login'));
                    $dom_container->insertBefore($dom_container->ownerDocument->createElement('h2', Mage::helper('onelogin_saml')->__($headerText)), $dom_container->firstChild);

                    $html = $xml->saveXML();
                    $event->getTransport()->setHtml($html);
               }
            } catch (Exception $e) {}

        }
    }

    public function initSSOLogout($event)
    {
        $module_enabled = Mage::helper('onelogin_saml')->checkEnabledModule();
        if ($module_enabled) {
            $slo_enabled = Mage::getStoreConfig('onelogin_saml/options/slo');
            if ($slo_enabled) {
                Mage::helper('onelogin_saml')->initSLO();
            }
        }
    }
}
