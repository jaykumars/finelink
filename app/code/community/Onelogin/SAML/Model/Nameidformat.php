<?php
class Onelogin_SAML_Model_Nameidformat
{
    public function toOptionArray()
    {
	$values = array(
                'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
                'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
		        'urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName',
                'urn:oasis:names:tc:SAML:2.0:nameid-format:entity',
                'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
                'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
                'urn:oasis:names:tc:SAML:2.0:nameid-format:encrypted',
                'urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos'
	);

	$options = array();

	foreach ($values as $value) {
		$option = array('value' => $value, 'label' => $value);
		$options[] = $option;
	}

        return $options;
    }
}

