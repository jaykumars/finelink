<?php
$installer = $this;
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_item'), 'press_instructions', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			'nullable' => false,
        	'comment' => 'Press Instructions'
    ));