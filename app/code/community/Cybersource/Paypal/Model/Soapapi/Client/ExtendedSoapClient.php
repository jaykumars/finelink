<?php
class Cybersource_Paypal_Model_Soapapi_Client_ExtendedSoapClient extends SoapClient
{
    public $_storeId = null;

    public function __construct($wsdl, $options = array())
    {
        parent::__construct($wsdl, $options);
    }


    public function __doRequest($request, $location, $action, $version, $oneWay = 0)
    {
        $storeId = Mage::getModel('core/session')->getSoapRequestStoreId();

        if(!$storeId){
            $storeId = $this->_storeId;
        }

        //as you can specify multiple merchant IDs / SOAP Keys for different stores -
        //for admin refunds / auths / captures / orders each store view can have multiple soap keys / merchant IDs for different stores.
        //in order to introduce store scope, we pass the store ID in the abstract class which is taken from the payment that was made on a
        //specific store view.
        $user = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('merchant_id',$storeId);
        $password = Cybersource_Paypal_Model_Source_Consts::getSystemConfig('soapkey',$storeId);
        $soapHeader = "<SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"><wsse:Security SOAP-ENV:mustUnderstand=\"1\"><wsse:UsernameToken><wsse:Username>$user</wsse:Username><wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">$password</wsse:Password></wsse:UsernameToken></wsse:Security></SOAP-ENV:Header>";

        $requestDOM = new DOMDocument('1.0');
        $soapHeaderDOM = new DOMDocument('1.0');
        $requestDOM->loadXML($request);
        $soapHeaderDOM->loadXML($soapHeader);

        $node = $requestDOM->importNode($soapHeaderDOM->firstChild, true);
        $requestDOM->firstChild->insertBefore(
            $node, $requestDOM->firstChild->firstChild);
        $request = $requestDOM->saveXML();
        $response = parent::__doRequest($request, $location, $action, $version, $oneWay);

        Mage::log($request,null,'test_soap.log',true);
        Mage::log($response,null,'test_soap.log',true);

        return $response;
    }
}
