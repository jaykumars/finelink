<?php
class Rocketbuild_Myapprovals_Block_Billing extends Mage_Checkout_Block_Onepage_Billing {

    protected function _construct() {
        parent::_construct();
        $storeId = Mage::app()->getStore()->getId();
        $quote = Mage::getModel('checkout/cart')->getQuote();
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $canMakePurchase = Mage::helper('myapprovals')->customerCanMakePurchase($customer->getId(), $quote->getId());
        
        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $group = Mage::getModel('customer/group')->load($groupId);

        $canOverrideBilling = $group->getFcrCanOverrideBilling();

        if ($storeId != 12) {
            if (!$canOverrideBilling && !$canMakePurchase) {
                Mage::app()->getLayout()->getBlock('head')->addJs('fineline/checkout.js');
                $this->getCheckout()->setStepData('billing', array(
                    'label'     => Mage::helper('checkout')->__('Billing Information <span style="color:#777; font-size:14px">- preset</span>'), // your custom label here
                    'is_show'   => $this->isShow()
                ));
            }
        } 
    }

    public function customerHasAddresses() {
        $hasAddresses = parent::customerHasAddresses();

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $quote = Mage::getModel('checkout/cart')->getQuote();

        $canMakePurchase = Mage::helper('myapprovals')->customerCanMakePurchase($customer->getId(), $quote->getId());
        $canOverrideBilling = $customer->getFcrCanOverrideBilling();

        if (!$canOverrideBilling && !$canMakePurchase) {
            return 0;
        }

        return $hasAddresses;
    }

    public function getAddress() {
        $address = parent::getAddress();

        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $group = Mage::getModel('customer/group')->load($groupId);

        $firstname = $group->getFcrBillingFirstName();
        if (!empty($firstname)){
            $address->setFirstname($firstname);
        }
        $middlename = $group->getFcrBillingMiddleName();
        if (!empty($middlename)){
            $address->setMiddlename($middlename);
        }
        $lastname = $group->getFcrBillingLastName();
        if (!empty($lastname)){
            $address->setLastname($lastname);
        }
        $company = $group->getFcrBillingCompany();
        if (!empty($company)){
            $address->setCompany($company);
        }
        $street = $group->getFcrBillingStreet();
        if (!empty($street)){
            $address->setStreet($street);
        }
        $city = $group->getFcrBillingCity();
        if (!empty($city)){
            $address->setCity($city);
        }
        $region = $group->getFcrBillingRegion();
        if (!empty($region)){
            $address->setRegion($region);
        }
        $regionid = $group->getFcrBillingRegionId();
        if (!empty($regionid)){
            $address->setRegionId($regionid);
        }
        $postcode = $group->getFcrBillingPostcode();
        if (!empty($postcode)){
            $address->setPostcode($postcode);
        }
        $country = $group->getFcrBillingCountryId();
        if (!empty($country)){
            $address->setCountryId($country);
        }
        $telephone = $group->getFcrBillingTelephone();
        if (!empty($telephone)){
            $address->setTelephone($telephone);
        }
        $fax = $group->getFcrBillingFax();
        if (!empty($fax)){
            $address->setFax($fax);
        }

        return $address;
    }

    public function isUseBillingAddressForShipping() {
        $canOverrideBilling = Mage::getSingleton('customer/session')->getCustomer()->getFcrCanOverrideBilling();
        return $canOverrideBilling ? parent::isUseBillingAddressForShipping() : false;
    }
}
