<?php
class Rocketbuild_Myapprovals_Model_Observer extends Varien_Event_Observer
{


    /**
     * Runs when displaying the 'Edit' or 'New' form for Customer Groups.
     * @param  Varien_Event_Observer $observer  Magento observer object.
     * @return
     */
    public function onBlockLayoutAfter(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'customer/account_navigation') {

            $currentCustomer = Mage::getSingleton('customer/session')->getCustomer();
            $groupId = $currentCustomer->getGroupId();
            $customerGroup = $group = Mage::getModel('customer/group')->load($groupId);
            $groupType = $customerGroup->getFcrRoletype();

            if ($groupType > 0) {
                $block->addLink('myapprovals', 'myapprovals/index/', 'My Approvals');
            }

            if ($currentCustomer->getFcrCanViewReports()) {
                $block->addLink('reports', 'reports/index/', 'Reports');
            }
        }
    }
 public function salesQuoteItemSetProduct(Varien_Event_Observer $observer)
    {
        /* @var $item Mage_Sales_Model_Quote_Item */
        $item = $observer->getQuoteItem();
        $origName = $item->getName();

        $docidObj = $item->getOptionByCode('chili_document_id');
        if ($docidObj !== NULL){
            $itemId = $docidObj->getValue();
            $variableValues = Mage::getModel('web2print/api')->getDocumentVariableValues($itemId);
            $result = $variableValues;
            $xml = new DOMDocument();
                $xml->loadXML($result);
                $items = $xml->getElementsByTagName('item');
                 foreach ($items as $item) {
                    $name = $item->getAttribute('name');
                    $value = $item->getAttribute('value');
                    // change name below to SKU when ready
                    if ($name == 'sku') {
                        //echo $value;
                    }
                }
                $item = $observer->getQuoteItem();
                $item->setName($origName . "-" . $value);
               return $this; 
            } else {
                $item->setName($origName);
                return $this;
            }
    }
}
