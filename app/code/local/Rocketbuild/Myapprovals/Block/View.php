<?php

class Rocketbuild_Myapprovals_Block_View extends Mage_Core_Block_Template {
    protected $_links = array();

    protected function _construct() {
        parent::_construct();
        $this->setTemplate( 'myapprovals/view.phtml' );
    }

    protected function _prepareLayout() {
        if ( $headBlock = $this->getLayout()->getBlock( 'head' ) ) {
            $headBlock->setTitle( $this->__( 'Requisition # %s', $this->getOrder()->getRealOrderId() ) );
        }
        $this->setChild(
            'payment_info',
            $this->helper( 'payment' )->getInfoBlock( $this->getOrder()->getPayment() )
        );
    }

    public function getPaymentInfoHtml() {
        return $this->getChildHtml( 'payment_info' );
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder() {
        return Mage::getModel('sales/quote')->load($this->getRequest()->getParam('order_id'));
    }

    public function addLink( $name, $path, $label ) {
        $this->_links[ $name ] = new Varien_Object( array(
            'name'  => $name,
            'label' => $label,
            'url'   => empty( $path ) ? '' : Mage::getUrl( $path, array( 'order_id' => $this->getOrder()->getId() ) )
        ) );

        return $this;
    }

    public function getLinks() {
        $this->checkLinks();

        return $this->_links;
    }

    private function checkLinks() {
        $order = $this->getOrder();
        if ( ! $order->hasInvoices() ) {
            unset( $this->_links['invoice'] );
        }
        if ( ! $order->hasShipments() ) {
            unset( $this->_links['shipment'] );
        }
        if ( ! $order->hasCreditmemos() ) {
            unset( $this->_links['creditmemo'] );
        }
    }

    /**
     * Get url for reorder action
     *
     * @deprecated after 1.6.0.0, logic moved to new block
     *
     * @param Mage_Sales_Order $order
     *
     * @return string
     */
    public function getReorderUrl( $order ) {
        if ( ! Mage::getSingleton( 'customer/session' )->isLoggedIn() ) {
            return $this->getUrl( 'sales/guest/reorder', array( 'order_id' => $order->getId() ) );
        }

        return $this->getUrl( 'sales/order/reorder', array( 'order_id' => $order->getId() ) );
    }

    /**
     * Get url for printing order
     *
     * @deprecated after 1.6.0.0, logic moved to new block
     *
     * @param Mage_Sales_Order $order
     *
     * @return string
     */
    public function getPrintUrl( $order ) {
        if ( ! Mage::getSingleton( 'customer/session' )->isLoggedIn() ) {
            return $this->getUrl( 'sales/guest/print', array( 'order_id' => $order->getId() ) );
        }

        return $this->getUrl( 'sales/order/print', array( 'order_id' => $order->getId() ) );
    }

    public function getPreviewImage($item)
    {
        $product = $item->getProductId();
        $documentId = $item->getOptionByCode('chili_document_id');
        $nonChiliPreviewImage = $this->helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(50,50);
        //$chiliPreviewImage = $this->helper('catalog/image')->_getChiliImageReqUrl($documentId);

        if ($documentId === null) {
            $nonChiliPreviewImage = $this->helper('catalog/image')->init($item->getProduct(), 'small_image')->resize(50,50);
            return $nonChiliPreviewImage;
        } else {
            $docidObj = $item->getOptionByCode('chili_document_id');
            $docid = null;
                if ($docidObj != null) {
                $docid = $docidObj->getValue();
            }

             $controller = 'cart';
             $ajaxUrl = Mage::getUrl('web2print/ajax/image/');
             $url = Mage::helper('core/url')->addRequestParam($ajaxUrl,
                    array(
                        'controller' => 'cart',
                        'documentId' => $docid,
                        'redirect' => true,
                        )
                    );
            return $url;
        }
        
    }

    // Get sku name for variable items
    public function getVariableSku($item)
    {
        $name = $item->getName();
        echo $name;
        // $docidObj = $item->getOptionByCode('chili_document_id');
        // if ($docidObj !== NULL){
        // $itemId = $docidObj->getValue();
        // $variableValues = Mage::getModel('web2print/api')->getDocumentVariableValues($itemId);
        // $result = $variableValues;
        // $xml = new DOMDocument();
        //         $xml->loadXML($result);
        //         $items = $xml->getElementsByTagName('item');
        //          foreach ($items as $item) {
        //             $name = $item->getAttribute('name');
        //             $value = $item->getAttribute('value');
        //             // change name below to SKU when ready
        //             if ($name == 'sku') {
        //                 echo $value;
        //             }
        //          }
        //      }
        // else {
        //     $staticSku = $item->getSku();
        //     echo $staticSku;
        // }
    }

    public function getConfigureUrl($item)
    {
        // Default behavior
        $url = 'checkout/cart/configure';
        // $item = $this->getOrder()->getItem($itemId);
        $params = array('id' => $item->getId());


        $docidObj = $item->getOptionByCode('chili_document_id');
        $docid = null;
        if ($docidObj != null) {
            $docid = $docidObj->getValue();
        }

        // Change configure URL in case of a CHILI enabled product
        if ($docid != null) {
            $url = 'web2print/editor/load';
            $params['type'] = 'quoteitem';
        }else{
            $params['quoteid'] = $this->getOrder()->getId();
        }

        return $this->getUrl($url, $params);
    }

    public function getShippingMethodSelectOptions() {

        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $shipping = array();
        foreach($methods as $_ccode => $_carrier) {
            if($_methods = $_carrier->getAllowedMethods())  {
                if(!$_title = Mage::getStoreConfig("carriers/$_ccode/title"))
                    $_title = $_ccode;
                foreach($_methods as $_mcode => $_method)   {
                    $_code = $_ccode . '_' . $_mcode;
                    $shipping[$_code]=array('title' => $_method,'carrier' => $_title);
                }
            }
        }

        return $shipping;
    }

}