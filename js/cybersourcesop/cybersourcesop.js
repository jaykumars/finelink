/**
* © 2016 CyberSource Corporation. All rights reserved. CyberSource Corporation (including its subsidiaries,
* “CyberSource”) furnishes this code under the applicable agreement between the reader of this document
* (“You”) and CyberSource (“Agreement”). You may use this code only in accordance with the terms of the
* Agreement. The copyrighted code is licensed to You for use only in strict accordance with the Agreement.
* You should read the Agreement carefully before using the code.
*/

//remove users token choice on CC.phtml when they are adding a new card.
function removeTokenChoice(args) {
    //clear radio button choice
    $$('input[id="token"]').forEach(function(element, value) {
        element.checked = false;
    });
    //remove CVNs as well
    removeCVN(999);
    if (args == 1) {
        //enable CVN of New Card.
        $$('input[new="1"]')[0].enable();
    }
}

function removeChoice() {
    $$('input[id="token"]').forEach(function(element, value) {
        element.checked = false;
    });
    var tokenSize = document.getElementsByName('payment[payment_token]').length+1;
    $$('input[id="cybersourcesop_cc_cid"]').forEach(function(element, value) {
        if (value != tokenSize) {
            element.value = "";
        }
    } );
}

//function to remove values from all CVN fiels on cc.phtml when radio buttons are clicked to select token'd cards
//it also disables the other CVN input elements
function removeCVN(numberToKeep) {

    //tokenSize represents the last CVN input element used in the original CC Form
    var tokenSize = document.getElementsByName('payment[payment_token]').length+1;
    $$('input[id="cybersourcesop_cc_cid"]').forEach(function(element, value) {
        if (value != numberToKeep && value != tokenSize) {
            element.value = "";
            element.disable();
        } else {
            element.enable();
        }
    } );
}

//validation classes for Tokenised Radio buttons and CVNs for Customer Saved Cards(tokens)
Validation.addAllThese([
    //validate Radio token
    ['validate-cyber', 'Please specify a card or add a new card.', function(v, elm) {
        //check if OneStepCheckout is being used.
        var oneStepCheckout = $$('button[id="onestepcheckout-place-order"]')[0];
        if (oneStepCheckout) {
            return isTokensSelected(1, elm);
        } else {
            return isTokensSelected(0, elm);
        }
    }],
    //validate CVN input
    ['validate-cybercvn', 'Please fill in a CVN Number to continue.', function(v, elm) {

        return isCVNPopulated($(elm).readAttribute('counter'));
    }]]);

//This function checks if CVNs are populated with a value or not.
function isCVNPopulated(index){
    var checkedTokens = $$('input#token.validate-cyber:checked');
    if (typeof checkedTokens != 'undefined' && checkedTokens.length > 0){
        if (index == checkedTokens[0].readAttribute('index')){
            var cardCVN = $$('input[counter="'+index+'"]');
            if (!cardCVN[0].value){
                return false;
            }
        }
    }
    return true;
}

//checks if a token is selected
function isTokensSelected(onestepcheckout, elm){
    var validateCyber = false;
    var oneStepCheckout = false;
    //check if customer has tokens
    var tokens = document.getElementsByName('payment[payment_token]');
    //check if cybersource has been selected
    if (onestepcheckout != 1) {
        var cyberRadioBtn = document.getElementById('p_method_cybersourcesop');
        if  (cyberRadioBtn.checked) {
            validateCyber = true;
        }
    } else {
        validateCyber = true;
        oneStepCheckout = true;
    }
    var state = false;
    //state is 0 because nothing is selected.
    var hasSpecifiedCard = 0;
    if (validateCyber) {
        //remove AJAX Loader
        if (!oneStepCheckout) {
            //for base magento loader when continue is clicked.
            $('payment-please-wait').setStyle({'display':'none'});
        } else {
            state = true;
        }
        //check if user is adding new card
        var newCard = document.getElementById('new_token'); //hidden form input element
        var formCVN = '';
        if (typeof tokens != 'undefined' && tokens && newCard.value == "") {
            var checkedTokens = $$('input#token.validate-cyber:checked');
            if (typeof checkedTokens != 'undefined' && checkedTokens.length > 0){
                return true;
            }else{
                if ($(elm).readAttribute('index') == "0"){
                    return false;
                }
                return true;
            }
        }else {
            //Tokens are hidden - proceed to standard form validation.
            if (oneStepCheckout) {
                return true;
            } else {
                payment.save();
            }
        }
    } else {
        //skip function if this is not cybersourcesop
        if (oneStepCheckout) {
            return true;
        } else {
            payment.save();
        }
    }
}

//switches tokens on / off as well as new card details.
function switchNewCard() {
    //makes the card details appear/dissappear to add a new card
    if ($('new_token').value != 1) {
        $('cybersourcesop_cc_type_select_div').setStyle({ 'display': 'block' });
        $('cybersourcesop_cc_type_cc_number_div').setStyle({ 'display': 'block' });
        $('cybersourcesop_cc_type_exp_div').setStyle({ 'display': 'block' });
        $('use_saved_cc').setStyle({ 'display': 'block' });
        //deselect any radio buttons
        //clear validation-passed classes for tokens
        $$('input#token.validate-cyber').className = "validate-cyber";
        //clear validation-passed classes for cvns
        $$('input#cybersourcesop_cc_cid.validate-cvn').className = "validate-cybercvn";
        //hide Tokens
        removeTokenChoice();
        if ($('cybersourcesop_cc_type_cvv_div')) {$('cybersourcesop_cc_type_cvv_div').setStyle({ 'display': 'block' }); }
        $('cybersourcesop_cc_save_div').setStyle({ 'display': 'block' });
        $$('.tokenList')[0].setStyle({ 'display': 'none'});
        $('new_token').value = 1;
        //enable the CVN
        $$('input[new="1"]')[0].enable();
        return true;
    } else {
        //hide the card details. so user can select token rather.
        $('cybersourcesop_cc_type_select_div').setStyle({ 'display': 'none' });
        $('cybersourcesop_cc_type_cc_number_div').setStyle({ 'display': 'none' });
        $('cybersourcesop_cc_type_exp_div').setStyle({ 'display': 'none' });
        if ($('cybersourcesop_cc_type_cvv_div')) {$('cybersourcesop_cc_type_cvv_div').setStyle({ 'display': 'none' }); }
        $('cybersourcesop_cc_save_div').setStyle({ 'display': 'none' });
        $('use_saved_cc').setStyle({ 'display': 'none' });
        //clear validation-passed classes for tokens
        $$('input#token.validate-cyber').className = "validate-cyber";
        //clear validation-passed classes for cvns
        $$('input#cybersourcesop_cc_cid.validate-cvn').className = "validate-cybercvn";
        //deselect any radio buttons
        removeTokenChoice();
        //show tokens
        $$('.tokenList')[0].setStyle({ 'display': 'block'});
        $('new_token').value = "";
        return false;
    }
}

function showTokens() {
    if (this.value == 'token') {
        $('tokenList').style(this,"display","block");
    }
}
