<?php

class Onelogin_SAML_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Get config data by key
     *
     * @param string $key
     * @return mixed
     */
    public function getConfig($key = '')
    {
        return Mage::getStoreConfig($key);
    }

    /**
     * Get if module is enabled
     *
     * @return bool
     */
    public function checkEnabledModule()
    {
        return Mage::getStoreConfig('onelogin_saml/status/enabled');
    }

    /**
     * Process SAML Response
     *
     */
    public function processSSO()
    {
        if (Mage::helper('onelogin_saml')->checkEnabledModule()) {
            $auth = Mage::helper('onelogin_saml')->getAuth();
            $auth->processResponse();
            $errors = $auth->getErrors();
            if (!empty($errors)) {
                $error_msg = "Error at the ACS Endpoint.<br>" . implode(', ', $errors);
                if (Mage::getStoreConfig('onelogin_saml/advanced/debug')) {
                    $reason = $auth->getLastErrorReason();
                    if (isset($reason) && !empty($reason)) {
                        $error_msg .= '<br><br>Reason: ' . $reason;
                    }
                }
                $this->_processError($error_msg);
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
            } else if (!$auth->isAuthenticated()) {
                $this->_processError("ACS Process failed");
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
            } else {
                $userData = $this->_processAttrs($auth);

                $customerCollection = Mage::getModel('customer/customer')->getCollection();
                $customerCollection->addFieldToFilter('store_id', Mage::app()->getStore()->getId()); // Multisite support

                $obtainUserByCustomAttribute = Mage::getStoreConfig('onelogin_saml/attr_mapping/use_custom_to_identity_user');
                if ($obtainUserByCustomAttribute && isset($userData['custom'])) {
                    // Custom Attribute
                    $customAttributeCode = Mage::getStoreConfig('onelogin_saml/attr_mapping/custom_attribute_code');

                    $customer = $customerCollection->addAttributeToFilter($customAttributeCode, $userData['custom'][$customAttributeCode])
                        ->getFirstItem();
                } else {
                    if (empty($userData['email'])) {
                        $this->_processError("Onelogin SAML plugin can't obtain the email value from the SAML Response. Review the data sent by your IdP and the Attribute Mapping setting options" . $userData['email']);
                    }

                    // Try obtain the customer by Mail
                    $customer = $customerCollection->addAttributeToFilter('email', $userData['email'])
                        ->getFirstItem();
                }

                $relayState = Mage::app()->getFrontController()->getRequest()->getPost('RelayState');
                if (!empty($relayState) && strpos($relayState, '/sso/saml/login') === FALSE) {
                    $urlToGo = $relayState;
                } else {
                    $targetAfterLogin = Mage::getStoreConfig('onelogin_saml/options/target_after_login');
                    if (empty($targetAfterLogin)) {
                        $urlToGo = Mage::getBaseUrl();
                    } else {
                        if (strpos($targetAfterLogin, 'http://') === FALSE && strpos($targetAfterLogin, 'https://') === FALSE) {
                            $urlToGo = Mage::getUrl($targetAfterLogin);
                        } else {
                            $urlToGo = $targetAfterLogin;
                        }
                    }
                }

                if ($customer->getId()) {
                    // Customer exists
                    $updateuser = Mage::getStoreConfig('onelogin_saml/options/updateuser');
                    if ($updateuser) {
                        if (!empty($userData['firstname'])) {
                            $customer->setFirstname($userData['firstname']);
                        }
                        if (!empty($userData['lastname'])) {
                            $customer->setLastname($userData['lastname']);
                        }
                        if (!empty($userData['groupid'])) {
                            $customer->setGroupId($userData['groupid']);
                        }
                             if (isset($userData['custom']) && !empty($userData['custom'])) {
                                $customAttributeCode = Mage::getStoreConfig('onelogin_saml/attr_mapping/custom_attribute_code');

                                $customAttributeValue = $userData['custom'][$customAttributeCode];
                                try {
                                    $customer->setData($customAttributeCode, $customAttributeValue);
    //                                $customer->save();
                                } catch (Exception $e) {
                                }
                            }
                        if ($obtainUserByCustomAttribute) {
                            if (!empty($userData['email'])) {
                                $customer->setEmail($userData['email']);
                            }
                        }

                        $customer->save();
                    }
                } else {
                    // Customer doesn't exist
                    $autocreate = Mage::getStoreConfig('onelogin_saml/options/autocreate');
                    if ($autocreate) {
                        // I filter by store, but the customer data is linked to a website, so before try create the user, I should search by website

                        $customerCollection2 = Mage::getModel('customer/customer')->getCollection();
                        $customerCollection2->addFieldToFilter('website_id', Mage::app()->getStore()->getWebsiteId());

                        if ($obtainUserByCustomAttribute) {
                            // Custom Attribute
                            $customer = $customerCollection2->addAttributeToFilter($customAttributeCode, $userData['custom'][$customAttributeCode])
                                ->getFirstItem();
                        } else {
                            // Try obtain the customer by Mail
                            $customer = $customerCollection2->addAttributeToFilter('email', $userData['email'])
                                ->getFirstItem();
                        }

                        if ($customer->getId()) {
                            $urlToGo = Mage::getUrl('/');
                        } else {
                            $redirectOnJIT = Mage::getStoreConfig('onelogin_saml/options/redirect_on_jit');
                            try {
                                $customer->setEmail($userData['email']);
                                $customer->setFirstname($userData['firstname']);
                                $customer->setLastname($userData['lastname']);
                                $newPassword = $customer->generatePassword();
                                $customer->setPassword($newPassword);
                                if (empty($userData['groupid'])) {
                                    $userData['groupid'] = Mage::getStoreConfig('customer/create_account/default_group');
                                }
                                $customer->setGroupId($userData['groupid']);
                                $customer->save();
                                $customer->setConfirmation(null);
                                $customer->save();
                                //$customer->sendPasswordReminderEmail();

                                //Custom attribute
                                if (isset($userData['custom']) && !empty($userData['custom'])) {
                                    $customAttributeCode = Mage::getStoreConfig('onelogin_saml/attr_mapping/custom_attribute_code');

                                    $customAttributeValue = $userData['custom'][$customAttributeCode];
                                    try {
                                        $customer->setData($customAttributeCode, $customAttributeValue);
                                        $customer->save();
                                    } catch (Exception $e) {
                                    }
                                }

                                if (!empty($userData['address'])) {
                                    $customAddress = Mage::getModel('customer/address');
                                    $userData['address']['firstname'] = $userData['firstname'];
                                    $userData['address']['lastname'] = $userData['lastname'];
                                    $customAddress->setData($userData['address'])
                                        ->setCustomerId($customer->getId())
                                        ->setIsDefaultBilling('1')
                                        ->setIsDefaultShipping('1')
                                        ->setSaveInAddressBook('1');
                                    $customAddress->save();
                                    if ($redirectOnJIT) {
                                        $urlToGo = Mage::getUrl('customer/address/edit/id/' . $customAddress->getId());
                                    }
                                } else {
                                    if ($redirectOnJIT) {
                                        $urlToGo = Mage::getUrl('customer/address/new');
                                    }
                                }
                            } catch (Exception $e) {
                                $this->_processError('The auto-provisioning process failed: ' . $e->getMessage());
                            }
                        }
                    } else {
                        $this->_processError('The login could not be completed, user ' . $userData['email'] . ' does not exist in Magento and the auto-provisioning function is disabled');
                    }
                }
                $customerSession = Mage::getSingleton('customer/session');
                // Login and check the customer by his uid
                $customerSession->loginById($customer->getId());
                $customerSession->setData('saml_login', true);
                $customerSession->setData('saml_nameid', $auth->getNameId());
                $customerSession->setData('saml_nameid_format', $auth->getNameIdFormat());
                $customerSession->setData('saml_sessionindex', $auth->getSessionIndex());

                // Redirect
                Mage::app()->getFrontController()->getResponse()->setRedirect($urlToGo)->sendResponse();
                // If there is a duplicate header problem, use instead
                // header ('HTTP/1.1 301 Moved Permanently');
                // header ('Location: ' . $urlToGo);
                // exit;
                exit;
            }

        } else {
            $this->_processError('Onelogin SAML module has disabled status');
        }
    }

    public function _processError($errorMsg)
    {
        Mage::getSingleton('core/session')->addError($errorMsg);
        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
        exit();
    }

    /**
     * Process SAML Response
     *
     */
    public function _processAttrs($auth)
    {
        $userData = array(
            'username' => '',
            'email' => '',
            'firstname' => '',
            'lastname' => '',
            'groupid' => '',
        );

        $attrs = $auth->getAttributes();

        if (empty($attrs)) {
            $userData['email'] = $auth->getNameId();
        } else {
            $mailMapping = Mage::getStoreConfig('onelogin_saml/attr_mapping/email');
            $firstnameMapping = Mage::getStoreConfig('onelogin_saml/attr_mapping/firstname');
            $lastnameMapping = Mage::getStoreConfig('onelogin_saml/attr_mapping/lastname');
            $groupMapping = Mage::getStoreConfig('onelogin_saml/attr_mapping/group');

            if (!empty($mailMapping) && isset($attrs[$mailMapping]) && !empty($attrs[$mailMapping][0])) {
                $userData['email'] = $attrs[$mailMapping][0];
            }
            if (!empty($firstnameMapping) && isset($attrs[$firstnameMapping]) && !empty($attrs[$firstnameMapping][0])) {
                $userData['firstname'] = $attrs[$firstnameMapping][0];
            }
            if (!empty($lastnameMapping) && isset($attrs[$lastnameMapping]) && !empty($attrs[$lastnameMapping][0])) {
                $userData['lastname'] = $attrs[$lastnameMapping][0];
            }
            if (!empty($groupMapping) && isset($attrs[$groupMapping]) && !empty($attrs[$groupMapping][0])) {
                $saml_groups = $attrs[$groupMapping];
                if (!empty($saml_groups)) {
                    for ($i = 1; $i < 51; $i++) {
                        $groupValues[$i] = explode(',', Mage::getStoreConfig('onelogin_saml/group_mapping/group' . $i));
                    }

                    foreach ($saml_groups as $saml_group) {
                        for ($i = 1; $i < 51; $i++) {
                            if (in_array($saml_group, $groupValues[$i])) {
                                $userData['groupid'] = $i;
                                break 2;
                            }
                        }
                    }
                }
            }

            // Address data
            $addressMapping = array();
            $addressMapping['company'] = Mage::getStoreConfig('onelogin_saml/address_mapping/company');
            $addressMapping['street1'] = Mage::getStoreConfig('onelogin_saml/address_mapping/street1');
            $addressMapping['street2'] = Mage::getStoreConfig('onelogin_saml/address_mapping/street2');
            $addressMapping['city'] = Mage::getStoreConfig('onelogin_saml/address_mapping/city');
            $addressMapping['country_id'] = Mage::getStoreConfig('onelogin_saml/address_mapping/country');
            $addressMapping['region_id'] = Mage::getStoreConfig('onelogin_saml/address_mapping/state');
            $addressMapping['postcode'] = Mage::getStoreConfig('onelogin_saml/address_mapping/zip');
            $addressMapping['telephone'] = Mage::getStoreConfig('onelogin_saml/address_mapping/telephone');
            $addressMapping['fax'] = Mage::getStoreConfig('onelogin_saml/address_mapping/fax');

            $userData['address'] = array();
            foreach ($addressMapping as $key => $map) {
                if (!empty($map) && isset($attrs[$map]) && !empty($attrs[$map][0])) {
                    if ($key == 'street1') {
                        $userData['address']['street'][0] = $attrs[$map][0];
                    } else if ($key == 'street2') {
                        $userData['address']['street'][1] = $attrs[$map][0];
                    } else {
                        $userData['address'][$key] = $attrs[$map][0];
                    }
                }
            }

            // Custom Attribute
            $customAttributeCode = Mage::getStoreConfig('onelogin_saml/attr_mapping/custom_attribute_code');
            $customAttributeMapping = Mage::getStoreConfig('onelogin_saml/attr_mapping/custom_attribute_mapping');

            if (!empty($customAttributeMapping) && isset($attrs[$customAttributeMapping]) && !empty($attrs[$customAttributeMapping][0])) {
                $userData['custom'][$customAttributeCode] = $attrs[$customAttributeMapping][0];
            }
        }
        return $userData;
    }

    public function initSLO()
    {
        $customerSession = Mage::getSingleton('customer/session');
        if ($customerSession->isLoggedIn()) {
            $saml_login = $customerSession->getData('saml_login');
            if ($saml_login) {
                $auth = $this->getAuth();
                $samlSessionindex = $customerSession->getData('saml_sessionindex');
                $samlNameId = $customerSession->getData('saml_nameid');
                $samlNameIdFormat = $customerSession->getData('saml_nameid_format');
                $auth->logout(Mage::getBaseUrl(), array(), $samlNameId, $samlSessionindex, false, $samlNameIdFormat);
            }
        }
    }

    /**
     * Get saml auth
     *
     * @return OneLogin_SAML_Auth
     */
    public function getAuth($settings = null)
    {
        try {
            if ($settings == null) {
                $settings = $this->getSettings();
            }
            $auth = new OneLogin_Saml2_Auth($settings);
            return $auth;
        } catch (Exception $e) {
            $this->_processError('There is a problem with the SAML settings.' . $e->getMessage() . '<br>Review System > Configuration > Services > SAML.');
        }
    }

    /**
     * Get Settings
     *
     * @return Array
     */
    public function getSettings()
    {
        try {
            require(dirname(dirname(__FILE__)) . '/settings.php');
            return $settings;
        } catch (Exception $e) {
            $this->_processError('There is a problem with the SAML settings.' . $e->getMessage() . '<br>Review System > Configuration > Services > SAML.');
        }
    }

    /**
     * Process SLO
     *
     */
    public function processSLO()
    {
        if (Mage::helper('onelogin_saml')->checkEnabledModule()) {
            $auth = Mage::helper('onelogin_saml')->getAuth();
            $auth->processSLO();
            $errors = $auth->getErrors();
            if (empty($errors)) {
                // local logout
                $customerSession = Mage::getSingleton('customer/session');
                $customerSession->unsetData('saml_login');
                $customerSession->logout();
                Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
                // If there is a duplicate header problem, use instead
                // header ('HTTP/1.1 301 Moved Permanently');
                // header ('Location: ' . Mage::getBaseUrl());
                // exit;
            } else {
                $error_msg = 'Error at the SLS Endpoint.<br>' . implode(', ', $errors);
                if (Mage::getStoreConfig('onelogin_saml/advanced/debug')) {
                    $reason = $auth->getLastErrorReason();
                    if (isset($reason) && !empty($reason)) {
                        $error_msg .= '<br><br>Reason: ' . $reason;
                    }
                }
                $this->_processError($error_msg);
            }
        } else {
            $this->_processError('Onelogin SAML module has disabled status');
        }
    }

    public function printSPMetadata()
    {
        try {
            $settingsInfo = $this->getSettings();
            $settings = new OneLogin_Saml2_Settings($settingsInfo, true);
            $metadata = $settings->getSPMetadata();
            $errors = $settings->validateMetadata($metadata);
            if (empty($errors)) {
                header('Content-Type: text/xml');
                header('SAML-Magento: 1.1.4');
                echo $metadata;
                exit();
            } else {
                throw new OneLogin_Saml2_Error(
                    'Invalid SP metadata: ' . implode(', ', $errors),
                    OneLogin_Saml2_Error::METADATA_SP_INVALID
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}