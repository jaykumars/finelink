(function($){

    function updateFieldsToRoleType() {
        var fieldset = $('.roletype').closest('.fieldset'),
        approverselect = $('#fcr_approver'),
        approverval = approverselect.val(),
        approveechecks = $('.requisitioner-hide .checkboxes li'),
        roletype = $('.roletype').val(),
        currentStore = $('#fcr_store').val();

        approveechecksvals = [];
        approveechecks.each(function(){
            var input = $(this).find('input');
            if (input.prop('checked')) {
                approveechecksvals.push(input.val());
            }
        });

        roletype = 'undefined' == typeof roletype ? '0' : roletype;

        if (typeof approvees == 'undefined' || typeof approvers == 'undefined') {
            return;
        }

        // Update approver field based on currently selected store and roletype
        var newapprovers = [];
        for (var i = 0; i < approvers.length; i++) {
            var approver = approvers[i];
            approle = parseInt(approver.roletype,10);
            currole = parseInt(roletype, 10);
            if ((approver.store == currentStore || approver.store == '0' || currentStore == '0') && approle > currole) {
                newapprovers.push(approver);
            }
        }

        approverselect.html('');
        for (var j = 0; j < newapprovers.length; j++) {
            var approver = newapprovers[j];
            var options = {
                value: approver.value,
                html: approver.label
            }
            if (approver.value == approverval) {
                options.attr = {'selected':true};
            }
            approverselect.append($('<option>', options));
        }

        // Update approvee fields based on currently selected store and roletype
        var newapprovees = [];
        for (var k = 0; k < approvees.length; k++) {
            var approvee = approvees[k];
            approle = parseInt(approvee.roletype,10);
            currole = parseInt(roletype, 10);
            if ((approvee.store == currentStore || approvee.store == '0' || currentStore == '0') && approle < currole) {
                newapprovees.push(approvee);
            }
        }

        var checkboxul = $('.requisitioner-hide .checkboxes');
        checkboxul.html('');
        for (var l = 0; l < newapprovees.length; l++) {
            var approvee = newapprovees[l];
            console.log(approvee.value, approveechecksvals);
            var checkedstring = $.inArray(approvee.value.toString(), approveechecksvals) < 0 ? '' : ' checked=checked';
            console.log(checkedstring);
            checkboxul.append('<li><input id="fcr_approvees_'+(l+1)+'" type="checkbox" name="fcr_approvees[]" value="'+approvee.value+'"'+checkedstring+'/><label for="fcr_approvees_'+(l+1)+'">'+approvee.label+'</label></li>');
        }


        switch(roletype){
            case '0':
                fieldset.addClass('requisitioner').removeClass('purchaser');
                break;
            case '1':
                fieldset.removeClass('purchaser').removeClass('requisitioner');
                break;
            case '2':
                fieldset.addClass('purchaser').removeClass('requisitioner');
                break;
        }
    }

    function updateFieldDisplayByStyle(field) {
        var display = field.css('display');
        if (display == 'none') {
            field.closest('tr').hide();
        }else{
            field.closest('tr').show();
        }
    }

    function updateSameAsBillingFields() {
        var checked = $('#fcr_same_as_billing').prop('checked');
        if (checked) {
            $('#fcr_shipping_first_name').val( $('#fcr_billing_first_name').val() );
            $('#fcr_shipping_middle_name').val( $('#fcr_billing_middle_name').val() );
            $('#fcr_shipping_last_name').val( $('#fcr_billing_last_name').val() );
            $('#fcr_shipping_company').val( $('#fcr_billing_company').val() );
            $('#fcr_shipping_street').val( $('#fcr_billing_street').val() );
            $('#fcr_shipping_city').val( $('#fcr_billing_city').val() );
            $('#fcr_shipping_region').val( $('#fcr_billing_region').val() );
            $('#fcr_shipping_region_id').val( $('#fcr_billing_region_id').val() );
            $('#fcr_shipping_postcode').val( $('#fcr_billing_postcode').val() );
            $('#fcr_shipping_country_id').val( $('#fcr_billing_country_id').val() );
            $('#fcr_shipping_telephone').val( $('#fcr_billing_telephone').val() );
            $('#fcr_shipping_fax').val( $('#fcr_billing_fax').val() );

            shippingRegionUpdater.update();

        } else {
            $('#fcr_shipping_first_name').val( '' );
            $('#fcr_shipping_middle_name').val( '' );
            $('#fcr_shipping_last_name').val( '' );
            $('#fcr_shipping_company').val( '' );
            $('#fcr_shipping_street').val( '' );
            $('#fcr_shipping_city').val( '' );
            $('#fcr_shipping_region').val( '' );
            $('#fcr_shipping_region_id').val( '' );
            $('#fcr_shipping_postcode').val( '' );
            $('#fcr_shipping_country_id').val( 'US' );
            $('#fcr_shipping_telephone').val( '' );
            $('#fcr_shipping_fax').val( '' );
        }
    }

    $(function(){

        // Role type and approver logic
        $('input[name^=fcr_approvees]').eq(0).closest('tr').addClass('requisitioner-hide');
        $('.requisitioner-hide').closest('tr').addClass('requisitioner-hide');
        $('.purchaser-hide').closest('tr').addClass('purchaser-hide');

        $('.roletype, #fcr_store').on('change',function(e){
            updateFieldsToRoleType();
        });

        updateFieldsToRoleType();

        var billingRegion = $('#fcr_billing_region');
        var billingRegionId = $('#fcr_billing_region_id');
        var shippingRegion = $('#fcr_shipping_region');
        var shippingRegionId = $('#fcr_shipping_region_id');

        // Region logic
        $('#fcr_billing_country_id').on('change',function(e){
            updateFieldDisplayByStyle(billingRegion);
            updateFieldDisplayByStyle(billingRegionId);
        });

        $('#fcr_shipping_country_id').on('change',function(e){

            updateFieldDisplayByStyle(shippingRegion);
            updateFieldDisplayByStyle(shippingRegionId);
        });

        updateFieldDisplayByStyle(billingRegion);
        updateFieldDisplayByStyle(billingRegionId);
        updateFieldDisplayByStyle(shippingRegion);
        updateFieldDisplayByStyle(shippingRegionId);

        $('#billing_fieldset select, #shipping_fieldset select').removeClass('required-entry').removeClass('validate-select');

        // same as billing logic
        $('#fcr_same_as_billing').on('change', function(e){
            updateSameAsBillingFields();
            updateFieldDisplayByStyle(shippingRegion);
            updateFieldDisplayByStyle(shippingRegionId);
        });
    });
})(jQuery.noConflict());
