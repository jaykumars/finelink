<?php

class Fineline_Setstore_Model_Observer extends Varien_Event_Observer
{
  public function setstore($observer)
  {
      if(Mage::getSingleton('customer/session')->isLoggedIn())
      {
        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId(); //get the group id
        $view = Mage::getModel('customer/group')->load($groupId)->getFcrView(); //get the group store view
        //echo $view;
        Mage::app()->setCurrentStore($view); 
      }
  }
}

?>