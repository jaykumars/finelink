<?php

class Chili_Web2print_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{

    private $designCollaborationModel;

    /**
     * Get item configure url to editor
     * If the product has any document reference id to CHILI the editor should be the url
     *
     * @return string
     */
    public function getConfigureUrl()
    {
        // Default behavior
        $url = 'checkout/cart/configure';
        $item = $this->getItem();
        $params = array('id' => $item->getId());

        $docidObj = $item->getOptionByCode('chili_document_id');
        $docid = null;
        if ($docidObj != null) {
            $docid = $docidObj->getValue();
        }

        // Change configure URL in case of a CHILI enabled product
        if ($docid != null) {
            $url = 'web2print/editor/load';
            $params['type'] = 'quoteitem';
        }

        return $this->getUrl($url, $params);
    }

    public function setDesignCollaborationModel($quoteItemId)
    {
        $this->designCollaborationModel = Mage::helper('designcollaboration')->getDesignByQuoteItemId($quoteItemId);
    }

    public function isDesignCollaborationModel()
    {
        return (bool) $this->designCollaborationModel;
    }

    public function getDesignCollaborationName()
    {
        if ($this->designCollaborationModel) return $this->designCollaborationModel['designname'];
    }

    public function getDesignCollaborationImageUrl()
    {
        if ($this->designCollaborationModel) return Mage::helper('designcollaboration')->getViewImages($this->designCollaborationModel['finelink_item_id'], 'list');
    }

    public function getDesignCollaborationElementUrl()
    {
        if ($this->designCollaborationModel) return Mage::helper('designcollaboration')->getViewUrl($this->designCollaborationModel['design_id']);
    }

    public function getDesignCollaborationEditUrl()
    {
        if ($this->designCollaborationModel) return Mage::helper('designcollaboration')->getEditUrl($this->designCollaborationModel['design_id']);
    }

}