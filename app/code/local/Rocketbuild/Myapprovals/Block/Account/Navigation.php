<?php

class Rocketbuild_Myapprovals_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation {

    public function addLink($name, $path, $label, $urlParams=array(), $order=0)
    {
        $this->_links[$name] = new Varien_Object(array(
            'name' => $name,
            'path' => $path,
            'label' => $label,
            'url' => $this->getUrl($path, $urlParams),
            'order' => $order,
        ));
        return $this;
    }

    public function getLinks()
    {
        // Maybe add some smarter ordering here
        // uasort($this->_links, array($this, "compare"));

        $ma = $this->_links['myapprovals'];
        $rp = $this->_links['reports'];

        if ($ma != null) {
            unset($this->_links['myapprovals']);
            $this->_links['myapprovals'] = $ma;
        }

        if ($rp != null) {
            unset($this->_links['reports']);
            $this->_links['reports'] = $rp;
        }

        return $this->_links;
    }

    // private function compare($a, $b) {
    //     return $a->getOrder() - $b->getOrder();
    // }

}
