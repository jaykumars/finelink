<?php
$installer = $this;
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'cost_center', array(
          	'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			   'nullable' => true,
        	   'comment' => 'Cost Center'
    ));