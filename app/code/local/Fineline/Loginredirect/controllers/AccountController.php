<?php 

require_once 'Mage/Customer/controllers/AccountController.php';

class Fineline_Loginredirect_AccountController extends Mage_Customer_AccountController
{

    public function _loginPostRedirect()
    {
        $session = $this->_getSession();
        $beforeURL = $session->getBeforeAuthUrl();

        if (strpos($beforeURL, 'myapprovals/index') == true){
            $this->_redirectUrl(Mage::getUrl('myapprovals/index'));
        } 
        elseif (strpos($beforeURL, 'checkout/onepage') == true){
            $this->_redirectUrl(Mage::getUrl('checkout/onepage'));
        }
        else {
            $this->_redirectUrl(Mage::getBaseUrl());
        }
    }
}