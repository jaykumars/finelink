<?php

class Fineline_Customerroles_Block_Customer_Group_Renderer_Store extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
      public function render(Varien_Object $row)
      {
          $value =  $row->getData($this->getColumn()->getIndex());

          if ($value == 0) {
              return "<h5><h5>";
          }

          $website = Mage::getModel('core/website')->load($value);
          $name = $website->getName();
          return "<h5>$name</h5>";
      }
}
