<?php

class Fineline_Designcollaboration_Block_Modal_Sharing extends Mage_Core_Block_Template
{

    public function getSharingUser($designId)
    {
        $resultData = array();

        $designOwner = Mage::getModel('designcollaboration/designs')->getCollection()
            ->addFilter('design_id', $designId, 'AND')
            ->setPageSize(1)
            ->getFirstItem();

        if ($designOwner->getId() > 0) {
            $owner = Mage::getModel('customer/customer')->load($designOwner->getCreatedBy())->getData();
            $resultData[] = array('is_owner' => true,
                'sharing_id' => 0,
                'firstname' => $owner['firstname'],
                'lastname' => $owner['lastname'],
                'email' => $owner['email']);
        }

        $designsSharings = Mage::getModel('designcollaboration/sharings')->getCollection()
            ->addFilter('design_id', $designId)
            ->setOrder('created_at', 'ASC');

        foreach ($designsSharings->getData() AS $value) {

            if (is_null($value['customer_id'])) {
                $firstname = 'Unregistered';
                $lastname = 'user';
                $email = Mage::getModel('designcollaboration/unregisteredusers')->getUnregisteredUserEmailById($value['unregistered_user_id']);
            } else {
                $owner = Mage::getModel('customer/customer')->load($value['customer_id'])->getData();
                $firstname = $owner['firstname'];
                $lastname = $owner['lastname'];
                $email = $owner['email'];
            }

            $resultData[] = array('is_owner' => false,
                'sharing_id' => $value['sharing_id'],
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email);
        }

        return $resultData;
    }

    public function getCurrentEmailUser()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            return Mage::getSingleton('customer/session')->getCustomer()->getEmail();
        }

        return false;
    }

}