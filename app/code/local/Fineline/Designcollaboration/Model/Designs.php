<?php

class Fineline_Designcollaboration_Model_Designs extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/designs');
    }

    private function isStatusPendingOrApproved($designModel) {
        return ($designModel->getStatusId() == 3 || $designModel->getStatusId() == 4);
    }

    public function extendTimeLocked($designId, $customerId)
    {
        $updateData = array('locked_till' => Mage::getModel('core/date')->timestamp(time() + 900),
            'locked_at' => Mage::getModel('core/date')->timestamp(time()),
            'locked_by' => $customerId);
        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);

        try {
            $updateDesign->setId($designId)->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function setStatusUnlocked($designId, $customerId, $setStatusUnlocked = true)
    {
        $updateData = array('modified_at' => Mage::getModel('core/date')->timestamp(time()),
            'modified_by' => $customerId,
            'locked_till' => null,
            'locked_by' => null,
            'locked_at' => null);
        if ($setStatusUnlocked) $updateData['status_id'] = 1;

        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);

        try {
            $updateDesign->setId($designId)->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function updateDesignDataAndSetUnlockDesign($designId, $designName, $documentId = null, $customerId)
    {
        $updateData = array('designname' => $designName);
        if (!is_null($documentId)) $updateData['finelink_item_id'] = $documentId;

        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designId)->addData($updateData);

        try {
            $updateDesign->setId($designId)->save();
            $this->setStatusUnlocked($designId, $customerId);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function revokeStatusToEditDesign($designModel, $customerId)
    {
        $updateData = array('status_id' => 1,
            'modified_at' => Mage::getModel('core/date')->timestamp(time()),
            'modified_by' => $customerId,
            'approved_at' => null,
            'approved_by' => null);
        $updateDesign = Mage::getModel('designcollaboration/designs')->load($designModel->getDesignId())->addData($updateData);

        try {
            $updateDesign->setId($designModel->getDesignId())->save();
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerId, null, 6);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

}