<?php
class Onelogin_SAML_Model_Signaturealgorithm
{
    public function toOptionArray()
    {
    $values = array(
                'http://www.w3.org/2000/09/xmldsig#rsa-sha1',
                'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
                'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384',
                'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512',
                'http://www.w3.org/2000/09/xmldsig#dsa-sha1'
    );

    $options = array();

    foreach ($values as $value) {
        $option = array('value' => $value, 'label' => $value);
        $options[] = $option;
    }

        return $options;
    }
}
