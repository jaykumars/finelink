<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
    $this->getTable('eav_attribute'),
    'read_only',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable'  => false,
        'default'	=>	0,
        'comment'   => 'Read Only'
    )
);
$installer->endSetup();
