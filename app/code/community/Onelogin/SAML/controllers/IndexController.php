<?php

require_once("SamlController.php");

class Onelogin_SAML_IndexController extends Onelogin_SAML_SamlController
{
    public function indexAction() {
        parent::loginAction();
    }
}
