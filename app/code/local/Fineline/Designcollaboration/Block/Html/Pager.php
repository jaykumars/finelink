<?php

class Fineline_Designcollaboration_Block_Html_Pager extends Mage_Page_Block_Html_Pager
{
    protected $_showAmounts    = true;

    public function setShowAmounts($value)
    {
        $this->_showAmounts = $value;
        return $this;
    }

    public function getShowAmounts()
    {
        return $this->_showAmounts;
    }

    public function addAvailableLimit($newValue)
    {
        $newArray = array();
        foreach ($this->_availableLimit as $key => $value) {
            if ((int)$value > (int)$newValue) {
                $newArray[$newValue] = $newValue;
            }
            $newArray[$key] = $value;
        }
        $this->_availableLimit = $newArray;

        return $this;
    }

    public function setAvailableLimit(array $limits)
    {
        parent::setAvailableLimit($limits);

        return $this;
    }

    public function getLimitUrl($limit)
    {
        return $this->getPagerUrl(array(
            $this->getLimitVarName() => $limit,
            $this->getPageVarName() => 1
        ));
    }

}
