<?php
class Cybersource_Paypal_Model_Paypal extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'cybersourcepaypal';
    protected $_formBlockType = 'cybersourcepaypal/form_paypal';
    protected $_infoBlockType = 'cybersourcepaypal/info_paypal';
    protected $_isGateway = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_isInitializeNeeded = true;
    public $_customerSession = null;
    public $_checkout = null;
    public $_customer = null;
    const METHOD_GUEST    = 'guest';
    const METHOD_REGISTER = 'register';
    const METHOD_CUSTOMER = 'customer';

    public function __construct() {
        $this->_customerSession = Mage::getSingleton('customer/session');
        $this->_checkout = Mage::getSingleton('checkout/session');
    }

    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }

    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }

        $info = $this->getInfoInstance();

        return $this;
    }

    /**
     * Checkout redirect URL getter for onepage checkout (hardcode)
     *
     * @see Mage_Checkout_OnepageController::savePaymentAction()
     * @see Mage_Sales_Model_Quote_Payment::getCheckoutRedirectUrl()
     * @return string
     */
    public function getCheckoutRedirectUrl()
    {
        return Mage::getUrl('cybersourcepaypal/index/index');
    }

    public function validate()
    {
        return $this;
    }

    public function authorize(Varien_Object $payment, $amount)
    {
        //call soap API
        Mage::getModel('cybersourcepaypal/soapapi_paypal')->payPalEcDoPaymentService($payment, $amount);
        return $this;
    }

    public function capture(Varien_Object $payment, $amount)
    {
        //call soap API
        $response = Mage::getModel('cybersourcepaypal/soapapi_paypal')->payPalDoCaptureService($payment,$amount);
        return $this;
    }



    /**
     * Get Session Quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote() {
        $quoteId =  $this->_getCheckout()->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        return $quote;
    }

    /**
     * Get customer session object
     *
     * @return Mage_Customer_Model_Session
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
}