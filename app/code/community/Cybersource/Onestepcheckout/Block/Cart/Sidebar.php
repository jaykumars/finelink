<?php
/**
 * Minicart block *

 */
class Cybersource_Onestepcheckout_Block_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar
{
    /**
     * Get minicart custom checkout page
     *
     * @return int | float
     */
    function isPossibleOnestepcheckout()
    {
        return $this->helper('cybersource_onestepcheckout')->oneStepCheckoutEnabled();
    }

    public function getCheckoutUrl()
    {
       if($this->isPossibleOnestepcheckout()){
           return $this->getUrl('checkout/onestep', array('_secure'=>true));
        }
        return parent::getCheckoutUrl();
    }
}
