<?php

class Fineline_Designcollaboration_Model_Api extends Mage_Core_Model_Abstract
{
    /**
     * Defining constants webservice functions
     */
    const CHILI_ADD_PERSONAL_DOCUMENT = 'ResourceItemAdd';
    const CHILI_GET_PERSONAL_DOCUMENT = 'ResourceItemCopy';
    const CHILI_GET_RESOURCE_IMAGE_URL = 'ResourceItemGetURLForAnonymousUser';
    const CHILI_GET_EDITOR_URL = 'DocumentGetEditorURL';
    const CHILI_SAVE_PERSONAL_DOCUMENT  = 'ResourceItemSave';
    const CHILI_SEARCH_FOR_RESOURCE       = 'ResourceSearch';
    const CHILI_RESET_PREVIEWS  = 'ResourceItemResetPreviews';

    private $environmentWebserviceUrl;
    private $environmentUrl;
    private $environmentWsdl;
    private $environmentUsername;
    private $environmentPassword;

    public $instance;
    public $apiKey;

    public function __construct()
    {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 86400);

        $this->environmentUrl		    = Mage::helper('designcollaboration/data')->getChiliEnvironment();
        $this->environmentWebserviceUrl	= Mage::helper('designcollaboration/data')->getChiliWebserviceUrl();
        $this->environmentWsdl		    = Mage::helper('designcollaboration/data')->getChiliWsdl();
        $this->environmentUsername	    = Mage::helper('designcollaboration/data')->getChiliUsername();
        $this->environmentPassword	    = Mage::helper('designcollaboration/data')->getChiliPassword();

        //0 = default website
        $this->_websiteId = 0;
        $this->_website = null;

        // Get soap instance
        $this->getInstance();

        $this->getApiKey();

        parent::_construct();
        $this->_init('designcollaboration/api');
    }

    public function setWebsite($websiteId){
        if($websiteId){
            $website = Mage::getModel('core/website')->load($websiteId);
            //Set the website to this website id
            $this->_websiteId = $website->getId();
            $this->_website = $website;
        }else{
            $website = null;
        }

        $this->environmentUrl		= Mage::helper('designcollaboration/data')->getChiliEnvironment($website);
        $this->environmentWebserviceUrl	= Mage::helper('designcollaboration/data')->getChiliWebserviceUrl($website);
        $this->environmentWsdl		= Mage::helper('designcollaboration/data')->getChiliWsdl($website);
        $this->environmentUsername	= Mage::helper('designcollaboration/data')->getChiliUsername($website);
        $this->environmentPassword	= Mage::helper('designcollaboration/data')->getChiliPassword($website);

        $this->getApiKey();
    }

    /**
     * @return bool|SoapClient
     */
    public function getInstance() {
        if ($this->instance === null) {
            // Make sure the SOAP location is properly set
            $soapParameters['location'] = $this->environmentWebserviceUrl;

            try {
                $this->instance = @new SoapClient($this->environmentWsdl, $soapParameters);
            }catch(Exception $e) {
                $this->instance = false;
            }
        }
        return $this->instance;
    }

    public function getApiKey()
    {
        $apiKeyArray = Mage::getSingleton('core/session')->getChiliApiKeyArray();

        //Check if array is in session
        if (is_array($apiKeyArray) && array_key_exists($this->_websiteId, $apiKeyArray)) {
            $apiKey = $apiKeyArray[$this->_websiteId]['key'];
            $apiKeyExpireTime = $apiKeyArray[$this->_websiteId]['expireTime'];
        } else {
            $apiKey = null;
        }

        if (!$apiKey || (time() >= strtotime($apiKeyExpireTime))) {
            $this->connect();
        }

        return $apiKey;
    }

    public function connect() {
        if ($this->instance === false) {
            return null;
        }
        $apiKeyArray = Mage::getSingleton('core/session')->getChiliApiKeyArray();

        if($this->_website){
            //If website exists, check config
            if(Mage::helper('designcollaboration/data')->isSameAsDefaultConfig($this->_website)){
                //If config is same as default, use default api key
                if(!array_key_exists(0, $apiKeyArray) && time() >= strtotime($apiKeyArray[0]['expireTime'])){
                    $apiKeyArray[0] = $this->_generateApiKey();
                }
                $apiKeyArray[$this->_website->getId()] = $apiKeyArray[0];
            }else{
                //If not the same, generate and save new api key
                $apiKeyArray[$this->_website->getId()] = $this->_generateApiKey();
            }
        }else{
            //Create (default) api key as [0] element of array
            $apiKeyArray[0] = $this->_generateApiKey();
        }

        Mage::getSingleton('core/session')->setChiliApiKeyArray($apiKeyArray);
    }

    private function _generateApiKey()
    {
        if ($this->instance === false) {
            return null;
        }
        // ask for an api key
        $array = array(
            'environmentNameOrURL' => $this->environmentUrl,
            'userName' => $this->environmentUsername,
            'password' => $this->environmentPassword,
        );

        //get Key
        $key = $this->instance->GenerateApiKey($array);

        $result = array();
        $result['key'] = $this->_getDomResult($key->GenerateApiKeyResult, 'apiKey', 'key');
        $result['expireTime'] = $this->_getDomResult($key->GenerateApiKeyResult, 'apiKey', 'validTill');

        if ($result['key'] == '') {
            throw new Exception('Could not get valid API key from webservice. Not a valid combination of environment, username and password');
        }

        return $result;
    }

    private function _getDomResult($result, $tag, $attribute, $item = 0)
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($result);
            return $dom->getElementsByTagName($tag)->item($item)->getAttribute($attribute);
        } catch (Exception $e) {
            throw new Exception("Unable to get dom result: " . $e->getMessage());
            return false;
        }

    }

    protected function callFunction($method, $settings){
        $functionResponse = '';
        if ($this->instance === false) {
            return null;
        }
        Mage::dispatchEvent('designcollaboration_api_call_before', array('method'=>$method, 'settings'=>$settings));

        try {
            $functionResponse = $this->instance->$method($settings);
        }catch(Exception $e) {
            var_dump("Webservice Exception: " . print_r($e->__toString(), true));
            $functionResponse = null;
            $this->_exception = $e;

        }

        Mage::dispatchEvent('designcollaboration_api_call_after', array('method'=>$method, 'settings'=>$settings, 'response'=>$functionResponse));

        return $functionResponse;
    }

    /**
     * create new document
     */
    public function addDocument($documentName, $resourceName = 'Documents')
    {
        if ($this->instance === false) {
            return null;
        }
        $array = array(
            'apiKey' => $this->getApiKey(),
            'resourceName' => $resourceName,
            'newName' => $documentName,
            'folderPath' => 'tmp',
        );

        $document = $this->callFunction(self::CHILI_ADD_PERSONAL_DOCUMENT, $array);
        return $this->_getDomResult($document->ResourceItemAddResult, 'item', 'id');
    }

    /**
     * copy document
     */
    public function copyDocument($documentId, $documentName, $resourceName = 'Documents')
    {
        if ($this->instance === false) {
            return null;
        }
        $array = array(
            'apiKey' => $this->getApiKey(),
            'resourceName' => $resourceName,
            'itemID' => $documentId,
            'newName' => $documentName,
            'folderPath' => 'tmp',
        );

        $document = $this->callFunction(self::CHILI_GET_PERSONAL_DOCUMENT, $array);
        return $this->_getDomResult($document->ResourceItemCopyResult, 'item', 'id');
    }

    /**
     * save document
     * @param type $itemId
     * @param string $xmlString
     * @param string $resourceName
     * @return bool
     */
    public function saveDocument($itemId, $xmlString, $resourceName = 'Documents')
    {
        $apiKey = $this->getApiKey();
        if ($apiKey) {
            $array = array(
                'apiKey' => $this->getApiKey(),
                'resourceName' => $resourceName,
                'itemID' => $itemId,
                'xml' => $xmlString
            );

            $xml = $this->callFunction(self::CHILI_SAVE_PERSONAL_DOCUMENT, $array);
            return $xml->ResourceItemSaveResult == "<ok/>";
        }
    }

    /**
     * save document
     * @param type $itemId
     * @param string $resourceName
     * @return bool
     */
    public function resetPreviews($itemId, $resourceName = 'Documents')
    {
        $apiKey = $this->getApiKey();
        if ($apiKey) {
            $array = array(
                'apiKey' => $this->getApiKey(),
                'resourceName' => $resourceName,
                'itemID' => $itemId,
            );

            $xml = $this->callFunction(self::CHILI_RESET_PREVIEWS, $array);
            return $xml->ResourceItemResetPreviewsResult == "<ok/>";
        }
    }

    /**
     * get the image url of document
     * @param type $documentId
     * @param type $type
     * @param type $resourceName
     * @param type $pageNum
     * @return string imageUrl
     */
    public function getResourceImageUrl($documentId, $type = 'thumbnail', $resourceName = 'Documents', $pageNum = '1')
    {
        if ($this->getApiKey()) {
            $array = array(
                'apiKey' => $this->getApiKey(),
                'resourceName' => $resourceName,
                'itemID' => $documentId,
                'type' => $type,
                'pageNum' => $pageNum
            );

            $urlDocument = $this->callFunction(self::CHILI_GET_RESOURCE_IMAGE_URL, $array);

            if ($urlDocument) {
                $url = $this->_getDomResult($urlDocument->ResourceItemGetURLForAnonymousUserResult, "urlInfo", "url");
                return $url . "&t=" . time();
            }
        }
        return null;
    }

    /**
     * get iframe source url of the document's editor
     * @param   string  document reference id
     */
    public function getEditorUrl($documentId, $productId = false)
    {
        $array = array(
            'apiKey' => $this->getApiKey(),
            'itemID' => $documentId,
            'workSpaceID' => Mage::helper('designcollaboration/data')->getCurrentWorkspacePreference(),
            'viewPrefsID' => Mage::helper('designcollaboration/data')->getCurrentViewPreference(),
            'constraintsID' => Mage::helper('designcollaboration/data')->getCurrentDocumentConstraint(),
            'viewerOnly' => '',
            'forAnonymousUser' => false
        );

        $editorUrlRequestXml = $this->callFunction(self::CHILI_GET_EDITOR_URL, $array);
        $url = $this->_getDomResult($editorUrlRequestXml->DocumentGetEditorURLResult, "urlInfo", "url");
        $url .= '&enableFolding=true';

        return $url;
    }

    public function searchForResource($resourceName = 'Documents', $name = ''){
        $apiKey = $this->getApiKey();
        if ($apiKey) {
            $array = array(
                'apiKey'       => $apiKey,
                'resourceName' => $resourceName,
                'name'          => $name
            );
            $result = $this->callFunction(self::CHILI_SEARCH_FOR_RESOURCE, $array);

            return $result->ResourceSearchResult;
        }
    }

}
