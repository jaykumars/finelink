<?php

class Fineline_Extapi_Model_Observer extends Varien_Object
{
    public function addAttibutes($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setPressInstructions($product->getPressInstructions());
        $quoteItem->setE32Template($product->getE32Template());
        $quoteItem->setCustomizable($product->getCustomizable());
        $quoteItem->setColorsBackSelect($product->getColorsBackSelect());
        $quoteItem->setColorsFrontSelect($product->getColorsFrontSelect());
        $quoteItem->setFgiNumber($product->getFgiNumber());
        $quoteItem->setFinishSize($product->getFinishSize());
        $quoteItem->setInventory($product->getInventory());
        $quoteItem->setLeadTime($product->getLeadTime());
        $quoteItem->setSides($product->getSides());
        $quoteItem->setStock($product->getStock());
        $quoteItem->setDescription($product->getDescription());
    }
  /*  public function addE32($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setE32Template($product->getE32Template());
    }
    public function addCustomizable($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setCustomizable($product->getCustomizable());
    }
    public function addColorsBackSelect($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setColorsBackSelect($product->getColorsBackSelect());
    }
    public function addColorsFrontSelect($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setColorsFrontSelect($product->getColorsFrontSelect());
    }
    public function addFgiNumber($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setFgiNumber($product->getFgiNumber());
    }
    public function addFinishSize($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setFinishSize($product->getFinishSize());
    }
    public function addInventory($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setInventory($product->getInventory());
    }
    public function addLeadTime($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setLeadTime($product->getLeadTime());
    }
    public function addSides($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setSides($product->getSides());
    }
    public function addStock($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setStock($product->getStock());
    }*/
}

?>