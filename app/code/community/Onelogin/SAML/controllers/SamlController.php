<?php

class Onelogin_SAML_SamlController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $this->loginAction();
    }

    /**
     * Process login
     */
    public function loginAction() {
        $customerSession = Mage::getSingleton('customer/session', array('name'=>'frontend'));
        if (!$customerSession->isLoggedIn()){
            $module_enabled = Mage::helper('onelogin_saml')->checkEnabledModule();
            if ($module_enabled) {
                $settingsInfo = Mage::helper('onelogin_saml')->getSettings();

                $idp_sso_binding = $settingsInfo['idp']['singleSignOnService']['binding'];
                if ($idp_sso_binding == "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect") {
                    $this->loginActionRedirect($settingsInfo, $customerSession);
                } else {
                    $this->loginActionPOST($settingsInfo, $customerSession);
                }
            } else {
                Mage::getSingleton('core/session')->addError("You tried to start a SSO process but Onelogin SAML module has disabled status");
                $this->_redirectUrl(Mage::getBaseUrl());
            }
        } else {
            Mage::getSingleton('core/session')->addError("You tried to start a SSO process but you are already logged");
            $this->_redirectUrl(Mage::getBaseUrl());
        }
    }


    /**
     * Process login (HTTP-POST)
     */
    public function loginActionPOST($settingsInfo, $customerSession)
    {
        $settings = new OneLogin_Saml2_Settings($settingsInfo, true);
        $authNRequest = new OneLogin_Saml2_AuthnRequest($settings);
        $authNRequestXML = $authNRequest->getXML();

        $key = $settings->getSPkey();
        $cert = $settings->getSPcert();

        $signatureAlgorithm = $signatureAlgorithm = Mage::getStoreConfig('onelogin_saml/advanced/signaturealgorithm');
        $sigAlg = isset($signatureAlgorithm)? $signatureAlgorithm : 'http://www.w3.org/2000/09/xmldsig#rsa-sha1';
        
        $signedAuthNRequestXML = OneLogin_Saml2_Utils::addSign($authNRequestXML, $key, $cert, $sigAlg);
        
        $encodedAuthNRequest = base64_encode($signedAuthNRequestXML);

        $params = array(
            'SAMLRequest' => $encodedAuthNRequest
        );

        $redirectTo = $customerSession->getBeforeAuthUrl();
        if (isset($redirectTo)) {
            $params['RelayState'] = $redirectTo;
        }

        $idpData = $settings->getIdPData();
        $ssoURL = $idpData['singleSignOnService']['url'];
        $this->executePost($ssoURL, $params);
    }

    /**
     * Process login (HTTP-Redirect)
     */
    public function loginActionRedirect($settingsInfo, $customerSession)
    {
        $auth = Mage::helper('onelogin_saml')->getAuth($settingsInfo);

        $redirectTo = $customerSession->getBeforeAuthUrl();
        if (isset($redirectTo)) {
            $auth->login($redirectTo);
        } else {
            $auth->login();
        }
    }

    /**
     * Process logout
     */
    public function logoutAction()
    {
        $module_enabled = Mage::helper('onelogin_saml')->checkEnabledModule();
        if ($module_enabled) {
            $slo_enabled = Mage::getStoreConfig('onelogin_saml/options/slo');
            if ($slo_enabled) {
                Mage::helper('onelogin_saml')->initSLO();
            }
        }
        Mage::getSingleton('core/session')->addError("You tried to SLO but was disabled, try local logout");
        $this->_redirectUrl(Mage::getBaseUrl());
    }

    /**
     * AssertionConsumerService. Process acs
     */
    public function acsAction()
    {
        Mage::helper('onelogin_saml')->processSSO();
    }

    /**
     * Single Logout Service. Process sls
     */
    public function slsAction()
    {
        Mage::helper('onelogin_saml')->processSLO();
    }

    /**
     * Process logout
     */
    public function metadataAction()
    {
         Mage::helper('onelogin_saml')->printSPMetadata();
    }

    public function executePost($url, $params)
    {
        echo '<html>';

        echo '<body onload="document.getElementById(\'send\').click();">';
        echo '<form method="POST" action="'.$url.'">';
        foreach ($params as $key => $value) {
            echo '<input type="hidden" name="'.$key.'" value="'.$value.'">';
        }
        echo '<input type="submit" id="send" value="Send">';
        echo '</form>';
        echo '</body>';
        echo '</html>';
    }

    public function executePostCurl($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($params));
        $result = curl_exec($ch);
    }
}
