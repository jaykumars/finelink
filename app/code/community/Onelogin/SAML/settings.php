<?php

$samlStrict = Mage::getStoreConfig('onelogin_saml/advanced/strict');
$samlDebug = Mage::getStoreConfig('onelogin_saml/advanced/debug');

$idp_entityid = Mage::getStoreConfig('onelogin_saml/idp/entityid');
$idp_sso = Mage::getStoreConfig('onelogin_saml/idp/sso');

$idp_sso_binding = Mage::getStoreConfig('onelogin_saml/idp/sso_binding');

$idp_slo = Mage::getStoreConfig('onelogin_saml/idp/slo');

$sp_entityid = Mage::getStoreConfig('onelogin_saml/advanced/entityid');
$sp_nameIDFormat = Mage::getStoreConfig('onelogin_saml/advanced/nameidformat');

$nameIdEncrypted = Mage::getStoreConfig('onelogin_saml/advanced/nameid_encrypted');
$authnRequestsSigned = Mage::getStoreConfig('onelogin_saml/advanced/authn_request_signed');
$logoutRequestSigned = Mage::getStoreConfig('onelogin_saml/advanced/logout_request_signed');
$logoutResponseSigned = Mage::getStoreConfig('onelogin_saml/advanced/logout_response_signed');
$wantMessagesSigned = Mage::getStoreConfig('onelogin_saml/advanced/want_message_signed');
$wantAssertionsSigned = Mage::getStoreConfig('onelogin_saml/advanced/want_assertion_signed');
$wantAssertionsEncrypted = Mage::getStoreConfig('onelogin_saml/advanced/want_assertion_encrypted');

$signMetadata = Mage::getStoreConfig('onelogin_saml/advanced/metadata_signed');

$signatureAlgorithm = Mage::getStoreConfig('onelogin_saml/advanced/signaturealgorithm');

$requestedAuthnContext = Mage::getStoreConfig('onelogin_saml/advanced/requestedauthncontext');

if (isset($requestedAuthnContext)) {
  if (!is_array($requestedAuthnContext)) {
    $requestedAuthnContext = explode(',', $requestedAuthnContext);
  }

  // Remove -1 value. Fix the Magento's multiselect issue
  $requestedAuthnContext = array_diff($requestedAuthnContext, array('-1'));

} else {
   $requestedAuthnContext = false;
}

require_once('_toolkit_loader.php');

$settings = array (

    'strict' => isset($samlStrict)? $samlStrict : false,
    'debug' => isset($samlDebug)? $samlDebug : false,

    'sp' => array (
        'entityId' => $sp_entityid ? $sp_entityid : 'php-saml',
        'assertionConsumerService' => array (
            'url' => Mage::getUrl('sso/saml/acs',array('_secure'=>true)),
        ),
        'singleLogoutService' => array (
            'url' => Mage::getUrl('sso/saml/sls',array('_secure'=>true)),
        ),
        'NameIDFormat' => $sp_nameIDFormat ? $sp_nameIDFormat : 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    ),
    'idp' => array (
        'entityId' => $idp_entityid,
        'singleSignOnService' => array (
            'url' => $idp_sso,
            'binding' => isset($idp_sso_binding)? $idp_sso_binding : 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ),
        'x509cert' => Mage::getStoreConfig('onelogin_saml/idp/x509cert'),
    ),

    'security' => array (
        'signMetadata' => isset($signMetadata)? ($signMetadata? true: false) : false,
        'nameIdEncrypted' => isset($nameIdEncrypted)? $nameIdEncrypted : false,
        'authnRequestsSigned' => isset($authnRequestsSigned)? $authnRequestsSigned : false,
        'logoutRequestSigned' => isset($logoutRequestSigned)? $logoutRequestSigned : false,
        'logoutResponseSigned' => isset($logoutResponseSigned)? $logoutResponseSigned : false,
        'wantMessagesSigned' => isset($wantMessagesSigned)? $wantMessagesSigned : false,
        'wantAssertionsSigned' => isset($wantAssertionsSigned)? $wantAssertionsSigned : false,
        'wantAssertionsEncrypted' => isset($wantAssertionsEncrypted)? $wantAssertionsEncrypted : false,
        'signatureAlgorithm' => isset($signatureAlgorithm)? $signatureAlgorithm : 'http://www.w3.org/2000/09/xmldsig#rsa-sha1',
        'requestedAuthnContext' => !empty($requestedAuthnContext)? $requestedAuthnContext : false,
        'relaxDestinationValidation' => true,
    )
);

$sp_x509cert = Mage::getStoreConfig('onelogin_saml/advanced/x509cert');
$sp_privatekey = Mage::getStoreConfig('onelogin_saml/advanced/privatekey');

if (!empty($sp_x509cert)) {
    $settings['sp']['x509cert'] = $sp_x509cert;
}

if (!empty($sp_privatekey)) {
    $settings['sp']['privateKey'] = $sp_privatekey;
}

if (!empty($idp_slo)) {
    $settings['idp']['singleLogoutService']['url'] = $idp_slo;
}
