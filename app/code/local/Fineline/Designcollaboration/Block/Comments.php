<?php

class Fineline_Designcollaboration_Block_Comments extends Mage_Core_Block_Template
{
    public function getComments($designId)
    {
        $commentsCollection = Mage::getModel('designcollaboration/comments')->getCollection()
            ->addFieldToFilter('design_id', $designId)
            ->setOrder('created_at', 'DESC');

        return $commentsCollection;
    }

}