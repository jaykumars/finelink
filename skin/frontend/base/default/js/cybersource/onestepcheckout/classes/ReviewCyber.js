var
    window,
    //Prototype objects
    $,
    $$,
    Ajax,
    Class,
    Event,
    Form,
//external object
    MethodStep,
    checkout,
//create constructor
    Review = Class.create(),
    property,
    baseUrl;


Review.prototype = {
    readyToSave: false,
    getStepUpdateUrl: null,
    getCybersourceUpdateUrl : null,
    stepId: 'review',
    forms: [],
    initialize: function (id, getStepUpdateUrl, submitOrderUrl, successUrl) {
        'use strict';

        this.stepContainer    = $('checkout-step-' + id);
        this.getStepUpdateUrl = getStepUpdateUrl || baseUrl + 'checkout/onestep/updateOrderReview';
        this.getCybersourceUpdateUrl = baseUrl + 'checkout/onestep/updateCyberReview';
        this.submitOrderUrl   = submitOrderUrl  || baseUrl + 'checkout/onestep/submitOrder';
        this.successUrl       = successUrl || baseUrl + 'checkout/onestep/success';
        this.onUpdate         = this.reviewUpdated.bindAsEventListener(this);
        this.onSuccess        = this.orderSubmitAfter.bindAsEventListener(this);
        this.onComplete        = this.cyberSubmit.bindAsEventListener(this);
        this.readyToSave      = false;
        this.forms            = [
            'co-billing-form',
            'co-shipping-form',
            'co-shipping-method-form',
            'co-payment-form',
            'checkout-agreements'
        ];

        // update the review without validating previous steps
        // Event.observe($('checkout-review-update'), 'click', this.updateReview.bindAsEventListener(this, false));

        //update with validating before submitting the order
        $$('#order_submit_button').invoke('observe','click',this.submit.bindAsEventListener(this));
    },
    /**
     * Submits all checkout forms to update the review step or to place an order
     */
    submit: function () {
        'use strict';

        var checkoutMethod,
            request,
            cyberRequest,
            parameters = '',
            postUrl   = this.getStepUpdateUrl,
            onSuccess = this.onUpdate,
            i;
        var cardDetails = [];
        /**
         * Submit order instead of updating only
         */
        if (this.readyToSave) {
            postUrl = '';
            onSuccess = this.onSuccess;
        }

        var cardDetails = [];
        var card = [cardDetails];

        if (checkout && checkout.validateReview(true)) {
            this.startLoader();
            var selected = $$('input[name="payment[method]"]:checked');
            //remove the contents of the customer's credit card details to save 'nothing' on the payment methods.
            if (selected.length > 0 && selected[0].id === "p_method_cybersourcesop") {
                $$('#payment_form_cybersourcesop input,#payment_form_cybersourcesop select').each(function(item)
                    {
                        item.setAttribute('data',item.value);
                        cardDetails[item.getAttribute('id')] = item.value;
                        item.value = '';
                    }
                );
            }

            this.readyToSave = true;

            for (i = 0; i < this.forms.length; i += 1) {
                if ($(this.forms[i])) {

                    parameters += '&' + Form.serialize(this.forms[i]);
                }
            }

            if (checkout.steps.login && checkout.steps.stepContainer) {
                checkoutMethod = 'register';
                $$('input[name="checkout_method"]').each(function (element) {
                    if ($(element).checked) {
                        checkoutMethod = $(element).value;
                    }
                });
                parameters += '&checkout_method=' + checkoutMethod;
            }
            parameters = parameters.substr(1);
            //submit to action

            if (postUrl) {
                if (selected.length > 0 && selected[0].id === "p_method_cybersourcesop") {

                this.getCybersourceUpdateUrl = baseUrl + 'checkout/onestep/updateCyberReview';
                //generate HTML form for Cybersource to submit.
                cyberRequest = new Ajax.Request(
                    this.getCybersourceUpdateUrl,
                    {
                        method:     'post',
                        onComplete: this.onComplete,
                        onSuccess:  this.onUpdate,
                        onFailure:  checkout.ajaxFailure.bind(checkout),
                        parameters: parameters
                    }
                );
                } else {

                    request = new Ajax.Request(
                        this.submitOrderUrl,
                        {
                            method:     'post',
                            onComplete: this.stopLoader.bind(this),
                            onSuccess:  this.onSuccess,
                            onFailure:  checkout.ajaxFailure.bind(checkout),
                            parameters: parameters
                        }
                    );
                }
            } else {
                //restore form elements
                if ($('cybersourceform')) {
                    $('card_type').value = $('cybersourcesop_cc_type').getAttribute('data');
                    $('card_number').value = $('cybersourcesop_cc_number').getAttribute('data');
                    $('card_expiry_date').value = $('cybersourcesop_expiration').getAttribute('data') + '-' + $('cybersourcesop_expiration_yr').getAttribute('data');
                    $('card_cvn').value = $('cybersourcesop_cc_cid').getAttribute('data');
                    $('cybersourceform').submit();
                }
            }
            //restore form elements
            if (selected.length > 0 && selected[0].id === "p_method_cybersourcesop") {
                $$('#payment_form_cybersourcesop input,#payment_form_cybersourcesop select').each(function(item)
                    {
                        item.value = item.getAttribute('data');
                        item.setAttribute('data','');
                    }
                );
            }

            //placate jslint
            if (request) {
                if (request.nothing === undefined) {
                    request.nothing = 0;
                }
            }
        }
    },

    /**
     * Updates the review step
     *
     * @param event
     * @param noValidation
     */
    updateReview: function (event, noValidation) {
        'use strict';

        var parameters = '',
            i,
            request,
            cyberRequest,
            valid = false;

        noValidation = !!noValidation;

        valid = (checkout && checkout.validateReview(!noValidation));

        if (valid) {
            this.startLoader();
            for (i = 0; i < this.forms.length; i += 1) {
                if ($(this.forms[i])) {

                    parameters += '&' + Form.serialize(this.forms[i]);
                }
            }

            parameters = parameters.substr(1);

            request = new Ajax.Request(
                this.getStepUpdateUrl,
                {
                    method:     'post',
                    onComplete: this.stopLoader.bind(this),
                    onSuccess:  this.onUpdate,
                    onFailure:  checkout.ajaxFailure.bind(checkout),
                    parameters: parameters
                }
            );



        }

        //placate jslint
        if (event.nothing === undefined) {
            event.nothing = 0;
        }
        if (request.nothing === undefined) {
            request.nothing = 0;
        }
        if(cyberRequest){
            if (cyberRequest.nothing === undefined) {
                cyberRequest.nothing = 0;
            }
        }
    },

    /**
     * Updates the HTMl of the review step
     *
     * @param transport
     *
     * @returns {boolean}
     */
    reviewUpdated: function (transport) {
        'use strict';

        var response = {};

        if (transport && transport.responseText) {
            response = JSON.parse(transport.responseText);
        }

        if (!response.error && this.readyToSave) {
            if ($$('#order_submit_button')) {
                $$('#order_submit_button').each(function(element){ element.title = checkout.buttonSaveText; });
                $$('#order_submit_button').each(function(element){ element.down().down().update(checkout.buttonSaveText); });
            }
        } else {
            this.readyToSave = false;
            if ($$('#order_submit_button')) {
                $$('#order_submit_button').each(function(element){ element.title = checkout.buttonUpdateText; });
                $$('#order_submit_button').each(function(element){ element.down().down().update(checkout.buttonUpdateText); });
            }
        }
        //the response is expected to contain the update HTMl for the payment step
        if (checkout) {
            checkout.setResponse(response);
        }
    },

    /**
     * Reruns the Submit to send the cybersource form
     */
    cyberSubmit: function () {
        'use strict';

        if (this.readyToSave) {
               this.submit();
            }
    },

    /**
     * Shows the loging step loader
     */
    startLoader: function () {
        'use strict';

        $$('#order_submit_button').invoke('disable');
        $$('#review-submit-please-wait').each(Element.show);
        if (checkout) {
            $$('#order_submit_button').each(function(element){ element.down().down().update(checkout.buttonWaitText); });
            checkout.toggleLoading(this.stepId + '-please-wait', true);
        }

    },

    /**
     * Hides the login step loader
     */
    stopLoader: function () {
        'use strict';

        $$('#order_submit_button').invoke('enable');
        $$('#review-submit-please-wait').each(Element.hide);

        if (checkout) {
            checkout.toggleLoading(this.stepId + '-please-wait', false);
        }

    },

    /**
     * Processes the controller response to the submit order request
     *
     * @param transport
     */
    orderSubmitAfter: function (transport) {
        'use strict';

        var response = {};

        if (transport && transport.responseText) {
            response = JSON.parse(transport.responseText);
        }
        if (response.success) {
            this.isSuccess = true;
            window.location = this.successUrl;
        } else {
            this.stopLoader();
            this.readyToSave = false;
            if ($$('#order_submit_button')) {
                $$('#order_submit_button').each(function(element){ element.title = checkout.buttonUpdateText; });
                $$('#order_submit_button').each(function(element){ element.down().down().update(checkout.buttonUpdateText); });
            }
        }
        if (checkout) {
            checkout.setResponse(response);
        }
    }
};

function removeTokenChoice(args) {
    //clear radio button choice
    $$('input[id="token"]').forEach(function(element, value) {
        element.checked = false;
    });
    //remove CVNs as well
    removeCVN(999);
    if (args == 1) {
        //enable CVN of New Card.
        $$('input[new="1"]')[0].enable();
    }
}

function removeCVN(numberToKeep) {

    //tokenSize represents the last CVN input element used in the original CC Form
    var tokenSize = document.getElementsByName('payment[payment_token]').length+1;
    $$('input[id="cybersourcesop_cc_cid"]').forEach(function(element, value) {
        if (value != numberToKeep && value != tokenSize) {
            element.value = "";
            element.disable();
        } else {
            element.enable();
        }
    } );
}

/**
 * Extend *_method step object prototypes with shared properties
 */
for (property in MethodStep) {
    if (MethodStep.hasOwnProperty(property)) {
        if (!Review.prototype[property]) {
            Review.prototype[property] = MethodStep[property];
        }
    }
}
