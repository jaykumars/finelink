<?php

class Fineline_Designcollaboration_Model_Unregisteredusers extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/unregisteredusers');
    }

    public function getUnregisteredUserByEmail($email, $insert = true)
    {
        $unregisteredUsers = Mage::getModel('designcollaboration/unregisteredusers')->getCollection()
            ->addFieldToFilter('email', $email)
            ->setPageSize(1)
            ->getFirstItem();

        if ($unregisteredUsers->getUnregisteredUserId() > 0) {
            return $unregisteredUsers->getUnregisteredUserId();
        }

        if (!$insert) {
            return null;
        }

        $dataInsert = array('created_at' => Mage::getModel('core/date')->date(),
            'email' => $email);
        $unregisteredUsersModel = Mage::getModel('designcollaboration/unregisteredusers')->setData($dataInsert);
        try {
            return $unregisteredUsersModel->save()->getId();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getUnregisteredUserEmailById($userId)
    {
        $unregisteredUsers = Mage::getModel('designcollaboration/unregisteredusers')->getCollection()
            ->addFieldToFilter('unregistered_user_id', $userId)
            ->setPageSize(1)
            ->getFirstItem();

        if ($unregisteredUsers->getUnregisteredUserId() > 0) {
            return $unregisteredUsers->getEmail();
        }

        return null;
    }

    public function removeUnregisteredUsers($unregisteredUserId)
    {
        $sharingModel = Mage::getModel('designcollaboration/sharings')->getCollection()
            ->addFieldToFilter('unregistered_user_id', $unregisteredUserId);

        if ($sharingModel->getSize() < 1) {
            $unregisteredUserModel = Mage::getModel('designcollaboration/unregisteredusers')->load($unregisteredUserId);

            try {
                $unregisteredUserModel->delete();
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }

    public function searchAndReplaceUnregisteredUser($entityId, $email)
    {
        $idCustomer = $this->getUnregisteredUserByEmail($email, false);

        if (!is_null($idCustomer)) {
            $sharingData = Mage::getModel('designcollaboration/sharings')->getCollection()
                ->addFieldToFilter('unregistered_user_id', $idCustomer);

            foreach ($sharingData as $key => $item) {
                $updateSharing = array('unregistered_user_id' => null,
                    'customer_id' => $entityId);
                $sharingModelUpdate = Mage::getModel('designcollaboration/sharings')->load($item->getSharingId())->addData($updateSharing);

                try {
                    $sharingModelUpdate->save();
                } catch (Exception $e) {
                    return $e->getMessage();
                }
            }
            $this->removeUnregisteredUsers($idCustomer);
        }

        return true;
    }

}