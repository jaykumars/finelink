<?php

class Rocketbuild_Reports_IndexController extends Mage_Core_Controller_Front_Action {

	/**
	 * Checking if user is logged in or not
	 * If not logged in then redirect to customer login
	 */
	public function preDispatch() {
		parent::preDispatch();

		$authenticated = Mage::getSingleton( 'customer/session' )->authenticate( $this );

		if ( ! $authenticated ) {
			$this->setFlag( '', 'no-dispatch', true );

			// adding message in customer login page
			Mage::getSingleton( 'core/session' )
			    ->addSuccess( Mage::helper( 'reports' )->__( 'Please sign in or create a new account' ) );
		} else if ( ! Mage::getSingleton( 'customer/session' )->getCustomer()->getFcrCanViewReports() ) {
			$this->_redirect('customer/account');
		}
	}

	// public function SaveAction() {
	// 	//get Request stuff
	// 	$report = Mage::getModel('rocketbuild_reports/myreports');
	// 	//set attributes on model
	// 	$report->save();
	// }

	public function IndexAction() {

		$this->loadLayout();
		$this->getLayout()->getBlock( "head" )->setTitle( $this->__( "Reports" ) );
		$breadcrumbs = $this->getLayout()->getBlock( "breadcrumbs" );
		// $breadcrumbs->addCrumb( "account", array(
		// 	"label" => $this->__( "My Account" ),
		// 	"title" => $this->__( "My Account" ),
		// 	"link"  => "/customer/account/"
		// ) );
		//
		// $breadcrumbs->addCrumb( "reports", array(
		// 	"label" => $this->__( "Reports" ),
		// 	"title" => $this->__( "Reports" )
		// ) );

		$this->renderLayout();
	}


	public function SaveReportAction() {

		$id = $this->getRequest()->getPost('id');
		$customer_id = $this->getRequest()->getPost('customer_id');
		$name = $this->getRequest()->getPost('name');
		$base_key = $this->getRequest()->getPost('base_key');
		$checked_attributes = $this->getRequest()->getPost('attributesToSelect');
		$attributes = $this->getRequest()->getPost('attributes');
		$date_range_start = $this->getRequest()->getPost('date_range_start');
		$date_range_end = $this->getRequest()->getPost('date_range_end');

		if($id){
			$report = Mage::getModel('rocketbuild_reports/myreports')->load($id);
		}else{
			$report = Mage::getModel('rocketbuild_reports/myreports');
		}

		$data = array('id'=>$id, 'customer_id'=>$customer_id,'name'=>$name,'base_key'=>$base_key, 'checked_attributes'=>$checked_attributes,  'attributes'=>$attributes,  'date_range_start'=>$date_range_start, 'date_range_end'=>$date_range_end );

		$report->setData($data);

		$report->save();


	}


	/**
	 * Export orders to CSV format
	 *
	 */
	public function ExportCsvAction() {
		$attributesToSelect = $this->getRequest()->getParam('attributesToSelect');
		$key = $this->getRequest()->getParam('key');
		$date_range_start = $this->getRequest()->getParam('date_range_start');
		$date_range_end = $this->getRequest()->getParam('date_range_end');
		$fileName = $key.'.csv';
		$grid     = Mage::helper( 'reports/export' )->generateCSV( $attributesToSelect, $key, $date_range_start, $date_range_end );
		$this->_prepareDownloadResponse( $fileName, $grid );
	}

	/**
	 * Export orders to XML format
	 *
	 */
	public function ExportXmlAction() {
		$attributesToSelect = $this->getRequest()->getParam('attributesToSelect');
		$key = $this->getRequest()->getParam('key');
		$date_range_start = $this->getRequest()->getParam('date_range_start');
		$date_range_end = $this->getRequest()->getParam('date_range_end');
		$fileName = $key.'.xlsx';
		$grid     = Mage::helper( 'reports/export' )->generateXML( $attributesToSelect, $key, $date_range_start, $date_range_end );
		$this->_prepareDownloadResponse( $fileName, $grid );
	}
}
