<?php

/**
 * Created by PhpStorm.
 * User: jays
 * Date: 10/23/2017
 * Time: 4:49 PM
 */
class Fineline_MasterRedirect__Model_Observer extends Varien_Event_Observer
{
    public function setLogoutUrl($observer)
    {

//        $session = $this->_getSession();
        $session = Mage::getSingleton('customer/session');
        $session->logout()->renewSession();

        $storeId = Mage::app()->getStore()->getStoreId();
        $config = Mage::getStoreConfig('masterredirect_options/section_one/custom_field_one', $storeId);

        if (isset($config) && !empty($config)) {
            Mage::app()->getFrontController()->getResponse()->setRedirect($config);
        }
    }
}