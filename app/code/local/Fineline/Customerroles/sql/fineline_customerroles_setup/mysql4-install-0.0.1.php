<?php

$installer = $this;
$installer->startSetup();

$customerGroupTable = $installer->getTable('customer/customer_group');
$table = $installer->getConnection();

// Customer
//

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "fcr_can_override_billing",  array(
    "type"     => "int",
    "backend"  => "",
    "label"    => "Can override billing?",
    "input"    => "boolean",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default"  => "",
    "frontend" => "",
    "unique"   => false,
    "note"     => "Enables editable billing address even if in requisitioner group."
));

$installer->addAttribute("customer", "fcr_can_view_reports",  array(
    "type"     => "int",
    "backend"  => "",
    "label"    => "Can view reports?",
    "input"    => "boolean",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default"  => "",
    "frontend" => "",
    "unique"   => false,
    "note"     => "Can view site reports?"
));

$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "fcr_can_override_billing");
$attribute2 = Mage::getSingleton("eav/config")->getAttribute("customer", "fcr_can_view_reports");

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'fcr_can_override_billing',
    '999'  //sort_order
);
$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'fcr_can_view_reports',
    '999'  //sort_order
);

$used_in_forms=array();

$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
//$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";

$attribute->setData("used_in_forms", $used_in_forms)
        ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 100);
$attribute->save();

$attribute2->setData("used_in_forms", $used_in_forms)
        ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 100);
$attribute2->save();









// Customer Group
//
$table->addColumn($customerGroupTable, 'fcr_roletype', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Customer Group Type'
));

$table->addColumn($customerGroupTable, 'fcr_approver', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Customer Group Approver'
));

$table->addColumn($customerGroupTable, 'fcr_approvees', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => false,
  'comment' => 'Customer Group Approvee'
));

$table->addColumn($customerGroupTable, 'fcr_store', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => false,
  'comment' => 'Customer Group Store'
));

// Billing fields

$table->addColumn($customerGroupTable, 'fcr_billing_first_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing First Name'
));

$table->addColumn($customerGroupTable, 'fcr_billing_middle_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Middle Name'
));

$table->addColumn($customerGroupTable, 'fcr_billing_last_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Last Name'
));

$table->addColumn($customerGroupTable, 'fcr_billing_suffix', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Suffix'
));

$table->addColumn($customerGroupTable, 'fcr_billing_company', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Company'
));

$table->addColumn($customerGroupTable, 'fcr_billing_street', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Street'
));

$table->addColumn($customerGroupTable, 'fcr_billing_city', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing City'
));

$table->addColumn($customerGroupTable, 'fcr_billing_region', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Region'
));

$table->addColumn($customerGroupTable, 'fcr_billing_region_id', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => true,
  'comment' => 'Billing Region Id'
));

$table->addColumn($customerGroupTable, 'fcr_billing_postcode', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Postcode'
));
$table->addColumn($customerGroupTable, 'fcr_billing_country_id', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Country Id'
));
$table->addColumn($customerGroupTable, 'fcr_billing_telephone', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Telephone'
));
$table->addColumn($customerGroupTable, 'fcr_billing_fax', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Billing Fax'
));

// Shipping fields

$table->addColumn($customerGroupTable, 'fcr_shipping_first_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping First Name'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_middle_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Middle Name'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_last_name', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Last Name'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_suffix', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Suffix'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_company', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Company'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_street', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Street'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_city', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping City'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_region', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Region'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_region_id', array(
  'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
  'nullable' => true,
  'comment' => 'Shipping Region Id'
));

$table->addColumn($customerGroupTable, 'fcr_shipping_postcode', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Postcode'
));
$table->addColumn($customerGroupTable, 'fcr_shipping_country_id', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Country Id'
));
$table->addColumn($customerGroupTable, 'fcr_shipping_telephone', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Telephone'
));
$table->addColumn($customerGroupTable, 'fcr_shipping_fax', array(
  'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  'nullable' => true,
  'comment' => 'Shipping Fax'
));

$installer->endSetup();
