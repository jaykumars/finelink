<?php

class Fineline_Designcollaboration_Model_Observers_Store extends Varien_Event_Observer
{
    private $formDefaultValues = array();

    private function prepareFormDefaultValues($model)
    {
        $this->formDefaultValues['yesno'] = array(
            array(
                'value' => 0,
                'label' => Mage::helper('core')->__('No'),
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('core')->__('Yes'),
            )
        );

        $customers = Mage::getModel('customer/group')
            ->getCollection()
            ->addFieldToFilter('fcr_store', $model->getData('website_id'))
            ->setOrder('customer_group_code', 'ASC');
        foreach($customers as $value) {
            $this->formDefaultValues['customer_groups'][] = array(
                'value' => $value->getCustomerGroupId(),
                'label' => $value->getCustomerGroupCode()
            );
        }
    }

    //Needed for setting default values
    private function dcEnableDesignsScript() {
        return <<<EOT
            <script type="text/javascript">
                function dcEnableDesignsToggle() {
                    var dc_enable_designs = jQuery('#dc_enable_design :selected').val();
                    var dc_require_design_approval = jQuery('#dc_require_design_approval');
                    if (dc_enable_designs == 0) {
                        dc_require_design_approval.val(0);
                    }
                    jQuery('#dc_default_design_approval_group').val(0);
                }
            </script>
EOT;
    }

    private function dcRequireDesignsApprovalScript() {
        return <<<EOT
            <script type="text/javascript">
                function dcRequireDesignsApproval() {
                    jQuery('#dc_default_design_approval_group').val(0);
                }
            </script>
EOT;
    }

    public function appendCustomForm(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        $form = $block->getForm();
        $storeKey =  Mage::registry('store_type');
        $model = Mage::registry('store_data');

        $this->prepareFormDefaultValues($model);

        //--------------------------------------------------------------------------------------------------------------
        // Design Collaboration
        $dcFieldSet = $form->addFieldset('dc_fieldset', array(
            'legend' => Mage::helper('core')->__('Design Collaboration'),
        ));

        $dcEnableDesigns = $dcFieldSet->addField('dc_enable_design', 'select', array(
            'name' => $storeKey.'[dc_enable_design]',
            'label' => Mage::helper('core')->__('Enable Design Collaboration'),
            'title' => Mage::helper('core')->__('Enable Design Collaboration'),
            'class' => '',
            'required' => true,
            'values' => $this->formDefaultValues['yesno'],
            'value' => $model->getData('dc_enable_design'),
            'onchange' => 'dcEnableDesignsToggle()',
            'after_element_html' => $this->dcEnableDesignsScript(),
        ));

        $dcRequireDesignApproval = $dcFieldSet->addField('dc_require_design_approval', 'select', array(
            'name' => $storeKey.'[dc_require_design_approval]',
            'label' => Mage::helper('core')->__('Require Design Approval'),
            'title' => Mage::helper('core')->__('Require Design Approval'),
            'class' => '',
            'required' => true,
            'values' => $this->formDefaultValues['yesno'],
            'value' => $model->getData('dc_require_design_approval'),
            'onchange' => 'dcRequireDesignsApproval()',
            'after_element_html' => $this->dcRequireDesignsApprovalScript(),
        ));

        $dcDefaultDesignApprovalGroup = $dcFieldSet->addField('dc_default_design_approval_group', 'select', array(
            'name' => $storeKey.'[dc_default_design_approval_group]',
            'label' => Mage::helper('core')->__('Default Design Approval Group'),
            'title' => Mage::helper('core')->__('Default Design Approval Group'),
            'class' => '',
            'required' => true,
            'values' => $this->formDefaultValues['customer_groups'],
            'value' => $model->getData('dc_default_design_approval_group')
        ));

        $block->setChild('form_after', $block->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap($dcEnableDesigns->getHtmlId(), $dcEnableDesigns->getName())
            ->addFieldMap($dcRequireDesignApproval->getHtmlId(), $dcRequireDesignApproval->getName())
            ->addFieldMap($dcDefaultDesignApprovalGroup->getHtmlId(), $dcDefaultDesignApprovalGroup->getName())
            ->addFieldDependence($dcRequireDesignApproval->getName(), $dcEnableDesigns->getName(), '1')
            ->addFieldDependence($dcDefaultDesignApprovalGroup->getName(), $dcRequireDesignApproval->getName(), '1')
            ->addFieldDependence($dcDefaultDesignApprovalGroup->getName(), $dcEnableDesigns->getName(), '1')
        );

        //--------------------------------------------------------------------------------------------------------------
        // Digital Asset Management
        $damFieldSet = $form->addFieldset('dam_fieldset', array(
            'legend' => Mage::helper('core')->__('Digital Asset Management'),
        ));

        $damAccess = $damFieldSet->addField('dam_access', 'select', array(
            'name' => $storeKey.'[dam_access]',
            'label' => Mage::helper('core')->__('DAM Access'),
            'title' => Mage::helper('core')->__('DAM Access'),
            'class' => '',
            'required' => true,
            'values' => $this->formDefaultValues['yesno'],
            'value' => $model->getData('dam_access')
        ));

        return $this;
    }
}