<?php

class Cybersource_Onestepcheckout_Block_Onestep extends Mage_Checkout_Block_Onepage
{
    public function isCyberEnabled($storeId) {
        $enabled = Mage::getStoreConfig('payment/cybersourcesop/onestep_enabled',$storeId);
        if ($enabled) {
            return true;
        }
        return false;
    }
    protected function _getStepCodes()
    {
        return array('login', 'billing', 'shipping_method', 'shipping',  'payment', 'review');
    }
}
