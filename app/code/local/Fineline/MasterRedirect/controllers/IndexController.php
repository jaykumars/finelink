<?php
/**
 * Created by PhpStorm.
 * User: jays
 * Date: 10/23/2017
 * Time: 1:31 PM
 */


class Fineline_MasterRedirect_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     *  Index Action
     * @access public
     * @return void
     */
    public function indexAction()
    {
        echo "Here's where I change the URL after Logout<br>";
//        $this->getResponse()->setRedirect("http://www.google.com");
    }

    /**
     * Secondary Action
     * @access public
     * @return void
     */
    public function secondAction()
    {
        echo "Here's where I change the URL after Logout Afterwards<br>";
//        $this->getResponse()->setRedirect("http://www.google.com");
    }

}