<?php

class Fineline_Designcollaboration_Model_Statuses extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/statuses');
    }

}