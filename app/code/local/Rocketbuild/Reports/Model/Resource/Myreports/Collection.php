<?php

class Rocketbuild_Reports_Model_Resource_Myreports_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	protected function _construct()
	{
		$this->_init('rocketbuild_reports/myreports');
	}
}
