<?php

class Fineline_Designcollaboration_Model_History extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('designcollaboration/history');
    }

    public function addDesignHistory($designModel, $designName, $customerData, $editorXML, $message)
    {
        $newItemId = Mage::getModel('designcollaboration/api')->copyDocument($designModel->getFinelinkItemId(), $designName);
        if (is_null($newItemId)) {
            return false;
        }

        $result = Mage::getModel('designcollaboration/api')->saveDocument($designModel->getFinelinkItemId(), $editorXML);
        if ($result === false) {
            return false;
        }

        $result = Mage::getModel('designcollaboration/api')->resetPreviews($designModel->getFinelinkItemId());
        if ($result === false) {
            return false;
        }

        $historyData = array('design_id' => $designModel->getDesignId(),
            'finelink_item_id' => $newItemId,
            'created_at' => Mage::getModel('core/date')->timestamp(time()),
            'created_by' => $customerData->getId());
        $historyModel = Mage::getModel('designcollaboration/history')->setData($historyData);
        try {
            $historyId = $historyModel->save()->getId();
            Mage::getModel('designcollaboration/designs')->updateDesignDataAndSetUnlockDesign($designModel->getId(), $designName, null, $customerData->getId());
            Mage::getModel('designcollaboration/comments')->updateLastCommentByHistoryId($designModel->getId(), $historyId);
            Mage::getModel('designcollaboration/comments')->addPost($designModel->getId(), $customerData->getId(), $message, 2, null);
        } catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError(
                $this->__('Error on added history table: %s', $e->getMessage())
            );
        }

        return true;
    }

}