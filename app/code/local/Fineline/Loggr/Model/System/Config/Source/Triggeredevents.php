<?php

class Fineline_Loggr_Model_System_Config_Source_Triggeredevents
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('All events')),
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Only errors')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('All events'),
            1 => Mage::helper('adminhtml')->__('Only errors'),
        );
    }
}
