<?php
$installer = $this;
$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'thirdparty', array(
          	 'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			     'nullable' => true,
        	   'comment' => 'Third Party?'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'thirdpartynumber', array(
          	 'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
  			     'nullable' => true,
        	   'comment' => 'Third Party Number'
    ));